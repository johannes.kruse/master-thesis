# -*- coding: utf-8 -*-

# Examine the properties of  sigma_p*H^T*H- eigenvalues with random injection

import lib.net_model as nm
import numpy as np
import matplotlib.pyplot as plt
import lib.prin_flow_pat as pfp
import lib.observables as ob
import pandas as pd
import scipy.stats as sst
import scipy.linalg as sl
import lib.help_tools as ht

#%%
obs=pd.DataFrame(index=[1],columns=['q50_abs',
                                    'q50_rel',
                                    'q95_abs',
                                    'q95_rel',
                                    'd_b',
                                     'trans_cap'],
                            dtype=np.float)

   
    
#%% Calculate sigma_p*H^T*H- eigenvalues for grid using random injection

N=64
eigenv=pd.DataFrame(index=[1], columns=np.arange(N) )
grid=nm.NetModel('grid_2d', N)
nsamples=50

for i in range(1,nsamples+1):
    cov=ht.make_rand_inj(N)[2]
    flow=pfp.PrinFlow(cov,grid, i)
    flow.calc_all().save(obs, eigenv)



fig, ax=plt.subplots(1,2, figsize=(9,4))
ax[1].plot(np.arange(1.,N)/(N-1), eigenv.T.iloc[1:][::-1]/eigenv.T.sum(), '.-')
ax[0].plot(np.arange(1.,N)/(N-1),
           eigenv.T.iloc[1:][::-1].cumsum()/eigenv.T.sum() , '.-')
ax[0].set_ylabel(
                r'$\sum_{k=1}^{K} \lambda_k \cdot ( \sum_{k=1}^{N-1} \lambda_k )^{-1}$ ')
ax[0].set_xlabel(r'$K/$ N')
ax[1].set_ylabel(
                r'$ \lambda_k \cdot ( \sum_{k=1}^{N-1} \lambda_k )^{-1}$ ')
ax[1].set_xlabel(r'$k/$ N')
fig.tight_layout()

fig.savefig('Results/phth_eva_{}nodes_{}samples_grid_2d.pdf'.format(N,nsamples))



#%% Calculate observables deduced from sigma_p*H^T*H- eigenvalues
#using a grid and different numbers of nodes

nsamples=100
Ns=np.arange(3,11)**2    
obs_n=pd.DataFrame(index=[Ns[0]],columns=['q50_rel',
                                          'q95_rel',
                                          'd_b',
                                          'trans_cap'],
                                           dtype=np.float)
obs_n_std=obs_n.copy()
fig1, ax=plt.subplots(3,1, figsize=(7,4))


j=0
for N in Ns:
    grid=nm.NetModel('grid_2d', N)
    
    for i in range(1,nsamples+1):
        cov=ht.make_rand_inj(N)[2]
        flow=pfp.PrinFlow(cov,grid, i)
        flow.calc_all().save(obs)
    
    obs_n.loc[N]=obs.mean()
    obs_n_std.loc[N]=obs.std().loc[obs_n.columns]
    
    if j%3==0:
        ax[0].hist(obs.q95_rel, label='N={}'.format(N),
                density=True, alpha=0.5)
        ax[1].hist(obs.d_b, label='N={}'.format(N),
                density=True, alpha=0.5, log=True)
        ax[2].hist(obs.trans_cap, bins=20,
                  label='N={}'.format(N), density=True, alpha=0.8,
                  histtype='step')
        
    j=j+1


ax[2].legend()
ax[0].set_xlim([0,1])
ax[1].set_xlim([0,1])
ax[1].set_xlabel(r'Distance $d_B$')
ax[0].set_xlabel(r'$K_{95}/(N-1)$')
ax[2].set_xlabel('Transmission capacity')
ax[1].set_ylabel('Probability')
plt.tight_layout()
plt.savefig('Results/observable_dist_varyN_100samples_grid_2d.pdf')

ax=obs_n.plot(yerr=obs_n_std, style='o-')
ax.set_xlim([Ns.min()-1,Ns.max()+1])
ax.set_ylim([0,1.3])
ax.set_xlabel('N')
ax.set_ylabel('Observable')
plt.savefig('Results/observable_mean_varyN_100samples_grid_2d.pdf')





