#!/usr/bin/env python2
# -*- coding: utf-8 -*-


#Check cumulative sum of a_ij=\mu_i \lambda_j <\mu_i|\lambda_j>^2
# where \mu is eigenval of the injection covariance matrix and \lamda of H^TH


import lib.net_model as nm
import numpy as np
import matplotlib.pyplot as plt
import lib.prin_flow_pat as pfp
import itertools as it
import lib.help_tools as ht


#%%
N=4

g=nm.NetModel('grid_1d', N)


U,d, cov=ht.make_rand_inj(N, dist='uniform')

flow=pfp.PrinFlow(cov,g, 1)
flow.calc_all()

args=it.product(range(N), range(N))
a=[flow.p_eva[i]*g.hth_eva[j]*np.dot(flow.p_eve[:,i], g.hth_eve[:,j])**2 
   for (i,j) in args]
a= np.sort(np.array(a))[::-1]


#plt.plot(np.arange(0.,N)/N, np.cumsum(flow.phth_eva[::-1])/np.sum(flow.phth_eva),'.-' )
#plt.ylabel(r'$\sum_{k=1}^{K} \lambda_k \cdot ( \sum_{k=1}^{N-1} \lambda_k )^{-1}$ ')
#plt.xlabel(r'$K/$ N')
#plt.plot(np.arange(0.,N**2)/N, np.cumsum(a)/np.sum(a), '.-' )
#plt.ylabel(r'$\sum_{k=1}^{\tilde K} a_k \cdot ( \sum_{k=1}^{N^2} a_k )^{-1}$ ')
#plt.xlabel(r'$\tilde K/$ N')

plt.plot(np.arange(0.,N)/N,
         np.cumsum(flow.phth_eva[::-1])/np.sum(flow.phth_eva),
         '.-',
         label=r'$\sum_{k=1}^{K} \lambda_k \cdot ( \sum_{k=1}^{N-1} \lambda_k )^{-1}$ ')
plt.plot( (np.arange(0.,N**2)/N**2 ),
         (np.cumsum(a)/np.sum(a) ),
         '.-',
         label=r'$\sum_{k=1}^{\tilde K} a_k \cdot ( \sum_{k=1}^{N^2} a_k )^{-1}$ ')

plt.xlabel(r'$K/$ N or $\tilde K$ / $N^2$')
plt.ylabel('Cumulative sum')
plt.legend()

#plt.savefig('Results/aij_cumulative_sum_N{}_P_uniform_grid_1d.pdf'.format(N))
