# -*- coding: utf-8 -*-

#Check random injection generators and observables on properties and consistency

import lib.net_model as nm
import numpy as np
import matplotlib.pyplot as plt
import lib.prin_flow_pat as pfp
import lib.observables as ob
import scipy.stats as sst
import scipy.linalg as sl
import lib.help_tools as ht

ns=np.array([3,10, 100])


#%%Inspect method 1 to generate random cov. mat.

fig1,ax1=plt.subplots(3,1)
fig2,ax2=plt.subplots(1,1)
fig3,ax3=plt.subplots(1,1)
j=0

for N in ns:
    us=np.zeros( (500,N) )
    cs=np.zeros( (500,N) )
    ds=np.zeros(500)
    
    for i in range(500):
        u, d, c= ht.make_rand_inj(N, 'bendel')
        cs[i]=np.diag(d)
        us[i]=np.angle(sl.eig(u)[0])
        ds[i]=ob.dist(u, np.diag(np.ones(N)) )
    
    ax1[j].hist(ds, density=True, label='N={}'.format(N))
    ax1[j].set_xlim([0,1])
    ax1[j].legend()
    ax2.hist(us.flatten(),histtype='step', density=True, bins=30,
             label='N={}'.format(N))
    ax3.hist(cs.flatten(),histtype='step', density=True, bins=30,
             label='N={}'.format(N) )
    
    j=j+1

ax2.legend()
ax3.legend()
ax2.set_xlabel(r'Angle $\theta$')
ax2.set_ylabel('Probability')
ax3.set_xlabel(r'Eigenvalue $\lambda$')
ax3.set_ylabel('Probability')
ax1[2].set_xlabel('Distance $d_B$')
ax1[1].set_ylabel('Probability')
fig1.tight_layout()

fig1.savefig('Results/bendel_method_dB_hist.pdf')
fig2.savefig('Results/bendel_method_unitary_matrix_eigenvals.pdf')
fig3.savefig('Results/bendel_method_cov_matrix_eigenvals.pdf')

#%%Inspect method 2 to generate random cov. mat.

fig1,ax1=plt.subplots(3,1)
fig2,ax2=plt.subplots(1,1)
fig3,ax3=plt.subplots(1,1)
j=0

for N in ns:
    us=np.zeros( (500,N) )
    cs=np.zeros( (500,N) )
    ds=np.zeros(500)
    
    for i in range(500):
        c= ht.make_rand_inj(N, 'wishart')
        cs[i], u=sl.eigh(c)
        us[i]=np.angle(sl.eig(u)[0])
        ds[i]=ob.dist(u, np.diag(np.ones(N)) )
    
    ax1[j].hist(ds, density=True, label='N={}'.format(N))
    ax1[j].set_xlim([0,np.sqrt(2*N)])
    ax1[j].legend()
    ax2.hist(us.flatten(),histtype='step', density=True, bins=30,
             label='N={}'.format(N))
    ax3.hist(cs.flatten(),histtype='step', density=True, bins=30,
             label='N={}'.format(N),log=True  )
    
    j=j+1

ax2.legend()
ax3.legend()
ax2.set_xlabel(r'Angle $\theta$')
ax2.set_ylabel('Probability')
ax3.set_xlabel(r'Eigenvalue $\lambda$')
ax3.set_ylabel('Probability')
ax1[2].set_xlabel('Distance $d_B$')
ax1[1].set_ylabel('Probability')
fig1.tight_layout()

fig1.savefig('Results/wishart_method_dB_hist.pdf')
fig2.savefig('Results/wishart_method_unitary_matrix_eigenvals.pdf')
fig3.savefig('Results/wishart_method_cov_matrix_eigenvals.pdf')