#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 11:01:21 2018

System Observables

@author: adminuser
"""

import numpy as np
from scipy.optimize import linear_sum_assignment as lsa

def qx(w,x):
    
    """Number of entries summing up to a share x of w.sum().
    w should be non-increasing """
    
    res= np.argmax((np.cumsum(w) /np.sum(w) )>x   ) +1
    return res, 1.*res/(w.shape[0])
    

def q50(w):
    
    """Number of entries summing up to 50% of w.sum().
    w should be non-increasing """
    
    res= np.argmax((np.cumsum(w) /np.sum(w) )>0.5   ) +1
    return res, 1.*res/(w.shape[0])
    
def q95(w):
    
    """Number of entries summing up to 95% of w.sum().
    w should be non-increasing """
    
    res=np.argmax( (np.cumsum(w) /np.sum(w) )>0.95   )+1
    return res, 1.*res/(w.shape[0])

def dist(u,v):
    
    """Distance between two eigenbasis u and v"""
    
    weights_b=np.abs( np.dot(u.T,v) )
    opt_args=lsa(-weights_b)
    d_b=np.sqrt( np.around(  np.sum(2.-2.*weights_b[opt_args]), decimals=7) ) 
       
    return  d_b/np.sqrt(u.shape[0]*2)
    
def trans_meas(net):
    
    """Transmission capacity and cost """
    
    kl=net.lines_t.p0.abs().quantile(0.99)
    tl=kl*net.lines.length

    return kl.sum(), tl.sum()

def backup_meas(net):
    
    """Backup Energy and capacity """
    
    back= -(net.mis + net.loads_t.p_set).clip_upper(0.)

    return back.mean().sum(), back.quantile(0.99).sum()
    


    
    
    
    