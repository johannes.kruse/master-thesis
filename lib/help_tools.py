#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 26 10:24:25 2018

@author: adminuser

Miscellaneous functions

"""

import scipy.linalg as sl
import numpy as np
from matplotlib import pyplot as plt
import scipy.stats as sst
import pandas as pd
import networkx as nx
from mpl_toolkits.basemap import Basemap
from scipy.stats import ks_2samp as ksd
from shapely.geometry import Point, Polygon, LineString
import geopandas as gp
from scipy.spatial import Voronoi
import matplotlib.colorbar as mplc
import itertools


def calc_ksd(data1, data2):
    
    """alculate the Kolmogorov-Smirnov distance between two sample data1 and data2.
    The data should contain the time series as columns. """
    
    N=data1.shape[1]
    result= [ksd(data1.iloc[:,i], data2.iloc[:,i])[0] for i in range(N)]
    
    return np.array(result)

def corr_len(scf, bin_width):
    
    """Calculate correlation length given the SCF. scf[:,1] should be the correlation
    values and scf[1,1] is for dist=0. 'bin_width' should be the same as used 
    during calculation of the SCF. """
    
    return np.sum(np.abs(scf[1:,1])*bin_width)

def spherical_distance(longlat1, longlat2):
    
    """Great circle distance"""
    
    long1 = np.radians(longlat1[0])
    long2 = np.radians(longlat2[0])
    lat1 = np.radians(longlat1[1])
    lat2 = np.radians(longlat2[1])
    
    R = 6371. # the radius of the earth in km
    
    def hav(theta):
        return (1. - np.cos(theta))/2.
    
    return 2.*R*np.arcsin(np.sqrt(hav(lat2 - lat1)
            + np.cos(lat1)*np.cos(lat2)*hav(long2 - long1)))


def spatial_corr(data, pos, no_avrg=False):
    
    """Calculate the spatial correlogram using data and the corresponding positions.
    data contains time series as columns. Both inputs should be numpy arrays. """
    
    #calculate correlation matrix
    cvals=np.corrcoef(data.T)
    
    #Get indice of lower triangular matrix
    rows, cols=np.tril_indices_from(cvals, k=-1)
    
    #Get array of correlations of all node combinations
    cvals=cvals[rows, cols]
    
    #Add distances of nodes 
    cvals=pd.DataFrame({'correl': cvals, 'dij': spherical_distance(pos[rows].T, 
                                                                   pos[cols].T) })
    if no_avrg==True:
        return cvals
    
    #Cut distances into N_d bins
    N_d=350
    bin_width=cvals.dij.max()/N_d
    dcut=pd.cut(cvals.dij,
                bins=np.linspace(0., cvals.dij.max(),N_d+1))
    
    #Assign bin centers to correlation entries
    dcut=dcut.apply(lambda x: x.mid).astype(np.float)
    cvals=cvals.assign(dist=dcut)
    
    #Average over bins and add correlation value for dist=0
    cvals=cvals.groupby('dist',as_index=False ).mean().drop(['dij'], axis=1).values
    cvals=np.vstack([[0,1],cvals])
    
    return cvals, corr_len(cvals, bin_width)

    
def scale_layout(g_wind, g_solar, load, buses, alpha, gamma=1., return_mis=True):

    """alpha-gamma rescaling of raw power generation time series
    (see Schäfer et al. 2017). alpha is the percentage of wind power in the output."""
    
    # Scaling factors for each country 
    rw=load.mean().groupby(buses.country).sum()/g_wind.mean().groupby(buses.country).sum()
    rw*=alpha*gamma
    rs=load.mean().groupby(buses.country).sum()/g_solar.mean().groupby(buses.country).sum()
    rs*=(1.-alpha)*gamma
    
    # Apply rescaling to each country group of nodes
    g_wind=g_wind.groupby(buses.country, axis=1).apply(lambda x: x*rw.loc[x.name])
    g_solar=g_solar.groupby(buses.country, axis=1).apply(lambda x: x*rs.loc[x.name])
    
    #Return mismatch
    if return_mis:
        return g_wind+g_solar-load
    else:
        return g_solar, g_wind

    

def g_plot(ws, params, label, save_fig=False, path=''):
    
    """Plot routine for evaluation of Uncorrelated-homogeneous injection"""
    
    plt.figure(figsize=(12,5))
    

    plt.subplot(1,2,1)
    for i in range(len(ws)):
        ne= ws[i].shape[0]
        
        p_label=''
        for plab in params.keys():
            p_label=p_label +plab + '={}'.format(params[plab][i])+ ' '
            
        plt.plot(np.arange(1.,ne+1) / ne,
                 np.cumsum(ws[i]) /np.sum(ws[i]), '.-', label=p_label)
        plt.xlabel(r'$\tilde K/$ N')
        plt.ylabel(
                r'$\sum_{k=1}^{\tilde K} 1/\mu_k \cdot ( \sum_{k=1}^{N} 1/\mu_k )^{-1}$ ')
        plt.legend()
        #plt.xscale('log')
   
    plt.subplot(1,2,2)
    for i in range(len(ws)):
        ne= ws[i].shape[0]
        
        p_label=''
        for plab in params.keys():
            p_label=p_label +plab + '={}'.format(params[plab][i])+ ' '
        
        plt.plot( np.arange(1.,ne+1),
                 ws[i] /np.sum(ws[i]), '.-', label=p_label)
        plt.ylabel(r'$1/\mu_k \cdot ( \sum_{k=1}^{N} 1/\mu_k )^{-1}$ ')
        plt.xlabel(r'k')
        plt.xscale('log')
        plt.legend()
        
    plt.tight_layout()
    if save_fig:
        plt.savefig(path + label + '.pdf') 

def plot_cbar(vmax, fig, axes, cmap='RdBu_r', orientation='vertical'):
    
    """Plot colorbar to a figure using "cmap" and clim=[-vmax,vmax] """
    
    
    sm = plt.cm.ScalarMappable(cmap=cmap,
                               norm=plt.Normalize(vmin=-vmax, vmax=vmax))
    sm.set_array([])
 

    if orientation=='vertical':
        cax,kw = mplc.make_axes([ax for ax in axes.flat],
                                        orientation='vertical',
                                        pad=0.01, aspect=100)
    elif orientation=='horizontal':
        cax,kw = mplc.make_axes([ax for ax in axes.flat],
                                        orientation='horizontal',
                                        pad=0.01, aspect=100)
    else:
        print 'No such orientation!!!'
        
    fig.colorbar(sm, cax=cax, **kw)

    
    if orientation=='vertical':
        ticks=cax.get_yticklabels()
        ticks[0].set_text(r'$<$ -{}'.format(vmax))
        ticks[-1].set_text(r'$>$ {}'.format(vmax))
        cax.set_yticklabels(ticks) 
    elif orientation=='horizontal':
        ticks=cax.get_xticklabels()
        ticks[0].set_text(r'$<$ -{}'.format(vmax))
        ticks[-1].set_text(r'$>$ {}'.format(vmax))
        cax.set_xticklabels(ticks) 
    
    return cax
    
def compute_bbox_with_margins(margin, x, y):

    """Compute a box around positions x,y with certain margin"""
    
    pos = np.asarray((x, y))
    minxy, maxxy = pos.min(axis=1), pos.max(axis=1)
    xy1 = minxy - margin*(maxxy - minxy)
    xy2 = maxxy + margin*(maxxy - minxy)
    
    return tuple(xy1), tuple(xy2)


def eu_map(network, ax, linewidth=0.25, fix_aspect=True, resolution='l'):
    
    """Draw a map given the positions of the nodes of 'network'. Linewidth
    of borders and coastlines can be adapted."""
    
    (x1, y1), (x2, y2) = compute_bbox_with_margins(0.05,
                                                   network.buses.x,
                                                   network.buses.y)
    
    bmap = Basemap(resolution=resolution, epsg=network.srid,
               llcrnrlat=y1, urcrnrlat=y2, llcrnrlon=x1,
               urcrnrlon=x2, ax=ax, fix_aspect=fix_aspect)
    bmap.drawcountries(linewidth=linewidth, zorder=-1)
    bmap.drawcoastlines(linewidth=linewidth, zorder=-1)
    
    
    return bmap


def make_rand_inj(N, method='bendel', dist='uniform'):
    
    """Create random covariance matrix"""
    
    if method=='bendel':
        
        #Create random orthonormal matrix
        u=sst.ortho_group.rvs(dim=N)
        
        #Create random eigenvalues with distribution 'dist'
        if dist=='uniform':
            d=np.diag(np.random.rand(N))
        elif dist=='lognormal':
            d=np.diag(np.random.lognormal(1,1,N))
        else:
            print 'No such distribution'
        
        #return U*D*U^T
        return u, d, np.dot(u, np.dot(d, u.T) )
    
    elif method=='wishart':
        
        #Draw random data matrix X and return covariance matrix X*X^T
        x=np.random.randn(N,N)
        return np.dot(x, x.T)
    
    else:
        print 'No such method for rand. cov. matrices'


def get_ptdf(pypsa_net):
    
    """Get ptdf from a pypsa network instance """
    
    elist=pypsa_net.lines.loc[:,['bus1', 'bus0']].values
    nlist=pypsa_net.buses.index.values
    graph=pypsa_net.graph()
    
    K=nx.incidence_matrix(graph,
                          oriented=True,
                          edgelist=elist,
                          nodelist=nlist).toarray()
    
    Li=sl.pinvh(nx.laplacian_matrix(graph,nodelist=nlist).toarray())
    
    return pd.DataFrame(data=np.dot(K.T, Li),
                        index=pypsa_net.lines.index.values,
                        columns=nlist)
    
def choose_lines(net,param, nlines, method='polygon', randomize=True):
    
    """
    Choose lines by using different methods

    Parameters
    ----------
    net : pypsa.network    
    method : string
        One of the methods listed under 'param'
    param : list
        Paramter(s) of the method:
            -sp_eve: Integer index of injection PC and step between chosen
                     node indice
            -circle: list of central buses [bus0, bus1]
            -polygon: Polygon0 and Polygon1 as a list of (x,y) tuples and
                      axis for sorting (either 'x' or 'y')
            -line: list of (x,y) tuples describing the Line(segments) 

    Returns
    -------
    buses0, buses1 OR chosen_lines: numpy.ndarray
        Two arrays containing the indice of the buses at the ends of the 
        chosen lines OR the indice of the chosel lines
        
    """
    
    if method=='sp_eve':
        
        if net.sp_eve.size==0:
            net.calc_sp().calc_peig()
        
        pc_index, step=param
        
        peig=pd.Series(data=net.sp_eve[:,pc_index], index=net.buses.index)

        buses0= peig[peig>0].sort_values(ascending=False).index.get_values()
        buses1= peig[peig<=0].sort_values().index.get_values()
        
        buses0=buses0[::step][:nlines]
        buses1=buses1[::step][:nlines]
        
        return buses0, buses1
        
    if method=='circle':
        
        bus0, bus1=param
        
        dist0=spherical_distance(net.buses.loc[:,['x','y']].values.T,
                                 net.buses.loc[bus0,['x','y']].values)
        dist1=spherical_distance(net.buses.loc[:,['x','y']].values.T,
                                 net.buses.loc[bus1,['x','y']].values)
        
        buses0=(net.buses[dist0<150]).sample(frac=1).index.get_values()
        buses1=(net.buses[dist1<100]).sample(frac=1).index.get_values()
        
        buses0=buses0[:nlines]
        buses1=buses1[:nlines]
        
        return buses0, buses1
    
    if method=='polygon':
        
        poly0, poly1, ax= Polygon(param[0]), Polygon(param[1]), param[2]
        
        list0=[poly0.contains(Point((x,y))) for (x,y) in net.buses.loc[:,['x','y']].values]
        list1=[poly1.contains(Point((x,y))) for (x,y) in net.buses.loc[:,['x','y']].values]
        
        buses0=net.buses[list0].sort_values(by=ax)
        buses1=net.buses[list1].sort_values(by=ax)
        
        try:
            buses0=buses0.groupby(pd.cut(buses0.loc[:,ax], nlines)).apply(lambda f: f.iloc[[f.shape[0]/2]])
            buses0.index=buses0.index.droplevel()
        except(IndexError):
            buses0=buses0.iloc[np.linspace(0,buses0.shape[0]-1,nlines,dtype=np.int)]
        try: 
            buses1=buses1.groupby(pd.cut(buses1.loc[:,ax], nlines)).apply(lambda f: f.iloc[[f.shape[0]/2]])
            buses1.index=buses1.index.droplevel()
        except(IndexError):
            buses1=buses1.iloc[np.linspace(0,buses1.shape[0]-1,nlines,dtype=np.int)]

        shuffle_ind=np.arange(nlines)
        if randomize:
            np.random.shuffle(shuffle_ind)
        
        buses0=buses0.iloc[shuffle_ind].index.get_values()
        buses1=buses1.iloc[shuffle_ind].index.get_values()
                
        return buses0, buses1
    
    if method=='line':
        
        line=LineString(param)
        
        buses01=net.lines.loc[:,'bus0':'bus1']
        
        net_lines=[LineString( [(net.buses.loc[b0,'x'],
                                 net.buses.loc[b0,'y']),
                                (net.buses.loc[b1,'x'],
                                 net.buses.loc[b1,'y'])] ) for b0,b1 in buses01.values]
        net_lines=gp.GeoSeries(data=net_lines, index=net.lines.index)
        
        mask=net_lines.intersects(line)
        buses01=buses01[mask]
        chosen_lines=net_lines[mask]
        
        g=net.graph()
        mask=[(g.degree(b0)>1 and g.degree(b1)>1) for b0, b1 in buses01.values]
        chosen_lines=chosen_lines[mask]
        
        chosen_lines=chosen_lines.sample(nlines).index.get_values()  
        
        return chosen_lines


def plot_eve_overlap(eva1, eve1, eva2, eve2, ax=None, label=True,n=10):

    ov=np.dot(np.expand_dims(eva1,0).T, np.expand_dims(eva2,0))
    ov=ov*(np.dot(eve1.T, eve2))**2
    ov=ov/ov.sum()

    M=eve1.shape[0]
    ind= np.array(list(itertools.product(np.arange(M), np.arange(M))))
    ind=ind[ov.flatten().argsort()][::-1]
    
    sort_ov=ov[ind[:,0],ind[:,1]]
    
    if ax is None:
        ax=plt.gca()
    
    ax.bar(np.arange(n), sort_ov[:n],
            tick_label=[str((i+1,j+1)) for i,j in ind[:n]])
    if label:
        ax.set_ylabel(r'$O_{nk} \left( \sum O_{nk} \right)^{-1}$', fontsize=15)
    

def finite_voronoi(ps_net, radius=None):
   
    """
    Construct finite voronoi cells for the nodes of a PyPSA network

    Parameters
    ----------
    ps_net : pypsa.network
        Network, for which the voronoi cells are constructed
    radius : float, optional
        Distance to 'points at infinity'.

    Returns
    -------
    vor_cells : geopandas.GeoDataFrame
        The frame contains the geometry and the node belonging to each cell.
        
    """
    
    #Construct standard voronoi cells 
    vor=Voronoi(ps_net.buses.loc[:,['x','y']].values)


    #----Reconstruct voronoi regions to finite cells----#

    new_regions = []
    new_vertices = vor.vertices.tolist()

    center = vor.points.mean(axis=0)
    if radius is None:
        radius = vor.points.ptp().max()

    # Construct a map containing all ridges for a given point
    all_ridges = {}
    for (p1, p2), (v1, v2) in zip(vor.ridge_points, vor.ridge_vertices):
        all_ridges.setdefault(p1, []).append((p2, v1, v2))
        all_ridges.setdefault(p2, []).append((p1, v1, v2))

    # Reconstruct infinite regions
    for p1, region in enumerate(vor.point_region):
        vertices = vor.regions[region]

        if all(v >= 0 for v in vertices):
            # finite region
            new_regions.append(vertices)
            continue

        # reconstruct a non-finite region
        ridges = all_ridges[p1]
        new_region = [v for v in vertices if v >= 0]

        for p2, v1, v2 in ridges:
            if v2 < 0:
                v1, v2 = v2, v1
            if v1 >= 0:
                # finite ridge: already in the region
                continue

            # Compute the missing endpoint of an infinite ridge

            t = vor.points[p2] - vor.points[p1] # tangent
            t /= np.linalg.norm(t)
            n = np.array([-t[1], t[0]])  # normal

            midpoint = vor.points[[p1, p2]].mean(axis=0)
            direction = np.sign(np.dot(midpoint - center, n)) * n
            far_point = vor.vertices[v2] + direction * radius

            new_region.append(len(new_vertices))
            new_vertices.append(far_point.tolist())

        # sort region counterclockwise
        vs = np.asarray([new_vertices[v] for v in new_region])
        c = vs.mean(axis=0)
        angles = np.arctan2(vs[:,1] - c[1], vs[:,0] - c[0])
        new_region = np.array(new_region)[np.argsort(angles)]

        # Finish 
        new_regions.append(new_region.tolist())
    
    regions=new_regions
    vertices=np.asarray(new_vertices)  
    
    #--------------------------------------------------#
    
    #Convert cells to GeoDataFrame
    vor_cells=gp.GeoSeries([Polygon(vertices[region]) for region in regions])
    vor_cells=gp.GeoDataFrame({'geometry':vor_cells,
                               'node':list(ps_net.buses.index) })
    
    return vor_cells
    

#%%

def make_region_mask(ps_net, map_path):
    
    """ 
    Create a region mask for a PyPSA network. 
    
    Parameters
    ----------

    ps_net : pypsa.Network 
        Network that define the region of the mask
    map_path : str 
        This path should contain a map, that covers the network. The map data 
        should be importable by geopandas and should consist of polygons. Suitable
        Maps could e.g. be obtained at https://geojson-maps.ash.ms
        
    Returns
    -------
    
    mask : geopandas.GeoDataFrame
        Region mask comprising one (Multi-)Polygon. The mask consists of polygons
        from the map, that intersect with the network nodes. 
    
    """
    
    #Import map from map_path  
    large_map= gp.read_file(map_path).geometry
    
    #Extract all single Polygons and transform them to GeoDataFrame
    map_polys=np.hstack(large_map.values)
    map_polys=gp.GeoDataFrame({'geometry':map_polys})
    
    #Create GeoDataFrame from the network nodes
    N=ps_net.buses.shape[0]
    nodes=[Point(ps_net.buses.iloc[i].loc[['x','y']]) for i in range(N)]
    nodes=gp.GeoDataFrame({'geometry':nodes})
    
    #Extract the map polygons, that intersect with a node
    mask=gp.sjoin(map_polys, nodes)
    
    #Combine all single Polygons to one (Multi-)Polygon
    mask=gp.GeoDataFrame({'geometry':[mask.geometry.unary_union]})
    
    return mask
    
    
    
    
    
    
    
    
        
        
        