#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 11:00:57 2018

@author: adminuser

Network models

"""

import networkx as nt
import scipy.linalg as sl
import numpy as np
import net_model_constr as nmc


#%% Network Model Class


class NetModel:
     
    _net_methods={'grid_2d': nmc.grid_2d,
                 'grid_1d': nmc.grid_1d,
                 'barabasi_albert': nmc.barabasi_albert,
                 'watts_strogatz': nmc.watts_strogatz,
                 'complete': nmc.complete_graph,
                 'g_nm': nmc.g_nm_graph
    
            }
    
    model_params={'grid_2d': ['N'],
                 'grid_1d': ['N'],
                 'barabasi_albert': ['N', 'c'],
                 'watts_strogatz': ['N','k','p'],
                 'complete': ['N'],
                 'g_nm': ['N','L']
            }
    
    def __init__(self, model_name, params ):
        self.name=model_name
        self.params=params
        self.network= self._net_methods[self.name](self.params)
        self.hth_eva=[]
        self.hth_eve=[]  
        self.hth=sl.pinvh( nt.laplacian_matrix(self.network).toarray() )
    
    def calc_spec(self):
        if len(self.hth_eva)==0:
            w, v =sl.eigh(self.hth)
            self.hth_eva, self.hth_eve = np.sort(w),v[:,np.argsort(w)]
        return self
    
    def average(self,nsamples):
        w=0
        for i in range(nsamples):
            self.network= self._net_methods[self.name](self.params)
            w= w + np.sort( nt.laplacian_spectrum(self.network) )
        
        self.hth_eva =  np.append([0], 1./ (1.*w / nsamples)[1:][::-1]) 
        return self

    def save(self, data_frame):
        pass





    
    