#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 15:04:42 2018

@author: adminuser

BaFloNet and Coarse-graining algorithm

"""

import pypsa as ps
import pandas as pd
import scipy.linalg as sl
import numpy as np
import lib.help_tools as ht
import geopandas as gp

#%% 

class BaFloNet(ps.Network):
    
    """Class to calculate power balancing and linear power flow on a network
    given the load and generation. PCA is implemented for evaluation."""
    
    def __init__(self):
        
        super(BaFloNet, self).__init__()
        self.folder=None
        self.mis=pd.DataFrame()
        self.load=pd.DataFrame()
        self.ptdf=pd.DataFrame()
        self.regions=gp.GeoDataFrame()
        self.sf=pd.DataFrame()
        self.sp=pd.DataFrame()
        self.sm=pd.DataFrame()
        self.hth=np.array([])
        self.sp_eve=np.array([]) 
        self.sp_eva=np.array([])
        self.sf_eve=np.array([]) 
        self.sf_eva=np.array([])
        self.m_eve=np.array([])
        self.hth_eve=np.array([])
        self.hth_eva=np.array([])
        self.sm_eve=np.array([]) 
        self.sm_eva=np.array([])
       
    def load_top(self, folder=None, ptdf=True):
        
        """Load the network Topology and set data path"""
        
        #Set path for source directory 
        if folder is not None:
            self.folder=folder
        else:
            self.folder='/home/adminuser/Dokumente/Master_Arbeit/' + \
            'Code/EU_grid_model/Results/data/re-eu_original' 
           
        #Import pypsa network                                   
        self.import_from_csv_folder(self.folder)
        
        #Import ptdf matrix
        try:
            self.ptdf=pd.read_pickle(self.folder + '/ptdf.gzip')
        except IOError: 
            self.ptdf=ht.get_ptdf(self)

        #import nodal regions
        self.regions=gp.read_file(self.folder+'/node_regions.geo.json')
        
        return self
        
    def load_dir(self, alpha=0.8, ptdf=True, load_mismatch=False):

        """Import time series from folder"""
        
        #Import load time series
        self.load=pd.read_pickle(self.folder + '/load.gzip')
        self.load.index=pd.to_datetime(self.load.index )  

        #Import mismatch or import raw generation data and rescale its layout
        if load_mismatch:
            self.mis=pd.read_pickle(self.folder + '/mismatch.gzip')
            self.mis.index=pd.to_datetime(self.mis.index )   
        else:
            g_wind=pd.read_pickle(self.folder + '/g_wind.gzip')
            g_solar=pd.read_pickle(self.folder + '/g_solar.gzip')
            self.mis=ht.scale_layout(g_wind, g_solar, self.load, self.buses, alpha)
            self.mis.index=pd.to_datetime(self.mis.index )    
        
        return self
    
    def load_dat(self, g_wind, g_solar, load):

        """Import one or more time series as data frames. Generation time series
        already have to be scaled with alpha."""
    
        self.load=load
        self.load.index=pd.to_datetime(self.load.index )  
        
        self.mis= g_wind+ g_solar - load
        self.mis.index=pd.to_datetime(self.mis.index )    
        
        return self
    
    def balance(self, scheme="syn"):
        
        inj=pd.DataFrame()
        
        if scheme=='syn':
            #synchronized balancing
            bal=self.load.mean().to_frame().dot(self.mis.sum(axis=1).to_frame().T).T 
            bal=bal/self.load.mean().sum()
            
        elif scheme=='uni':
            #uniform balancing
            a=np.ones((self.buses.shape[0],self.buses.shape[0]))/self.buses.shape[0]
            bal=np.dot(a,self.mis.values.T).T
            
        elif scheme=='one':
            #one-takes-it-all balancing
            a=np.zeros((self.buses.shape[0],self.buses.shape[0]))
            a[0]=1.
            bal=np.dot(a,self.mis.values.T).T
            
        else:
            print 'No such balancing scheme available'
            
        inj=self.mis - bal
        self.import_series_from_dataframe(-inj,'Load', 'p_set')
        
        return self
    
    def rplot(self, r_weights, ax=None, basemap=True, alpha=1, cmap='RdBu_r',
              vmin=None, vmax=None, bus_sizes=0, legend=False,zorder=-1, title=''):
    
        """Plot regions with regional weights """
        
        regions=self.regions.assign(weight=r_weights)
        regions.plot(ax=ax, column='weight', alpha=alpha, vmin=vmin, vmax=vmax,
                     cmap=cmap, legend=legend, zorder=zorder)
                
        self.plot(ax=ax,basemap=basemap,bus_sizes=bus_sizes, line_widths=0, title=title)
        
        return self
    
    def qplot(self, flows, quant=0, ax=None, basemap=True, line_colors='g',
              bus_sizes=0., line_widths=2500, title=''):
        
        """Plot upper quantile of flows """
        
        if type(flows)==np.ndarray:
            flows=pd.Series(data=flows, index=self.lines.index)
            
        flows=flows.where(flows >= flows.quantile(quant),0)

        self.plot(ax=ax,basemap=basemap,bus_sizes=bus_sizes,
                  line_widths=line_widths, flow=flows/flows.abs().max(),
                  line_colors=line_colors, title=title)  

        return self
    
    def add_line(self, bus0, bus1, name, calc_dist=False, up_ptdf=True):
        
        """Add Line to pypsa network and update the PTDFs """
        
        if calc_dist:
            dist=ht.spherical_distance(self.buses.loc[bus0].loc[['x','y']].values, 
                                       self.buses.loc[bus1].loc[['x','y']].values)
        else:
            dist=0
            
        new_line=pd.DataFrame({'bus0':[bus0], 'bus1':[bus1], 'length': [dist],
                               'x':[1.], 'name':[name]})
        new_line=new_line.set_index('name')
        
        
        self.import_components_from_dataframe(new_line,'Line')
        if up_ptdf:
            self.update_ptdf()
        
        return self
    
    def update_ptdf(self):
        self.ptdf=ht.get_ptdf(self)
    
    def calc_sp(self):
        self.sp= (-self.loads_t.p_set).cov()        
        return self
    
    def calc_sf(self):
        self.sf= self.ptdf.dot(self.sp.dot(self.ptdf.T))
        return self
    
    def calc_sm(self):
        self.sm= self.mis.cov()
        return self
    
    def calc_hth(self):
        self.hth= self.ptdf.T.dot(self.ptdf).values
        return self
    
    def calc_htheig(self):
        self.hth_eva, self.hth_eve= sl.eigh(self.hth)
        self.hth_eva, self.hth_eve= self.hth_eva[::-1], self.hth_eve[:,::-1]    
        return self
    
    def calc_peig(self):
        self.sp_eva, self.sp_eve= sl.eigh(self.sp)
        self.sp_eva, self.sp_eve= self.sp_eva[::-1], self.sp_eve[:,::-1]
        return self

    def calc_feig(self):
        self.sf_eva, self.m_eve= sl.eig(np.dot(self.sp, self.hth))
        self.sf_eve= np.dot(self.ptdf.values,self.m_eve) 
        
        self.m_eve= self.m_eve[:, np.argsort(np.real(self.sf_eva))][:,::-1]
        self.sf_eve= self.sf_eve[:, np.argsort(np.real(self.sf_eva))][:,::-1]
        self.sf_eve= self.sf_eve/ np.sqrt((self.sf_eve**2).sum(axis=0))
        self.sf_eve, self.m_eve= np.real(self.sf_eve), np.real(self.m_eve)
        self.sf_eva= np.sort(np.real(self.sf_eva))[::-1]
        return self
    
    def calc_meig(self):
        self.sm_eva, self.sm_eve= sl.eigh(self.sm)
        self.sm_eva, self.sm_eve= self.sm_eva[::-1], self.sm_eve[:,::-1]
        return self
    
    def calc_eig(self):
        self.calc_sp().calc_sf().calc_hth()
        self.calc_peig().calc_feig().calc_htheig()
        return self
    
    
    
#%% 
        
def coarse_grain(old_net,dx=1.,nbin=0):
    
    #old bus coordinates
    old_nodes=old_net.buses[['x','y']]
      
    if nbin!=0: 
        #create bin edges with number of bins given (rectangular divisions)
        x_ed=np.linspace(old_nodes.x.min()-1e-6,old_nodes.x.max(),nbin+1)
        y_ed=np.linspace(old_nodes.y.min()-1e-6,old_nodes.y.max(),nbin+1)

    else:
        #create bin edges with bin width dx (square divisions)
        x_ed=np.arange(old_nodes.x.min()-1e-4,old_nodes.x.max()+1.*dx,1.*dx)
        y_ed=np.arange(old_nodes.y.min()-1e-4,old_nodes.y.max()+1.*dx,1.*dx)
    
    #Find bin to which each bus belongs to
    xc=pd.cut(old_nodes.x, bins=x_ed)
    yc=pd.cut(old_nodes.y, bins=y_ed)
    old_nodes=old_nodes.assign(bin_label=zip(xc,yc))
    
    # Make a new network
    new_net=BaFloNet()
    
    #Calc the mean x,y coordinate in each bin and drop empty bins
    new_nodes=old_nodes.groupby('bin_label', as_index=False).mean().dropna()
    
    #Add new nodes to new network
    new_net.import_components_from_dataframe(new_nodes.iloc[:,1:], 'Bus')
    
    #Add lines in new network
    j=0
    old_graph=old_net.graph()
    for old1,old2,name in old_graph.edges: 
        
        #Get bin of old nodes and extract bin name
        bin1=new_nodes.loc[new_nodes.bin_label==old_nodes.loc[old1].bin_label]
        bin2=new_nodes.loc[new_nodes.bin_label==old_nodes.loc[old2].bin_label]
        new1, new2 = str(bin1.index.item()), str(bin2.index.item())
        
        #Add line between bin 1 and 2 when they are connected in old network
        if new1!=new2 and not new_net.graph().has_edge(new1, new2):
            l=ht.spherical_distance(bin1.loc[:,'x':'y'].values[0],
                                    bin2.loc[:,'x':'y'].values[0])
            new_net.add('Line', '{}'.format(j), bus0=new1, bus1=new2, length=l)
            j+=1
    
    #import additional properties to new network
    new_net.lines.x=np.ones(len(new_net.lines))
    new_load_buses=pd.DataFrame(data=list(new_nodes.index), 
                                index=new_nodes.index, columns=['bus'])
    new_net.import_components_from_dataframe(new_load_buses, 'Load')
    new_net.set_snapshots(old_net.snapshots)
    new_net.ptdf= ht.get_ptdf(new_net)
    
    #Aggregate mismatch / load and adjust columns
    new_net.mis= old_net.mis.T.groupby(old_nodes.bin_label, as_index=False).sum().T
    new_net.load= old_net.load.T.groupby(old_nodes.bin_label, as_index=False).sum().T
    new_net.mis.columns=new_net.mis.columns.map(str)
    new_net.load.columns=new_net.load.columns.map(str)
    
    return new_net, (x_ed, y_ed)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

