#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 13:13:45 2018

@author: adminuser

Constructors for Network models in "NetModel"

"""


import networkx as nt
from datetime import datetime
import numpy as np


def grid_2d(params):
    n=int( np.sqrt(params) )
    return  nt.grid_2d_graph(n, n)

def grid_1d(params):
    if type(params)=='list':
        return nt.grid_graph(dim=[params[0]], periodic=params[1])
    else: 
        return nt.grid_graph(dim=[params], periodic=True)

def barabasi_albert(params):
    return nt.barabasi_albert_graph(n=params[0],
                                    m=params[1], seed=datetime.now() )

def watts_strogatz(params):
    return nt.newman_watts_strogatz_graph(n=params[0],
                                          k=params[1],
                                          p=params[2], seed=datetime.now())

def complete_graph(params):
    return nt.complete_graph(n=params)

def g_nm_graph(params):
    g=nt.gnm_random_graph(n=params[0],m=params[1], seed=datetime.now())
    while nt.is_connected(g)==False:
        g = nt.gnm_random_graph(n=params[0],
                                m=params[1], seed=datetime.now())
    return g

#def exponential_graph(n=64,deg=3):
#    degrees=np.round(np.random.exponential(scale=deg, size=n) )
#    degrees=degrees[degrees.nonzero()]
#    return nt.expected_degree_graph(degrees, selfloops=False, seed=datetime.now())


