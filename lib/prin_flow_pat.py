#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 13:28:57 2018

@author: adminuser

PrinFlow Class

"""

import scipy.linalg as sl
import numpy as np
import pandas as pd
import net_model as nm
import observables as ob
from datetime import datetime
from sklearn.datasets import make_spd_matrix
import scipy.stats as sst

class PrinFlow:
    
    """Class to examine principal components of linear flow on a network model """
    
    def __init__(self, injec_cov, net_model, name):
        self.name=name
        self.p=injec_cov
        self.model=net_model
        self.phth= np.dot(injec_cov, net_model.hth)
        self.phth_eva=[]
        self.phth_eve=[]
        self.p_eva=[]
        self.p_eve=[]
        self.obs=pd.DataFrame(index=[name],columns=['q50_abs',
                                                 'q50_rel',
                                                 'q95_abs',
                                                 'q95_rel',
                                                 'd_b',
                                                 'trans_cap'])
    
        
    def calc_phth_eig(self):
        w,v = sl.eig(self.phth)
        self.phth_eva, self.phth_eve = np.sort(np.real(w)), v[:,np.argsort(w)]
        return self
        
    def calc_p_eig(self):
        w,v = sl.eigh(self.p)
        self.p_eva, self.p_eve = np.sort(w), v[:,np.argsort(w)]
        return self
        
    def calc_all(self):
        self.calc_phth_eig()
        self.calc_p_eig()
        self.obs[['q50_abs', 'q50_rel']]= ob.q50(self.phth_eva[1:][::-1])
        self.obs[['q95_abs', 'q95_rel']]= ob.q95(self.phth_eva[1:][::-1])
        self.obs['d_b']=ob.dist(self.p_eve, self.model.calc_spec().hth_eve)
        self.obs.trans_cap=np.sqrt( self.phth_eva.sum() )/ self.p.diagonal().sum()
        return self
    
    def save(self, data_frame=[], phth_eva_frame=[]):
        if len(data_frame)!=0:
            data_frame.loc[self.name]=self.obs.loc[self.name]
        if len(phth_eva_frame)!=0:
            phth_eva_frame.loc[self.name]=self.phth_eva
        
        


