#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 15:09:33 2018

@author: adminuser

Time series Manipulation

"""

import numpy as np
from scipy.stats import norm
from statsmodels.distributions.empirical_distribution import ECDF
import pandas as pd
import scipy.linalg as sl
import scipy.stats as sst

#Empirical cumulative distribution function, using 1/(N+1) instead of 1/N
ecdf=lambda x: len(x)*ECDF(x)(x)/(len(x)+1) 
#Inverse ECDF using linear interpolation on ecdf(xdata)
iecdf=lambda x, xdata: np.interp(x, ecdf(np.sort(xdata)), np.sort(xdata)) 


class NormalCopula():
    
    """Class to fit and to sample from a normal copula. The input is a pandas 
    DataFrame. The columns should always contain the time series. """
    
    def __init__(self):
        self.cov=np.array([])
        self.N=0
        
    def estimate_cov(self, data):
        
        """Estimate the covariance of the normal copula given the data"""
        
        data=data.values
        
        data=norm.ppf( np.array([ ecdf(x) for x in data.T ]).T )
        self.cov=np.cov(data, rowvar=False)
        self.N=self.cov.shape[0]
        
        return self
        
    def sample_joint_dist(self, data, zero_mean=False):
        
        """Sample the joint distribution defined by this normal copula and
        the marginal distributions from data"""
        
        col=data.columns
        ind=data.index
        data=data.values
        
        #Sampling from copula and margins
        z=np.random.multivariate_normal(np.zeros(data.shape[1]),
                                        self.cov, data.shape[0],
                                        check_valid='raise')
        z=np.array([iecdf(norm.cdf(z[:,i]),x) for i,x in enumerate(data.T)]).T
        
        #Center the new time series
        if zero_mean:
            z= z-z.mean(axis=0).sum()/z.shape[1]
        
        return pd.DataFrame(z, columns=col, index=ind)
    
    def scale_cov(self, d):
        
        """Rescale the copula covariance matrix. 'd' should be a scalar between 0 and 1"""

        self.cov=np.diag(np.ones(self.N))+d*np.tril(self.cov,-1)+d*np.triu(self.cov,1)
        
        return self
    
        
def scale_var(data, s):
    
    """Scale the variance of a time series with factor 's'. 'data' should 
    contain the time series as columns."""
    
    return s*(data-data.mean()) + data.mean()


class MaPriCo():

    """Class to change statistical properties of time series by manipulating their
    principal components"""
    
    def __init__(self, time_series):
        
        self.ts=time_series
        self.N=time_series.shape[1]
        self.pvar=np.array([])
        self.pcom=np.array([])
    
    def pca(self):
        
        self.pvar, self.pcom = sl.eigh(self.ts.cov())
        return self
    
    def import_pc(self, pvar, pcom):
        
        self.pvar, self.pcom = pvar, pcom
        return self
    
    def new_cov(self,k):
        
        if k>1:
            u=sst.ortho_group.rvs(dim=(self.N-k) )
            u=sl.block_diag(np.diag(np.ones(k)), u)
            u=np.dot(self.pcom, u)
        else:
            u=sst.ortho_group.rvs(dim=self.N )
        
        return pd.DataFrame( np.dot(u, np.dot(np.diag(self.pvar), u.T)),
                            index=self.ts.columns, columns= self.ts.columns)
    
    def new_ts(self,k):
        
        if k>1:
            u=sst.ortho_group.rvs(dim=(self.N-k) )
            u=sl.block_diag(np.diag(np.ones(k)), u)
            u=np.dot(self.pcom, u)
        else:
            u=sst.ortho_group.rvs(dim=self.N )
        
        alpha=np.dot(self.ts.values, self.pcom)
        
        return pd.DataFrame( np.dot(alpha, u.T ),
                            index=self.ts.index, columns= self.ts.columns)
    
        
               
    
    
    
    
    
    
    
    
    
    
    
    