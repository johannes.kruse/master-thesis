# -*- coding: utf-8 -*-

#Check how majorization could be applied to flow and injection PC-eigenvalues

import lib.bal_flow_net as bfn
import numpy as np
import matplotlib.pyplot as plt
import glob
import lib.observables as ob

path= './EU_grid_model/Results/majorization/'

#%% Load original network and coarse grain it 

orig_net=bfn.BaFloNet().load_top().load_dir()
net=bfn.coarse_grain(orig_net, nbin=64)[0]

#%% Calculate eigenvalues

net.balance().calc_eig()
N=net.buses.shape[0]

# !!! In the following figures we assume !!!:
# Sigma_P, H^TH are normalized by their individual traces
# and Sigma_F is normalized by the product Tr[Sigma_P]*Tr[H^TH]
# Only \tilde\lambda identifies unnormalized eigenvalues 

#%% 

plt.figure(figsize=(8,5))
plt.plot(np.cumsum(net.sp_eva)/net.sp_eva.sum(), '.-',
         label=r'$\lambda(\Sigma^P)$',
         markersize=12)
plt.plot(np.cumsum(net.sf_eva)/net.sf_eva.sum(), '.-',
         label=r'$\lambda(\Sigma^F)$', markersize=5)
plt.plot(np.cumsum(net.hth_eva)/net.hth_eva.sum(), '.-',
         label=r'$\lambda(H^TH)$', markersize=5)
plt.legend(fontsize=20)
plt.xlabel(r'$k$', fontsize=20)
plt.ylabel(r'$\sum_{i=1}^{k} \lambda^{\downarrow}_i \cdot ( \sum_{i=1}^{N} \lambda_i )^{-1}$ ',
           fontsize=20)

plt.savefig(path+'1_N{}.pdf'.format(N))

#%%

plt.figure(figsize=(8,5))

norm=net.sp_eva.sum()*net.hth_eva.sum()

plt.plot(np.cumsum( np.sort(net.sp_eva*net.hth_eva)[::-1])/norm, ':',
         label=r'$\lambda(\Sigma^P)^{\uparrow} \circ \lambda(H^TH)^{\uparrow}$',
         linewidth=3,c='#1f77b4')
plt.plot(np.cumsum(np.sort(net.sp_eva*net.hth_eva[::-1])[::-1])/norm, '--',
         label=r'$\lambda(\Sigma^P)^{\uparrow} \circ \lambda(H^TH)^{\downarrow}$',
         linewidth=3,c='#1f77b4')
plt.plot(np.cumsum(net.sf_eva/norm), '-',
         label=r'$\lambda(\Sigma^F)$',
         linewidth=4,c='#1f77b4')
plt.fill_between(np.arange(N),np.cumsum( np.sort(net.sp_eva*net.hth_eva)[::-1])/norm,
                 np.cumsum(np.sort(net.sp_eva*net.hth_eva[::-1])[::-1])/norm,
                 color='lightblue')

plt.yscale('log')
plt.ylim([0.5e-5,2e-1])
plt.xlim([-10,N])
plt.tick_params(labelsize=16)
plt.legend(fontsize=20)
plt.xlabel(r'$k$', fontsize=20)
plt.ylabel(r'$\sum_{i=1}^{k} \lambda^{\downarrow}_i$', fontsize=20)

plt.savefig(path+'2_N{}.pdf'.format(N))

#%%

plt.figure(figsize=(8,5))

plt.plot(np.cumsum(net.sp_eva*net.hth_eva)/(net.sp_eva*net.hth_eva).sum(), '.-',
         label=r'$\lambda(\Sigma^P)^{\uparrow} \circ \lambda(H^TH)^{\uparrow}$',
         markersize=3)
plt.plot(np.cumsum(net.sp_eva*net.hth_eva[::-1])/(net.sp_eva*net.hth_eva[::-1]).sum(),
         '.-',
         label=r'$\lambda(\Sigma^P)^{\uparrow} \circ \lambda(H^TH)^{\downarrow}$',
         markersize=3)
plt.plot(np.cumsum(net.sf_eva/net.sf_eva.sum()), '.-',
         label=r'$\lambda(\Sigma^F)$',
         markersize=3)

plt.legend(fontsize=20)
plt.xlabel(r'$k$', fontsize=20)
plt.ylabel(r'$\sum_{i=1}^{k} \lambda^{\downarrow}_i\cdot ( \sum_{i=1}^{N} \lambda_i )^{-1}$',
           fontsize=20)

plt.savefig(path+'3_N{}.pdf'.format(N))

#%%
plt.figure(figsize=(11,6))

plt.subplot(1,2,1)

plt.plot(net.sp_eva[:20]/net.sp_eva.sum(), '.-',
         label=r'$\lambda(\Sigma^P)^{\downarrow}$')
plt.plot(net.hth_eva[:20]/net.hth_eva.sum(), '.-',
         label=r'$\lambda(H^TH)^{\downarrow}$')
plt.plot(np.sort(net.sp_eva*net.hth_eva)[::-1][:20]/norm, '.-',
         label=r'$[\lambda(\Sigma^P)^{\downarrow} \circ \lambda(H^TH)^{\downarrow}]^{\downarrow} $' )
plt.xlabel(r'$n$', fontsize=20)
plt.ylabel(r'$\lambda_n$', fontsize=20)
plt.legend( fontsize=15)

plt.subplot(1,2,2)

plt.plot(net.sp_eva[:20]/net.sp_eva.sum(), '.-',
         label=r'$\lambda(\Sigma^P)^{\downarrow}$')
plt.plot(net.hth_eva[::-1][:20]/net.hth_eva.sum(), '.-', markersize=12, 
         label=r'$\lambda(H^TH)^{\uparrow}$')
plt.plot( np.sort(net.sp_eva*net.hth_eva[::-1])[::-1][:20]/norm, '.-',
         label=r'$[\lambda(\Sigma^P)^{\downarrow} \circ \lambda(H^TH)^{\uparrow}]^{\downarrow} $')
plt.xlabel(r'$n$', fontsize=20)
plt.legend( fontsize=15)

plt.savefig(path+'4_N{}.pdf'.format(N))

#%%
plt.figure(figsize=(8,5))


plt.plot(np.cumsum(np.sort(np.log(net.sp_eva)+np.log(net.hth_eva))[::-1] ),
         '-',
         label=r'$\ln\tilde\lambda(\Sigma^P)^{\uparrow} + \ln\tilde\lambda(H^TH)^{\uparrow}$',
         markersize=6)
plt.plot(np.cumsum( np.sort(np.log(net.sp_eva)+ np.log(net.hth_eva[::-1]))[::-1] ),
         '-',
         label=r'$\ln\tilde\lambda(\Sigma^P)^{\uparrow} + \ln\tilde\lambda(H^TH)^{\downarrow}$',
         markersize=6)
plt.plot(np.cumsum( np.log(net.sf_eva)), '-',
         label=r'$\ln\tilde\lambda(\Sigma^F)$',
         markersize=6)

plt.legend(fontsize=20)
plt.xlabel(r'$k$', fontsize=20)

plt.ylabel(r'$\sum_{i=1}^{k} \lambda^{\downarrow}_i$', fontsize=20)

plt.savefig(path+'5_N{}.pdf'.format())

#%%

plt.figure(figsize=(8,5))

norm=net.sp_eva.sum()*net.hth_eva.sum()

plt.plot(np.cumsum(np.sort(net.sp_eva*net.hth_eva)[::-1])/norm, '.-',
         label=r'$\lambda(\Sigma^P)^{\uparrow} \circ \lambda(H^TH)^{\uparrow}$',
         markersize=6)
plt.plot(np.cumsum(np.sort(net.sp_eva*net.hth_eva[::-1])[::-1])/norm, '.-',
         label=r'$\lambda(\Sigma^P)^{\uparrow} \circ \lambda(H^TH)^{\downarrow}$',
         markersize=6)
plt.plot(np.cumsum(net.sf_eva/norm), '.-',
         label=r'$\lambda(\Sigma^F)$',
         markersize=6)
plt.plot(np.cumsum(net.sp_eva)/net.sp_eva.sum(), '.-',
         label=r'$\lambda(\Sigma^P)$',
         markersize=6)
plt.plot(np.cumsum(net.hth_eva)/net.hth_eva.sum(), '.-',
         label=r'$\lambda(H^TH)$',
         markersize=6)

plt.legend(fontsize=20)
plt.xlabel(r'$k$', fontsize=20)
plt.ylabel(r'$\sum_{i=1}^{k} \lambda^{\downarrow}_i$', fontsize=20)

plt.savefig(path+'6_N{}.pdf'.format(N))

#%%

plt.figure(figsize=(8,5))

plt.plot(np.cumsum(np.sort(net.sp_eva*net.hth_eva)[::-1])/(net.sp_eva*net.hth_eva).sum(), ':',
         label=r'$\lambda(\Sigma^P)^{\uparrow} \circ \lambda(H^TH)^{\uparrow}$',
         linewidth=3, c='#1f77b4')
plt.plot(np.cumsum(np.sort(net.sp_eva*net.hth_eva[::-1])[::-1])/(net.sp_eva*net.hth_eva[::-1]).sum(),
         '--', c='#1f77b4',
         label=r'$\lambda(\Sigma^P)^{\uparrow} \circ \lambda(H^TH)^{\downarrow}$',
         linewidth=3)
plt.plot(np.cumsum(net.sf_eva/net.sf_eva.sum()), '-',
         label=r'$\lambda(\Sigma^F)$',
         linewidth=3, c='#1f77b4')
plt.plot(np.cumsum(net.sp_eva)/net.sp_eva.sum(), '-',
         label=r'$\lambda(\Sigma^P)$',
         linewidth=3, c='#ff7f0e')

plt.tick_params(labelsize=16)
plt.ylim([9e-1,1])
plt.legend(fontsize=20)
plt.xlabel(r'$k$', fontsize=20)
plt.ylabel(r'$\sum_{i=1}^{k} \lambda^{\downarrow}_i\cdot ( \sum_{i=1}^{N} \lambda_i )^{-1}$',
           fontsize=20)


plt.savefig(path+'7_N{}.pdf'.format(N))

#%%

direc= './EU_grid_model/Results/original_network/'
files=glob.glob(direc+'pca_coarse_grain_alpha{}_M*_{}_bal.npz'.format(0.8, 'syn'))

p_k95=np.array([])
f_k95=np.array([])
approx1=np.array([])
approx2=np.array([])

Ms=np.array([])

for fname in files:

    result=np.load(fname)
    
    p_k95=np.append(p_k95, ob.qx(result['sp_eva'], 0.95)[0] )
    f_k95=np.append(f_k95, ob.qx(result['sf_eva'], 0.95)[0] )
    approx1=np.append(approx1, 
                     ob.qx(np.sort(result['hth_eva']*result['sp_eva'])[::-1],
                           0.95)[0] )
    approx2=np.append(approx2, 
                     ob.qx(np.sort(result['hth_eva']*(result['sp_eva'][::-1]))[::-1],
                           0.95)[0] )
    Ms=np.append(Ms, int(fname.split('_')[5][1:]) )
    
p_k95=p_k95[Ms.argsort()]
f_k95=f_k95[Ms.argsort()]
approx1=approx1[Ms.argsort()]
approx2=approx2[Ms.argsort()]
Ms=Ms[Ms.argsort()]

#%%
plt.plot(Ms, f_k95,  linewidth=3, c='#1f77b4',
         label=r'$\lambda(\Sigma^F)$')
plt.plot(Ms, approx1, ':', linewidth=3, c='#1f77b4',
         label=r'$\lambda(\Sigma^P)^{\uparrow} \circ \lambda(H^TH)^{\uparrow}$')
plt.plot(Ms, approx2, '--', linewidth=3, c='#1f77b4',
        label=r'$\lambda(\Sigma^P)^{\uparrow} \circ \lambda(H^TH)^{\downarrow}$')    
plt.plot(Ms, p_k95,  linewidth=3,
         label=r'$\lambda(\Sigma^P)$',c='#ff7f0e')
plt.tick_params(labelsize=14)

#plt.yscale('log')
plt.legend(fontsize=13, loc='center right')
plt.ylim([-5,250])

plt.xlabel('Network size M', fontsize=15)
plt.ylabel('95-Percentile of PCs', fontsize=15)

plt.savefig(path + '95percentile_approx_coarse_graining2.pdf')














