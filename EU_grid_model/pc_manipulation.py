# -*- coding: utf-8 -*-

#Apply the manipulation of principal components to the mismatch time series

import lib.bal_flow_net as bfn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import lib.observables as ob
import lib.ts_manipulation as tm

path= './EU_grid_model/Results/pc_manipulation/'


#%% Define coarse graining scales
nbins= np.unique(np.round(2**np.arange(1,6.2,0.2)))[::-1]
result=pd.DataFrame(index=range(nbins.shape[0]+1),
                    columns=['nbin','M','dM', 'F_K95', 'P_K95'],
                    dtype=np.float)


#%% Load original network

orig_net=bfn.BaFloNet().load_top().load_dir()

#%% Mismatch PCA

orig_net.calc_sm().calc_meig()

plt.plot(np.cumsum(orig_net.sm_eva)/np.sum(orig_net.sm_eva), '.-')
plt.xlabel(r'$k$', fontsize=20)
plt.ylabel(r'$\sum_{i=1}^{k} \lambda_i \cdot ( \sum_{i=1}^{N} \lambda_i )^{-1}$ ',
           fontsize=20)
plt.savefig(path+ 'mismatch_principal_variances.pdf')

M_K95=ob.qx(orig_net.sm_eva, 0.95)[0]

#%% Mismatch-PC manipulation on original network

K=M_K95
mpc=tm.MaPriCo(orig_net.mis).import_pc(orig_net.sm_eva, orig_net.sm_eve)
orig_net.mis= mpc.new_ts(K)


#%% Calculate PC's in original network with manipulated mismatch-PCs

orig_net.balance().calc_sp().calc_sf().calc_hth()
orig_net.calc_peig().calc_feig()

result.iloc[0].nbin=None
result.iloc[0].M=orig_net.buses.shape[0]
result.iloc[0].dM=orig_net.lines.length.mean()
result.iloc[0].P_K95=ob.qx(orig_net.sp_eva, 0.95)[0]
result.iloc[0].F_K95=ob.qx(orig_net.sf_eva, 0.95)[0]


#%% Coarse-graining

net=orig_net
for i,nbin in enumerate(nbins, start=1):
    net=bfn.coarse_grain(net, nbin=nbin)[0]
    
    net.balance().calc_sp().calc_sf().calc_hth()
    net.calc_peig().calc_feig()

    result.iloc[i].nbin=nbin
    result.iloc[i].M=net.buses.shape[0]
    result.iloc[i].dM=net.lines.length.mean()
    result.iloc[i].P_K95=ob.qx(net.sp_eva, 0.95)[0]
    result.iloc[i].F_K95=ob.qx(net.sf_eva, 0.95)[0]
    
    print i,' out of ', nbins.shape[0], ' done! \n'

#%% Save/Load results from coarse-graining

K=0
#result.to_csv(path+'coarse_graining_pca_result_keep_{}_mismatch_PCs.csv'.format(K))
result=pd.read_csv(path+'coarse_graining_pca_result_keep_{}_mismatch_PCs.csv'.format(K))

#%% Plotting coarse-graining results

plt.plot(result.M, result.loc[:,['F_K95','P_K95' ]], '.-')
plt.legend([r'Flow $K_{95}$',r'Injection $K_{95}$'], loc='lower right', fontsize=17)
plt.xlabel('Network size M', fontsize=15)
plt.ylabel(r'q-Percentile of PCs $K_q$', fontsize=15)
plt.title('PC-Manipulation K={}'.format(K))
plt.savefig(path+'K_q_vs_M_keep_{}_mismatch_PCs.pdf'.format(K))
