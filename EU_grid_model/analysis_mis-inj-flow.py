# -*- coding: utf-8 -*-

#Data analysis of original model: Mismatch, Injection, Balancing, Flow

import lib.bal_flow_net as bfn
import numpy as np
import scipy.linalg as sl
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import lib.help_tools as ht
from scipy.optimize import curve_fit as cf
from matplotlib.colorbar import ColorbarBase

path= './EU_grid_model/Results/original_network/'


#%% Define parameter ranges

nbins= np.array([42]) #np.unique(np.round(2**np.arange(1,6.3,0.2)))[::-1]
alphas=np.linspace(0.,1., 21)


#%% Analyse balancing contribution to injection covariance: Distribution
    
c=cm.get_cmap('viridis')(np.linspace(0,1,alphas[::2].shape[0]))

for M in [1494]:
    for scheme in ['syn']: 
        
        plt.figure(figsize=(7,3.5))
        for n,alpha in enumerate(alphas[::2]):
            
            mcov=np.load(path+ 'pca_alpha{}_M{}_{}_bal.npz'.format(alpha,
                        M, scheme))['sm']
            pcov=np.load(path+ 'pca_alpha{}_M{}_{}_bal.npz'.format(alpha,
                        M, scheme))['sp']
            bcov=pcov-mcov
            
            plt.subplot(1,2,1)
            counts, bins, pat= plt.hist((bcov/mcov).flatten(), range=(-40,40),log=True,
                     bins=150, color=c[n], label=r'$\alpha={}$'.format(alpha),
                     histtype='step', density=True)
            plt.ylim([8*1e-5,1])
            plt.xlabel(r'$\frac{Cov(P_n, P_m)-Cov(\Delta_n, \Delta_m)}{Cov(\Delta_n, \Delta_m)} $',
                                fontsize=16)
            plt.ylabel('Probability density', fontsize=16)

            plt.subplot(1,2,2)
            plt.plot(bins[1:], 1.*np.cumsum(counts)/counts.sum(),
                     label=r'$\alpha={}$'.format(alpha),
                     color=c[n])
            plt.xlabel(r'$\frac{Cov(P_n, P_m)-Cov(\Delta_n, \Delta_m)}{Cov(\Delta_n, \Delta_m)} $',
                                fontsize=16)
            plt.ylabel('CDF', fontsize=16)
            

        plt.legend()
        plt.tight_layout()
        plt.savefig(path+\
                    'figures/sp_balance_constribution_M{}_{}_bal.pdf'.format(M, scheme))

#%% Analyse balancing contribution to injection covariance: Eigenvalues

M=1494

for scheme in ['syn', 'uni', 'one']:
    res=np.zeros((alphas.shape[0], M))
     
    #Load and prepare results
    for n,alpha in enumerate(alphas):
        
        #Load mismatch/injection covariance matrix
        mcov=np.load(path+ 'pca_alpha{}_M{}_{}_bal.npz'.format(alpha,
                     M, scheme))['sm']
        pcov=np.load(path+ 'pca_alpha{}_M{}_{}_bal.npz'.format(alpha,
                     M, scheme))['sp']
        
        #Calculate balancing contribution and its eigenvalues
        bcov=pcov-mcov
        eva,eve=sl.eigh(bcov)
        
        #Save eigenvalues
        res[n]=eva
        res[n]=res[n]/ np.sum(np.abs(res[n])) 
        
    res=np.abs(res[:,::-1])
    
    #plot results
    plt.fill_between(alphas, np.zeros(alphas.shape[0]),res.T[0], label='k=1')
    plt.fill_between(alphas, res.T[0],res.T[0]+res.T[-1], label='k=N')
    plt.fill_between(alphas, res.T[0]+res.T[-1],
                     np.ones(alphas.shape[0]), label=r'$1<$k$<$N')
    
    plt.grid()
    plt.legend(ncol=5, loc='lower center')
    plt.autoscale(enable=True, tight=True)
    plt.xlabel(r'$\alpha$', fontsize=19)
    plt.ylabel(r'$|\tilde\lambda^{\downarrow}_k(\Sigma^D)|$', fontsize=19)
    plt.savefig(path+'figures/sd_eva_M{}_{}_bal.pdf'.format(M, scheme))
    plt.clf()
    
#%% Analyse balancing contribution to injection covariance: Eigenvectors and majorization

M=1494

for scheme in ['one', 'syn', 'uni']:
    
    for n,alpha in enumerate([0.,0.8]):
        
        net=bfn.BaFloNet().load_top().load_dir(alpha=alpha).balance(scheme=scheme)
        net.calc_sm().calc_sp()
        net.calc_meig().calc_peig()
        
        #Load mismatch/injection covariance matrix
        mcov=np.load(path+ 'pca_alpha{}_M{}_{}_bal.npz'.format(alpha,
                     M, scheme))['sm']
        pcov=np.load(path+ 'pca_alpha{}_M{}_{}_bal.npz'.format(alpha,
                     M, scheme))['sp']
        
        #Calculate balancing contribution and its eigenvalues
        bcov=pcov-mcov
        eva,eve=sl.eigh(bcov)
        
        #Save eigenvalues
        eva=eva[::-1]
        eve=eve[:,::-1]
        
        #plot two largest eigenvectors on maps
        fig,ax=plt.subplots(1,2,figsize=(9,10))
        vmax=0.04
        
        a=net.plot(line_widths=0., ax=ax[0],line_colors='k',
                   bus_cmap='coolwarm',
                   title=r'$\tilde \lambda_1={:.3f}$'.format(eva[0]/np.sum(np.abs(eva)) ),
                   bus_colors=eve[:,0],   bus_sizes=3., basemap=False)
        a[0].set_clim(-vmax,vmax)
        a[0].set_linewidth(0)
        ht.eu_map(net,ax[0])
        
        a=net.plot(line_widths=0., ax=ax[1],line_colors='k',
                   bus_cmap='coolwarm',
                   title=r'$\tilde \lambda_N={:.3f}$'.format(eva[-1]/np.sum(np.abs(eva))),
                   bus_colors=eve[:,-1],  bus_sizes=3., basemap=False)
        a[0].set_clim(-vmax,vmax)
        a[0].set_linewidth(0)
        ht.eu_map(net,ax[1])
        
        cbar=plt.colorbar(a[0], ax=ax, orientation='horizontal', aspect=100,
                     pad=0.03)
        ticks=cbar.ax.get_xticklabels()
        ticks[0].set_text(r'$< 0.04$')
        ticks[-1].set_text(r'$> 0.04$')
        cbar.ax.set_xticklabels(ticks) 
        
        plt.savefig(path +\
                    'figures/sd_eve_alpha{}_M{}_{}_bal.pdf'.format(alpha, M,scheme),
                    bbox_inches='tight')
        
        plt.clf()
        plt.loglog(np.sort(net.sm_eva+eva[::-1])[::-1],net.sp_eva, 'o' )
        plt.plot([1e-1,np.amax(net.sp_eva)+10000],[1e-1,np.amax(net.sp_eva)+10000],
                  label='Line of identity')
        plt.xlabel(r'$\left(\lambda^{\downarrow}(\Sigma^{\Delta}) + \lambda^{\uparrow}(\Sigma^{D}) \right)^{\downarrow}$',
                   fontsize=15)
        plt.ylabel('$\lambda^{\downarrow}(\Sigma^{P})$', fontsize=15)
        plt.tick_params(labelsize=12)
        plt.legend(fontsize=12)
        plt.savefig(path +\
                    'figures/sd_majorization_alpha{}_M{}_{}_bal.pdf'.format(alpha, M,scheme),
                    bbox_inches='tight')
    
#%% Plott spatial correlations mismatch/injection
    
c=cm.get_cmap('viridis')(np.linspace(0,1,alphas[::2].shape[0]))

#Injection correlogram
for M in [1494, 469]:
    for scheme in ['syn', 'uni', 'one']:
        for i,alpha in enumerate(alphas[::2]):
            res=np.load(path + 'spatial_corr_alpha{}_M{}_{}_bal.npz'.format(alpha,
                        M, scheme))['cor_p']
            plt.plot(res[:,0],res[:,1], color=c[i], label=r'$\alpha={}$'.format(alpha))
        
        plt.xlabel('Distance d [km]', fontsize=13)
        plt.ylabel(r'$Q_d(\mathbf P)$', fontsize=13)
        plt.ylim([-0.4,1.])
        plt.xlim([0,3200])
        plt.plot(np.arange(0,3200), np.zeros(np.arange(0,3200).shape[0]),
                 '--', color='k')
        plt.legend(ncol=4)
        
        plt.savefig(path + 'figures/spatial_corr_injection_M{}_{}_bal.pdf'.format(M, scheme))
        plt.clf()

#Mismatch correlogram
for M in [1494, 469]:
    for i,alpha in enumerate(alphas[::2]):
        res=np.load(path + 'spatial_corr_alpha{}_M{}_{}_bal.npz'.format(alpha,
                    M, scheme))['cor_m']        
        plt.plot(res[:,0],res[:,1], color=c[i], label=r'$\alpha={}$'.format(alpha))
        
    plt.xlabel('Distance d [km]')
    plt.ylabel(r'$Q_d(\mathbf \Delta)$')
    plt.ylim([-0.4,1.])
    plt.xlim([0,3000])
    plt.plot(np.arange(0,3000), np.zeros(np.arange(0,3000).shape[0]),
             '--', color='k')
    plt.legend(ncol=4)

    plt.savefig(path + 'figures/spatial_corr_mismatch_M{}_{}_bal.pdf'.format(M, scheme))
    plt.clf()

#%% Plot spatial correlation of mismatch increments
    
func=lambda x,a,b: a*x**b

for M in [1494, 469]:
    for i,alpha in enumerate(alphas):            
        res=np.load(path + 'spatial_corr_alpha{}_M{}_{}_bal.npz'.format(alpha,
                    M, scheme))['cor_dm_sq']
        plt.plot(res[:,0],res[:,1], color=c[i], label=r'$\alpha={}$'.format(alpha))
           
        if alpha==1. and M==1494:
            n1=np.argmax(res[:,0]>50)
            n2=np.argmax(res[:,0]>400)
            popt, pcov= cf(func,res[n1:n2,0], res[n1:n2,1])
            plt.plot(np.arange(20,1000),
                     func(np.arange(20,1000), popt[0], popt[1]),
                     '--', color='k',
                     label=r'$\sim \rm{d}^{-0.73}$')
    
    plt.xlabel('Distance d [km]', fontsize=13)
    plt.ylabel(r'Corr($[d\Delta]^2$)', fontsize=13)
    plt.yscale('log')
    plt.xscale('log')
    plt.ylim([5e-3,1.5])
    plt.xlim([5,3200])
    if M==469: plt.xlim([25,3200]) 
    plt.legend(ncol=3)
    
    plt.savefig(path + 'figures/spatial_corr_mismatch_inc_sq_M{}_{}_bal.pdf'.format(M, scheme))
    plt.clf()    

#%% Plot PCA results depending an alpha

for M in [1494]:
    for scheme in ['syn']:
        res=np.zeros((alphas.shape[0], M))
        
        for typ in ['sp_eva']:
            
            #Load and prepare results
            for n,alpha in enumerate(alphas):
                res[n]= np.load(path+ 'pca_alpha{}_M{}_{}_bal.npz'.format(alpha,
                                 M, scheme))[typ]
                res[n]=res[n]/ np.sum(res[n]) 
                
            res=np.cumsum(res, axis=1)
            
            #plot results
            plt.fill_between(alphas, np.zeros(alphas.shape[0]),res.T[0], label='k=1')
            for n,curve in enumerate(res.T[1:9]):
                plt.fill_between(alphas, curve, res.T[n], label='k={}'.format(n+2))
            plt.fill_between(alphas, curve, np.ones(alphas.shape[0]), label=r'k$>$8')
            
            plt.grid()
            plt.legend(ncol=5, loc='lower center')
            plt.autoscale(enable=True, tight=True)
            plt.xlabel(r'$\alpha$', fontsize=19)
            if typ=='sm_eva':
                plt.ylabel(r'$\tilde \lambda_k^{\downarrow}(\mathbf \Sigma^\Delta)$', fontsize=19)
            else:
                plt.ylabel(r'$\tilde \lambda_k^{\downarrow}(\mathbf \Sigma^P)$', fontsize=19)
            plt.savefig(path+'figures/{}_M{}_{}_bal.pdf'.format(typ,M, scheme))
            plt.clf()
            

#%% Plot PCA results as maps
            
for M in [1494]:
    net=bfn.BaFloNet().load_top().load_dir()
    if M==30:
         net=bfn.coarse_grain(net, nbin=6)[0]
 
    for scheme in ['syn','uni', 'one']:
        for alpha in [0.,0.8]:
            for typ in ['sm_','sp_']:
                
                res=np.load(path+\
                            'pca_alpha{}_M{}_{}_bal.npz'.format(alpha,M, scheme))[typ+'eve']
                eva=np.load(path+\
                            'pca_alpha{}_M{}_{}_bal.npz'.format(alpha,M, scheme))[typ+'eva']
                eva=eva/eva.sum()
                
                fig,ax=plt.subplots(1,3,figsize=(9,10))
                vmax=0.04
                
                net.rplot(res[:,0], ax=ax[0], 
                          title=r'$\tilde\lambda_1={:.3f}$'.format(eva[0]),
                          vmin=-vmax, vmax=vmax)
                
                net.rplot(res[:,1], ax=ax[1], 
                          title=r'$\tilde\lambda_2={:.3f}$'.format(eva[1]),
                          vmin=-vmax, vmax=vmax)

                net.rplot(res[:,2], ax=ax[2], 
                          title=r'$\tilde\lambda_3={:.3f}$'.format(eva[2]),
                          vmin=-vmax, vmax=vmax)
                
                cbar_ax = fig.add_axes([0.035, 0.4, .94, .01])
                cbar=ColorbarBase(cmap='RdBu_r',
                                  norm=plt.Normalize(vmin=-vmax, vmax=vmax),
                                  ax=cbar_ax, orientation='horizontal')
                ticks=cbar_ax.get_xticklabels()
                ticks[0].set_text(r'$< 0.04$')
                ticks[-1].set_text(r'$> 0.04$')
                cbar_ax.set_xticklabels(ticks) 
                
                plt.tight_layout()
                plt.savefig(path+\
                            'figures/{}eve_alpha{}_M{}_{}_bal.pdf'.format(typ,
                                     alpha,M,scheme), bbox_inches='tight')
                
#%% Flow PCA
       
alpha=0.8


net=bfn.BaFloNet().load_top().load_dir(alpha=alpha)
net.balance()
net.calc_sp().calc_hth().calc_feig().calc_peig().calc_htheig()   

feva=net.sf_eva/net.sf_eva.sum()
feve=net.sf_eve
meve=net.m_eve

#%%

fig,ax=plt.subplots(1,3,figsize=(9,10))
vmax=0.04

net.rplot(meve[:,0], ax=ax[0], vmin=-vmax, vmax=vmax, alpha=0.4)
net.qplot(feve[:,0], ax=ax[0], title=r'$\tilde\lambda_1={:.3f}$'.format(feva[0]),
          quant=0.95)

net.rplot(meve[:,1], ax=ax[1], vmin=-vmax, vmax=vmax, alpha=0.4)
net.qplot(feve[:,1], ax=ax[1], quant=0.95, 
          title=r'$\tilde\lambda_2={:.3f}$'.format(feva[1]))

net.rplot(meve[:,2], ax=ax[2],vmin=-vmax, vmax=vmax, alpha=0.4)
net.qplot(feve[:,2], ax=ax[2], quant=0.95, 
          title=r'$\tilde\lambda_3={:.3f}$'.format(feva[2]))

cbar_ax = fig.add_axes([0.035, 0.4, .94, .01])
cbar=ColorbarBase(cmap='RdBu_r',
                  norm=plt.Normalize(vmin=-vmax, vmax=vmax),
                  ax=cbar_ax, orientation='horizontal')
ticks=cbar_ax.get_xticklabels()
ticks[0].set_text(r'$< 0.04$')
ticks[-1].set_text(r'$> 0.04$')
cbar_ax.set_xticklabels(ticks) 

plt.tight_layout()
plt.savefig(path+\
            'figures/sf_eve_alpha{}_M{}_syn_bal.pdf'.format(alpha,1494),
            bbox_inches='tight')
                
#%% Overlaps of topological and injection PCs
    
ht.plot_eve_overlap(net.sp_eva,net.sp_eve, net.hth_eva,net.hth_eve)
plt.savefig(path+ 'figures/overlap_coeff_flow_pca_alpha{}.pdf'.format(alpha),
            bbox_inches='tight')


#%% Plot mean/variances of capacity factors, layouts, generation and load
                          
dat_path='/home/adminuser/Dokumente/Master_Arbeit/External_data/RE-Europe_dataset_package'

#Load the data
solar_cf=pd.read_csv(dat_path+'/Nodal_TS/solar_signal_COSMO.csv',
                     index_col=0).iloc[:-1]
wind_cf=pd.read_csv(dat_path+'/Nodal_TS/wind_signal_COSMO.csv',
                    index_col=0).iloc[:-1]
solar_lay=pd.read_csv(dat_path+'/Static_data/solar_layouts_COSMO.csv', 
                      index_col=0).Proportional
wind_lay=pd.read_csv(dat_path+'/Static_data/wind_layouts_COSMO.csv',
                     index_col=0).Proportional
                     
#Load network
net=bfn.BaFloNet().load_top().load_dir()                     

#Calculate generation time series
g_wind=wind_cf.mul(wind_lay.values)
g_solar=solar_cf.mul(solar_lay.values)

#Calculate rescaled generation time series
gsolar,gwind= ht.scale_layout(g_wind, g_solar, net.load, net.buses,
                               0.8,  return_mis=False)

#Plot maps
plt.figure(figsize=(10,8))
a,b=net.plot(line_widths=0, bus_colors=wind_cf.mean().values)
cbar=plt.colorbar(a, orientation='horizontal', pad=0.05)
cbar.ax.set_xlabel('Mean capacity factor')
plt.savefig(path+'figures/wind_cf.pdf')

plt.figure(figsize=(10,8))
a,b=net.plot(line_widths=0, bus_colors=solar_cf.mean().values)
cbar=plt.colorbar(a, orientation='horizontal', pad=0.05)
cbar.ax.set_xlabel('Mean capacity factor')
plt.savefig(path+'figures/solar_cf.pdf')

plt.figure(figsize=(10,8))
a,b=net.plot(line_widths=0, bus_colors=gwind.mean().values)
cbar=plt.colorbar(a, orientation='horizontal', pad=0.05)
cbar.ax.set_xlabel('Mean power generation [MWh]')
plt.savefig(path+'figures/g_wind_alpha0.8.pdf')

plt.figure(figsize=(10,8))
a,b=net.plot(line_widths=0, bus_colors=gsolar.mean().values)
cbar=plt.colorbar(a, orientation='horizontal', pad=0.05)
cbar.ax.set_xlabel('Mean power generation [MWh]')
plt.savefig(path+'figures/g_solar_alpha0.8.pdf')       

plt.figure(figsize=(10,8))
a,b=net.plot(line_widths=0, bus_colors=net.load.mean().values)
cbar=plt.colorbar(a, orientation='horizontal', pad=0.05)
cbar.ax.set_xlabel('Mean load [MWh]')
plt.savefig(path+'figures/load.pdf')        

plt.figure(figsize=(10,8))
a,b=net.plot(line_widths=0, bus_colors=net.load.std().values)
cbar=plt.colorbar(a, orientation='horizontal', pad=0.05)
cbar.ax.set_xlabel('Mean load [MWh]')
plt.savefig(path+'figures/load_std_dev.pdf')                
                
plt.figure(figsize=(10,8))
a,b=net.plot(line_widths=0, bus_colors=net.mis.std().values)
cbar=plt.colorbar(a, orientation='horizontal', pad=0.05)
cbar.ax.set_xlabel('Mean load [MWh]')
plt.savefig(path+'figures/mismatch_std_devalpha0.8.pdf')    
            

#%% Plot  Flow/Injection observables vs M 

c=cm.get_cmap('viridis')(np.linspace(0,1,alphas[::2].shape[0]))

f1=plt.figure(1)
f2=plt.figure(2)
f3=plt.figure(3)
f4=plt.figure(4)
f5=plt.figure(5)

for i,alpha in enumerate(alphas[::2]):
    result=pd.read_csv(path+'original_re-eu_alpha{}.csv'.format(alpha))
    
    plt.figure(1)
    l1,=plt.plot(result.M, result.loc[:,'F_K95'], '-', color=c[i],
             label=r'$\alpha={}$'.format(alpha))
    l2,=plt.plot(result.M, result.loc[:,'P_K95'].values, '--', color=c[i])  
    plt.figure(2)
    plt.plot(result.M, result.loc[:,'K'], '.-', color=c[i],
             label=r'$\alpha={}$'.format(alpha))
    plt.figure(3)
    plt.plot(result.M, result.loc[:,'Tk'], '.-', color=c[i],
             label=r'$\alpha={}$'.format(alpha))
    plt.figure(4)
    plt.plot(result.M, result.loc[:,'F_K95'], '.-', color=c[i],
                 label=r'$\alpha={}$'.format(alpha))
    plt.figure(5)
    plt.plot(result.M, result.loc[:,'P_K95'].values, '.-', color=c[i],
             label=r'$\alpha={}$'.format(alpha))


plt.figure(1)
plt.subplots_adjust(bottom=0.45)
leg1=plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.legend([l2,l1], [r'Injection',r'Flow'])
plt.gca().add_artist(leg1)
plt.xlabel('Network size M', fontsize=12)
#plt.yscale('log')
plt.ylabel(r'95-Percentile of PCs', fontsize=12)
plt.savefig(path+'figures/K_q_vs_M.pdf')

plt.figure(2)
plt.subplots_adjust(bottom=0.45)
plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.xlabel('Network size M', fontsize=12)
plt.ylabel(r'Transmission capacity [MWh]', fontsize=12)
plt.savefig(path+'figures/trans_cap_vs_M.pdf',bbox_inches="tight")

plt.figure(3)
plt.subplots_adjust(bottom=0.45)
plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.xlabel('Network size M', fontsize=12)
plt.ylabel(r'Transmission cost [MWh $\cdot$ km]', fontsize=12)
plt.savefig(path+'figures/trans_cost_vs_M.pdf',bbox_inches="tight")

plt.figure(4)
plt.subplots_adjust(bottom=0.45)
plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.xlabel('Network size M', fontsize=12)
plt.ylabel(r'95-Percentile of Flow-PCs', fontsize=12)
plt.savefig(path+'figures/K_q_vs_M_flow.pdf')

plt.figure(5)
plt.subplots_adjust(bottom=0.45)
leg1=plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.xlabel('Network size M', fontsize=12)
plt.ylabel(r'95-Percentile of Injection-PCs', fontsize=12)
plt.savefig(path+'figures/K_q_vs_M_injection.pdf')


#%%  Plot Flow/Injection observables vs alpha

c=cm.get_cmap('viridis')(np.linspace(0,1,nbins[::5][:-1].shape[0]+1))

f1=plt.figure(1)
f2=plt.figure(2)
f3=plt.figure(3)
f4=plt.figure(4)


for i, nbin in enumerate(np.append(0,nbins[::5][:-1])):
    result=pd.DataFrame()
    
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+'original_re-eu_alpha{}.csv'.format(alpha))
        frame.loc[:,'alpha']=alpha
        if nbin==0:
            result=pd.concat([result,frame.loc[frame.loc[:,'nbin'].isnull()]])
        else:
            result=pd.concat([result,frame.loc[frame.loc[:,'nbin']==nbin]])
            
    result.sort_values(by='alpha', inplace=True)
    
    plt.figure(1)
    plt.plot(result.alpha, result.loc[:,'F_K95'], '.-', color=c[i],
             label=r'M={:.0f}'.format(result.M.iloc[0]))
    
    plt.figure(2)
    plt.plot(result.alpha, result.loc[:,'P_K95'], '.-', color=c[i],
             label=r'M={:.0f}'.format(result.M.iloc[0]))
    
    plt.figure(3)
    plt.plot(result.alpha, result.loc[:,'K'], '.-', color=c[i],
             label=r'M={:.0f}'.format(result.M.iloc[0]))
    
    plt.figure(4)
    plt.plot(result.alpha, result.loc[:,'Tk'], '.-', color=c[i],
             label=r'M={:.0f}'.format(result.M.iloc[0]))
        
plt.figure(1)
plt.subplots_adjust(bottom=0.4)
plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.xlabel(r'$\alpha$', fontsize=15)
plt.ylabel(r'95-Percentile of Flow-PCs', fontsize=15)
plt.savefig(path+'figures/Kq_flow_vs_alpha.pdf')

plt.figure(2)
plt.subplots_adjust(bottom=0.4)
plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.xlabel(r'$\alpha$', fontsize=15)
plt.ylabel(r'95-Percentile of Injection-PCs', fontsize=15)
plt.savefig(path+'figures/Kq_injection_vs_alpha.pdf')

plt.figure(3)
plt.subplots_adjust(bottom=0.4)
plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.xlabel(r'$\alpha$', fontsize=15)
plt.ylabel(r'Transmission capacity [MWh]', fontsize=15)
plt.savefig(path+'figures/trans_cap_vs_alpha.pdf', bbox_inches="tight")

plt.figure(4)
plt.subplots_adjust(bottom=0.4)
plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.xlabel(r'$\alpha$', fontsize=15)
plt.ylabel(r'Transmission cost [MwH $\cdot$ km]', fontsize=15)
plt.savefig(path+'figures/trans_cost_vs_alpha.pdf', bbox_inches="tight")



