# -*- coding: utf-8 -*-

#Apply the normal-copula model to wind power generation. 
#Parameter-sweep and analysis of results.

import lib.bal_flow_net as bfn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import lib.observables as ob
import lib.ts_manipulation as tm
import matplotlib.cm as cm
import lib.help_tools as ht
from scipy.stats import norm


path= './EU_grid_model/Results/copula_method/copula_load-gen/'


#%% Define parameter ranges and observables

deltas=np.array([0.5,1.])#np.around(np.linspace(0.,1.,11), decimals=5)
alphas=np.linspace(0.,1.,11)
betas=np.array([0.2,0.4,0.6,0.8,1.2,1.4,1.6,1.8])

result=pd.DataFrame(index=range(deltas.shape[0]+1),
                    columns=['delta','M', 'F_K95', 'P_K95', 'M_K95', 'K_B',
                             'E_B','K_T', 'T_c', 'M_var'],
                    dtype=np.float)

cor_w=pd.DataFrame(columns=['dist', 'original']+ deltas.astype('string').tolist(),
                   index=np.arange(351))
cor_m=pd.DataFrame(columns=['dist', 'original']+ deltas.astype('string').tolist(),
                   index=np.arange(351))
cor_p=pd.DataFrame(columns=['dist', 'original']+ deltas.astype('string').tolist(),
                   index=np.arange(351))
cor_len=pd.DataFrame(columns=['original']+ deltas.astype('string').tolist(),
                     index=['wind', 'mis', 'inj'])

m_sum=pd.DataFrame(columns=['original']+ deltas.astype('string').tolist(),
                   index=np.arange(26303))

#%% Import data, scale layout with alpha=1

data_dir='/home/adminuser/Dokumente/Master_Arbeit/' + \
            'Code/EU_grid_model/Results/data/re-eu_original' 
            
gwind=pd.read_pickle(data_dir + '/g_wind.gzip')
gsolar=pd.read_pickle(data_dir + '/g_solar.gzip')
load=pd.read_pickle(data_dir + '/load.gzip')

#Load topology 
net=bfn.BaFloNet().load_top()

#%% Simulation using the copula-method on wind generation, with snych. balancing

count=0
N_param=alphas.shape[0]*deltas.shape[0]*betas.shape[0]

for j,alpha in enumerate(alphas):
    for k,beta in enumerate(betas):
        
        #Calculate generation time series
        g_solar, g_wind= ht.scale_layout(gwind,gsolar, load, net.buses, alpha=alpha,
                                return_mis=False)
        
        #Change large-scale fluctuations of wind generation
        g_wind=tm.scale_var(g_wind, beta)
        
        #Simulation without copula
        net.load_dat(g_wind, g_solar, load)
        net.balance().calc_sm().calc_sp().calc_hth()
        net.calc_meig().calc_peig().calc_feig().lpf()
        
        #Save observalbles
        result.iloc[0].delta=np.nan
        result.iloc[0].M=net.buses.shape[0]
        result.iloc[0].M_K95=ob.qx(net.sm_eva, 0.95)[0]
        result.iloc[0].P_K95=ob.qx(net.sp_eva, 0.95)[0]
        result.iloc[0].F_K95=ob.qx(net.sf_eva, 0.95)[0]
        result.iloc[0].M_var=net.mis.var().sum()/net.load.mean().sum()**2
        result.iloc[0].K_T, result.iloc[0].T_c = ob.trans_meas(net)
        result.iloc[0].E_B, result.iloc[0].K_B = ob.backup_meas(net)
        
#        #Save mismatch PCA and contributions to injection/balancing
#        bal= net.mis + net.loads_t.p_set
#        np.savez(path+ 'pca_mis_alpha{}_beta{}_M1494'.format(alpha,beta),
#                 sm_eva=net.sm_eva,
#                 bal_cont= bal.dot(net.sm_eve).var().values,
#                 inj_cont= (-net.loads_t.p_set).dot(net.sm_eve).var().values )
                        
        #Estimate copula covariance and save it
        ncopula=tm.NormalCopula().estimate_cov(g_wind)
        orig_cov=ncopula.cov.copy()
        
        #Simulation with copula
        for i,delta in enumerate(deltas, start=1):
            
            count+=1
            print '\n', 'Step: {}/{}'.format(count, N_param)
            
            #Reset to original covariance
            ncopula.cov=orig_cov
            
            #Scale correlations and import copula time series into network
            ncopula.scale_cov(delta)
            g_wind_c=ncopula.sample_joint_dist(g_wind)
            net.load_dat(load=load, g_solar=g_solar, g_wind=g_wind_c)
            
            #Calculate variables
            net.balance().calc_sm().calc_sp().calc_hth()
            net.calc_meig().calc_peig().calc_feig().lpf()
        
            #Save observables
            result.iloc[i].delta=delta
            result.iloc[i].M=net.buses.shape[0]
            result.iloc[i].M_K95=ob.qx(net.sm_eva, 0.95)[0]
            result.iloc[i].P_K95=ob.qx(net.sp_eva, 0.95)[0]
            result.iloc[i].F_K95=ob.qx(net.sf_eva, 0.95)[0]
            result.iloc[i].M_var=net.mis.var().sum()/net.load.mean().sum()**2
            result.iloc[i].K_T, result.iloc[i].T_c = ob.trans_meas(net)
            result.iloc[i].E_B, result.iloc[i].K_B = ob.backup_meas(net)
        
#            #Save mismatch PCA and contributions to injection/balancing
#            if k in [0,1,4] and i in [1,6,11]:
#                bal= net.mis + net.loads_t.p_set
#                np.savez(path+ 'pca_mis_alpha{}_beta{}_delta{}_M1494'.format(alpha,
#                         beta,delta),
#                         sm_eva=net.sm_eva,
#                         bal_cont= bal.dot(net.sm_eve).var().values,
#                         inj_cont= (-net.loads_t.p_set).dot(net.sm_eve).var().values )
               
        #Save alpha-beta result
        result.to_csv(path+'wind_copula_M1494_alpha{}_beta{}'.format(alpha, beta))
        

#%% Simulation using the copula-method on wind generation, with snych. balancing
# Only with subset of parameters (to analyze special quantities)

count=0
N_param=2*deltas.shape[0]

for j,alpha in enumerate([0.8, 1.]):
    for k,beta in enumerate([1.]):
        
        #Calculate generation time series
        g_solar, g_wind= ht.scale_layout(gwind,gsolar, load, net.buses, alpha=alpha,
                                return_mis=False)
        
        #Change large-scale fluctuations of wind generation
        g_wind=tm.scale_var(g_wind, beta)
        
        #Simulation without copula
        net.load_dat(g_wind, g_solar, load)
        net.balance().lpf()
            
        #Save SCF and mismatch sum
        cor_w.iloc[:,0:2], cor_len.iloc[0,0]=ht.spatial_corr(g_wind,
                                             net.buses.loc[:,'x':'y'].values)
        cor_m.iloc[:,0:2], cor_len.iloc[1,0]=ht.spatial_corr(net.mis,
                                             net.buses.loc[:,'x':'y'].values)        
        cor_p.iloc[:,0:2], cor_len.iloc[2,0]=ht.spatial_corr(-net.loads_t.p_set,
                                             net.buses.loc[:,'x':'y'].values)
        
        #Save mismatch sum
        m_sum.iloc[:,0]=net.mis.sum(axis=1).values
                        
        #Estimate copula covariance and save it
        ncopula=tm.NormalCopula().estimate_cov(g_wind)
        orig_cov=ncopula.cov.copy()
        
        #Simulation with copula
        for i,delta in enumerate(deltas, start=1):
            
            count+=1
            print '\n', 'Step: {}/{}'.format(count, N_param)
            
            #Reset to original covariance
            ncopula.cov=orig_cov
            
            #Scale correlations and import copula time series into network
            ncopula.scale_cov(delta)
            g_wind_c=ncopula.sample_joint_dist(g_wind)
            net.load_dat(load=load, g_solar=g_solar, g_wind=g_wind_c)
            
            #Calculate Flows
            net.balance().lpf()
        
            #Save SCF 
            c,l=ht.spatial_corr(g_wind_c, net.buses.loc[:,'x':'y'].values)
            cor_w.iloc[:,i+1], cor_len.iloc[0,i]=c[:,1],l
            c,l=ht.spatial_corr(net.mis, net.buses.loc[:,'x':'y'].values)
            cor_m.iloc[:,i+1], cor_len.iloc[1,i]=c[:,1],l
            c,l=ht.spatial_corr(-net.loads_t.p_set, net.buses.loc[:,'x':'y'].values)
            cor_p.iloc[:,i+1], cor_len.iloc[2,i]=c[:,1],l
            
            #Save mismatch sum
            m_sum.iloc[:,i]=net.mis.sum(axis=1).values
        
    #Save results to files
    cor_w.to_pickle(path+ 'scf_wind_copula_M1494_alpha{}'.format(alpha))
    cor_m.to_pickle(path+ 'scf_mis_copula_M1494_alpha{}_beta{}'.format(alpha, beta))
    cor_p.to_pickle(path+ 'scf_inj_copula_M1494_alpha{}_beta{}'.format(alpha, beta))
    cor_len.to_csv(path + 'cor_len_copula_M1494_alpha{}_beta{}'.format(alpha, beta))
    m_sum.to_pickle(path + 'mis_sum_M1494_alpha{}_beta{}'.format(alpha,beta))
      
            
#%% Calculate Kolmogorov-Smirnov distance


alphas=np.linspace(0.,1.,11)
ks_w=pd.DataFrame(index=np.arange(1494), columns=alphas)
ks_m=pd.DataFrame(index=np.arange(1494), columns=alphas)
ks_p=pd.DataFrame(index=np.arange(1494), columns=alphas)
ks_f=pd.DataFrame(index=np.arange(2156), columns=alphas)

for j,alpha in enumerate(alphas):
      
    print alpha
    
    #Calculate generation time series
    g_solar, g_wind= ht.scale_layout(gwind,gsolar, load, net.buses, alpha=alpha,
                            return_mis=False)
     
    #Simulation without copula
    net.load_dat(g_wind, g_solar, load)
    net.balance().lpf()

    #Simulation of copula model delta=1
    ncopula=tm.NormalCopula().estimate_cov(g_wind)
    g_wind_c=ncopula.sample_joint_dist(g_wind)
    resca_net=bfn.BaFloNet().load_top()
    resca_net.load_dat(load=load, g_solar=g_solar, g_wind=g_wind_c)
    resca_net.balance().lpf()
    
    #Calculate KS-distance
    ks_w.iloc[:,j]=ht.calc_ksd(g_wind,g_wind_c)
    ks_m.iloc[:,j]=ht.calc_ksd(net.mis, resca_net.mis)
    ks_p.iloc[:,j]=ht.calc_ksd(-net.loads_t.p_set, -resca_net.loads_t.p_set)
    ks_f.iloc[:,j]=ht.calc_ksd(net.lines_t.p0, resca_net.lines_t.p0)
    
ks_w.to_csv(path+'ksd_g_wind')
ks_m.to_csv(path+'ksd_mismatch')
ks_p.to_csv(path+'ksd_injection')
ks_f.to_csv(path+'ksd_flow')
            
#%% Calculate percentile for original data and copula model delta=1

p_vals=np.arange(1,100,2)

for j,alpha in enumerate([0.1]):
       
    #Calculate generation time series
    g_solar, g_wind= ht.scale_layout(gwind,gsolar, load, net.buses, alpha=alpha,
                            return_mis=False)
     
    #Simulation without copula
    net.load_dat(g_wind, g_solar, load)
    net.balance().lpf()

    #Save percentiles
    np.savez(path + 'percentiles_M1494_alpha{}_original'.format(alpha),
             gwind=np.percentile((g_wind.values),p_vals,axis=0),
             mis=np.percentile((net.mis.values),p_vals,axis=0),
             inj=np.percentile((-net.loads_t.p_set.values),p_vals,axis=0),
             flow=np.percentile((net.lines_t.p0.values),p_vals,axis=0))

    #Simulation of copula model delta=1
    ncopula=tm.NormalCopula().estimate_cov(g_wind)
    g_wind=ncopula.sample_joint_dist(g_wind)
    net.load_dat(load=load, g_solar=g_solar, g_wind=g_wind)
    
    #Calculate Flows
    net.balance().lpf()
    
    #Save percentiles
    np.savez(path + 'percentiles_M1494_alpha{}_beta1.0_delta1.0'.format(alpha),
         gwind=np.percentile((g_wind.values),p_vals,axis=0),
         mis=np.percentile((net.mis.values),p_vals,axis=0),
         inj=np.percentile((-net.loads_t.p_set.values),p_vals,axis=0),
         flow=np.percentile((net.lines_t.p0.values),p_vals,axis=0))
    
    
#%% Calculate difference of correlations
    
    
alphas=np.linspace(0.,1.,11)

#wind generation
cor_d=pd.DataFrame(index=np.arange(1115271), columns=alphas)
for j,alpha in enumerate(alphas):
    print alpha
    
    #Calculate generation time series
    g_solar, g_wind= ht.scale_layout(gwind,gsolar, load, net.buses, alpha=alpha,
                            return_mis=False)
     
    #Simulation without copula 
    net.load_dat(g_wind, g_solar, load)
    cor=np.corrcoef(g_wind.T)[np.triu_indices(1494,k=1)]
    
    #Simulate with copula
    ncopula=tm.NormalCopula().estimate_cov(g_wind)
    g_wind=ncopula.sample_joint_dist(g_wind)
    
    #Calculate difference of correlations
    cor_d.iloc[:,j]= cor-np.corrcoef(g_wind.T)[np.triu_indices(1494,k=1)]
cor_d.to_pickle(path+'corr_diff_gwind_M1494')
    
#mismtach
cor_d=pd.DataFrame(index=np.arange(1115271), columns=alphas)
for j,alpha in enumerate(alphas):
    print alpha
    
    #Calculate generation time series
    g_solar, g_wind= ht.scale_layout(gwind,gsolar, load, net.buses, alpha=alpha,
                            return_mis=False)
     
    #Simulation without copula 
    net.load_dat(g_wind, g_solar, load)
    cor=np.corrcoef(net.mis.T)[np.triu_indices(1494,k=1)]
    
    #Simulate with copula
    ncopula=tm.NormalCopula().estimate_cov(g_wind)
    g_wind=ncopula.sample_joint_dist(g_wind)
    net.load_dat(load=load, g_solar=g_solar, g_wind=g_wind)
    
    #Calculate difference of correlations
    cor_d.iloc[:,j]= cor-np.corrcoef(net.mis.T)[np.triu_indices(1494,k=1)]
cor_d.to_pickle(path+'corr_diff_mis_M1494')
    
#injection
cor_d=pd.DataFrame(index=np.arange(1115271), columns=alphas)
for j,alpha in enumerate(alphas):
    print alpha
    
    #Calculate generation time series
    g_solar, g_wind= ht.scale_layout(gwind,gsolar, load, net.buses, alpha=alpha,
                            return_mis=False)
     
    #Simulation without copula 
    net.load_dat(g_wind, g_solar, load).balance()
    cor=np.corrcoef(-net.loads_t.p_set.T)[np.triu_indices(1494,k=1)]
    
    #Simulate with copula
    ncopula=tm.NormalCopula().estimate_cov(g_wind)
    g_wind=ncopula.sample_joint_dist(g_wind)
    net.load_dat(load=load, g_solar=g_solar, g_wind=g_wind).balance()
    
    #Calculate difference of correlations
    cor_d.iloc[:,j]= cor-np.corrcoef(-net.loads_t.p_set.T)[np.triu_indices(1494,k=1)]
cor_d.to_pickle(path+'corr_diff_inj_M1494')
    
#Flow
cor_d=pd.DataFrame(index=np.arange(2323090), columns=alphas)
for j,alpha in enumerate(alphas):
    print alpha
    
    #Calculate generation time series
    g_solar, g_wind= ht.scale_layout(gwind,gsolar, load, net.buses, alpha=alpha,
                            return_mis=False)
     
    #Simulation without copula 
    net.load_dat(g_wind, g_solar, load).balance().lpf()
    cor= np.corrcoef(net.lines_t.p0.T)[np.triu_indices(2156,k=1)]
    
    #Simulate with copula
    ncopula=tm.NormalCopula().estimate_cov(g_wind)
    g_wind=ncopula.sample_joint_dist(g_wind)
    net.load_dat(load=load, g_solar=g_solar, g_wind=g_wind).balance().lpf()
    
    #Calculate difference of correlations
    cor_d.iloc[:,j]= cor- np.corrcoef(net.lines_t.p0.T)[np.triu_indices(2156,k=1)] 
cor_d.to_pickle(path+'corr_diff_flow_M1494')            
    

#%% Plot observables against delta, beta fixed

beta=0.5

c=cm.get_cmap('viridis')(np.linspace(0,1,alphas.shape[0]))
plt.figure(figsize=(10,8))

plt.subplot(4,2,1)
for i,alpha in enumerate(alphas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha, beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].K_T*1e-3, '.-', c=c[i])
plt.ylabel(r'$\overline{\mathcal K}^T$ [GW]', fontsize=15)
plt.grid()

plt.subplot(4,2,2)
for i,alpha in enumerate(alphas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha, beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].K_B*1e-3, '.-', c=c[i])
plt.ylabel(r'$\overline{\mathcal K}^B$ [GW]', fontsize=15)
plt.grid()

plt.subplot(4,2,3)
for i,alpha in enumerate(alphas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha, beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].T_c*1e-6, '.-', c=c[i])
plt.ylabel(r'$\overline{\mathcal T}$ [GW$\cdot$1000km]', fontsize=15)
plt.grid()

plt.subplot(4,2,4)
for i,alpha in enumerate(alphas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha, beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].E_B*1e-3, '.-',  c=c[i])
plt.ylabel(r'$\overline{E}^B$ [GWh]', fontsize=15)
plt.ylim([0,162])
plt.grid()

plt.subplot(4,2,5)
for i,alpha in enumerate(alphas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha, beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].M_var, '.-',  c=c[i])
plt.ylabel(r'Var$\left(\overline{\mathbf \Delta}\right) / \langle L_{EU} \rangle^2$', fontsize=15)
plt.grid()

plt.subplot(4,2,6)
for i,alpha in enumerate(alphas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha, beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].P_K95,'.-',c=c[i])
plt.ylabel(r'$\overline{K}_{95}^P$', fontsize=15)
plt.xlabel(r'$\delta$', fontsize=19)
plt.grid()

plt.subplot(4,2,7)
for i,alpha in enumerate(alphas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha, beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].F_K95,
             '.-', label=r'$\alpha=${}'.format(alpha), c=c[i])
plt.ylabel(r'$\overline{K}_{95}^F$', fontsize=15)
plt.xlabel(r'$\delta$', fontsize=19)
plt.legend(ncol=3)
plt.grid()

plt.tight_layout()
plt.savefig(path + 'figures/obs_vs_delta_M1494_beta{}.pdf'.format(beta))


#%% Normalized observable against delta and beta for fixed alpha 

alpha=1.
deltas=np.around(np.linspace(0.,1.,11), decimals=5)
betas=np.around(np.arange(0.,2.1,0.2), decimals=5)

fig,ax=plt.subplots(1,2, figsize=(6.5,3.3))

result=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha, 1.))
ax[0].plot(result.loc[1:].delta, result.loc[1:].K_B/result.K_B.iloc[-1], '.-',
         label=r'$\overline{\mathcal K}^B$')
#ax[0].plot(result.loc[1:].delta, result.loc[1:].K_T/result.K_T.iloc[-1], '.-',
#         label=r'$\overline{\mathcal K}^T$')
ax[0].plot(result.loc[1:].delta, result.loc[1:].T_c/result.T_c.iloc[-1], '.-',
         label=r'$\overline{\mathcal T}$')
ax[0].plot(result.loc[1:].delta, result.loc[1:].E_B/result.E_B.iloc[-1], '.-',
         label=r'$\overline{E}^B$')
ax[0].set_title(r'$\beta=1.0, \alpha={}$'.format(alpha))
#ax[0].legend()
ax[0].set_xlabel(r'$\delta$', fontsize=18)
ax[0].set_ylabel(r'Observable $\overline X \bigg/ \overline X_{\delta=1}$', fontsize=15)
ax[0].grid()

result=pd.DataFrame()
for i,beta in enumerate(betas):
    frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                      index_col=1).iloc[-1,2:].to_frame().T
    frame.loc[1.0,'beta']=beta
    result=result.append(frame, ignore_index=True)

ax[1].plot(result.beta, result.K_B/result.K_B.iloc[5], '.-',
         label=r'$\overline{\mathcal K}^B$')
#ax[1].plot(result.beta, result.K_T/result.K_T.iloc[5], '.-',
#         label=r'$\overline{\mathcal K}^T$')
ax[1].plot(result.beta, result.T_c/result.T_c.iloc[5], '.-',
         label=r'$\overline{\mathcal T}$')
ax[1].plot(result.beta, result.E_B/result.E_B.iloc[5], '.-',
         label=r'$\overline{E}^B$')
ax[1].plot(result.beta, np.sqrt(result.M_var/result.M_var.iloc[5]), '.-',
         label=r'$\sqrt{\rm{Var}(\overline{\mathbf \Delta})}$')
ax[1].set_title(r'$\delta=1.0, \alpha={}$'.format(alpha))
ax[1].legend(fontsize=11,handlelength=1)
ax[1].set_xlabel(r'$\beta$', fontsize=18)
ax[1].set_ylabel(r'Observable $\overline X \bigg/ \overline X_{\beta=1}$', fontsize=15)
ax[1].grid()

plt.tight_layout()
plt.savefig(path+'figures/normalized_obs_vs_delta_beta_alpha{}.pdf'.format(alpha))


#%% observable vs alpha for different betas, delta fixed

delta=0.5
betas=np.arange(0,2.1,0.2)

c=cm.get_cmap('viridis')(np.linspace(0,1,betas.shape[0]))
plt.figure(figsize=(10,8))

orig_res=pd.DataFrame(index=alphas, columns=result.columns)
for j,alpha in enumerate(alphas):
    orig_res.iloc[j]=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,1.),
                                 index_col=1).iloc[0]

plt.subplot(4,2,1)
for i,beta in enumerate(betas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].K_T )
    plt.plot(alphas, res*1e-3, '.-', c=c[i])
#plt.plot(alphas, orig_res.K_T*1e-3, '--', c=c[5], label='original')
plt.ylabel(r'$\overline{\mathcal K}^T$ [GW]', fontsize=15)
plt.grid()

plt.subplot(4,2,2)
for i,beta in enumerate(betas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].K_B )
    plt.plot(alphas, res*1e-3, '.-', c=c[i])
#plt.plot(alphas, orig_res.K_B*1e-3, '--', c=c[5], label='original')
plt.ylabel(r'$\overline{\mathcal K}^B$ [GW]', fontsize=15)
plt.grid()

plt.subplot(4,2,3)
for i,beta in enumerate(betas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].T_c )
    plt.plot(alphas, res*1e-6, '.-', c=c[i])
#plt.plot(alphas, orig_res.T_c*1e-6, '--', c=c[5], label='original')
plt.ylabel(r'$\overline{\mathcal T}$ [GW$\cdot$1000km]', fontsize=15)
plt.grid()

plt.subplot(4,2,4)
for i,beta in enumerate(betas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].E_B )
    plt.plot(alphas, res*1e-3, '.-',  c=c[i])
#plt.plot(alphas, orig_res.E_B*1e-3, '--', c=c[5], label='original')
plt.ylabel(r'$\overline{E}^B$ [GWh]', fontsize=15)
plt.grid()

plt.subplot(4,2,5)
for i,beta in enumerate(betas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].M_var)
    plt.plot(alphas, res ,'.-',c=c[i])
#plt.plot(alphas, orig_res.M_var, '--', c=c[5], label='original')
plt.ylabel(r'Var$\left(\overline{\mathbf \Delta}\right) / \langle L_{EU} \rangle^2$', fontsize=15)
plt.grid()

plt.subplot(4,2,6)
for i,beta in enumerate(betas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].P_K95 )
    plt.plot(alphas, res,'.-',c=c[i])
#plt.plot(alphas, orig_res.P_K95, '--', c=c[5], label='original')
plt.ylabel(r'$\overline{K}_{95}^P$', fontsize=15)
plt.xlabel(r'$\alpha$', fontsize=19)
plt.grid()

plt.subplot(4,2,7)
for i,beta in enumerate(betas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].F_K95)
    plt.plot(alphas, res, '.-', label=r'$\beta=${}'.format(beta), c=c[i])
#plt.plot(alphas, orig_res.F_K95, '--', c=c[5], label='original')
plt.ylabel(r'$\overline{K}_{95}^F$', fontsize=15)
plt.xlabel(r'$\alpha$', fontsize=19)
plt.legend(ncol=3, bbox_to_anchor=(1.1,1), fontsize=14)
plt.grid()

plt.tight_layout()
plt.savefig(path + 'figures/obs_vs_alpha_M1494_delta{}.pdf'.format(delta))

#%% observable against alpha for different delta, beta fixed

beta=1.0
deltas=np.round(np.linspace(0,1,11), decimals=3)

orig_res=pd.DataFrame(index=alphas, columns=result.columns)
for j,alpha in enumerate(alphas):
    orig_res.iloc[j]=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,1.),
                                 index_col=1).iloc[0]

c=cm.get_cmap('viridis')(np.linspace(0,1,deltas.shape[0]))
plt.figure(figsize=(10,8))

plt.subplot(4,2,1)
for i,delta in enumerate(deltas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].K_T )
    plt.plot(deltas, res*1e-3, '.-', c=c[i])
plt.plot(alphas, orig_res.K_T*1e-3, '--', c=c[i], label='original')
plt.ylabel(r'$\overline{\mathcal K}^T$ [GW]', fontsize=15)
plt.grid()

plt.subplot(4,2,2)
for i,delta in enumerate(deltas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].K_B )
    plt.plot(deltas, res*1e-3, '.-', c=c[i])
plt.plot(alphas, orig_res.K_B*1e-3, '--', c=c[i], label='original')
plt.ylabel(r'$\overline{\mathcal K}^B$ [GW]', fontsize=15)
plt.grid()

plt.subplot(4,2,3)
for i,delta in enumerate(deltas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].T_c )
    plt.plot(deltas, res*1e-6, '.-', c=c[i])
plt.plot(alphas, orig_res.T_c*1e-6, '--', c=c[i], label='original')
plt.ylabel(r'$\overline{\mathcal T}$ [GW$\cdot$1000km]', fontsize=15)
plt.grid()

plt.subplot(4,2,4)
for i,delta in enumerate(deltas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].E_B )
    plt.plot(deltas, res*1e-3, '.-',  c=c[i])
plt.plot(alphas, orig_res.E_B*1e-3, '--', c=c[i], label='original')
plt.ylabel(r'$\overline{E}^B$ [GWh]', fontsize=15)
plt.grid()

plt.subplot(4,2,5)
for i,delta in enumerate(deltas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].M_var)
    plt.plot(deltas, res ,'.-',c=c[i])
plt.plot(alphas, orig_res.M_var, '--', c=c[i], label='original')
plt.ylabel(r'Var$\left(\overline{\mathbf \Delta}\right) / \langle L_{EU} \rangle^2$', fontsize=15)
plt.grid()

plt.subplot(4,2,6)
for i, delta in enumerate(deltas):
    res=np.array([])
    for j, alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].P_K95 )
    plt.plot(deltas, res,'.-',c=c[i])
plt.plot(alphas, orig_res.P_K95, '--', c=c[i], label='original')
plt.ylabel(r'$\overline{K}_{95}^P$', fontsize=15)
plt.xlabel(r'$\alpha$', fontsize=19)
plt.grid()

plt.subplot(4,2,7)
for i,delta in enumerate(deltas):
    res=np.array([])
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,beta),
                          index_col=1)
        res=np.append(res, frame.loc[delta].F_K95)
    plt.plot(deltas, res, '.-', label=r'$\delta=${}'.format(delta), c=c[i])
plt.plot(alphas, orig_res.F_K95, '--', c=c[i], label='original')
plt.ylabel(r'$\overline{K}_{95}^F$', fontsize=15)
plt.xlabel(r'$\alpha$', fontsize=19)
plt.legend(ncol=3, bbox_to_anchor=(1.1,1), fontsize=14)
plt.grid()

plt.tight_layout()
plt.savefig(path + 'figures/obs_vs_alpha_M1494_beta{}.pdf'.format(beta))



#%% Plot SCFs for different deltas

fig,ax=plt.subplots(1,2, figsize=(7,3))
c=cm.get_cmap('viridis')(np.linspace(0,1,deltas.shape[0]))

cor_w=pd.read_pickle(path+ 'scf_wind_copula_M1494_alpha1.0')
cor_len=pd.read_csv(path + 'cor_len_copula_M1494_alpha1.0_beta1.0',
                    index_col=0).loc['wind']

for i,delta in enumerate(deltas):
    ax[0].plot(cor_w.dist, cor_w.loc[:,str(delta)], c=c[i],
                                   label=r'$\delta={}$'.format(delta)) 
ax[0].plot(cor_w.dist, cor_w.loc[:,'original'], '--', c=c[i],label=r'original')
ax[0].legend(ncol=2)
ax[0].set_xlabel(r'Distance $d$ [km]', fontsize=14)
ax[0].set_ylabel(r'$Q_d(\mathbf G^W)$', fontsize=16)
ax[0].plot(np.arange(-200,3200), np.arange(-200,3200)*0, '--', linewidth=2, c='k')
ax[0].set_xlim([-200,3000])

ax[1].plot(deltas, cor_len.values[1:], '.-')
ax[1].set_xlabel(r'$\delta$', fontsize=16)
ax[1].set_ylabel(r'Correlation length $\xi_W$ [km]', fontsize=14)

plt.tight_layout()
plt.savefig(path + 'figures/scf_g_wind_M1494.pdf', bbox_inches='tight')

#%% Plot SCFs for mismatch and injection 

fig,ax=plt.subplots(2,3,figsize=(11,7))
c=cm.get_cmap('viridis')(np.linspace(0,1,deltas.shape[0]))

cor_m1=pd.read_pickle(path+ 'scf_mis_copula_M1494_alpha1.0_beta1.0')
cor_m2=pd.read_pickle(path+ 'scf_mis_copula_M1494_alpha0.8_beta1.0')
cor_len1=pd.read_csv(path + 'cor_len_copula_M1494_alpha1.0_beta1.0', index_col=0)
cor_len2=pd.read_csv(path + 'cor_len_copula_M1494_alpha0.8_beta1.0', index_col=0)

for i,delta in enumerate(deltas):
    ax[0,0].plot(cor_m1.dist, cor_m1.loc[:,str(delta)], c=c[i],
                                   label=r'$\delta={}$'.format(delta)) 
ax[0,0].plot(cor_m1.dist, cor_m1.loc[:,'original'], '--', c=c[i],label=r'original')
ax[0,0].legend(ncol=2)
ax[0,0].set_title(r'$\alpha=1.0$')
ax[0,0].set_xlabel(r'Distance $d$ [km]', fontsize=14)
ax[0,0].set_ylabel(r'$Q_d(\mathbf \Delta)$', fontsize=16)
ax[0,0].plot(np.arange(-200,3200), np.arange(-200,3200)*0, '--', linewidth=2, c='k')
ax[0,0].set_xlim([-200,3000])

for i,delta in enumerate(deltas):
    ax[0,1].plot(cor_m2.dist, cor_m2.loc[:,str(delta)], c=c[i],
                                   label=r'$\delta={}$'.format(delta)) 
ax[0,1].plot(cor_m2.dist, cor_m2.loc[:,'original'], '--', c=c[i])
ax[0,1].set_title(r'$\alpha=0.8$')
ax[0,1].set_xlabel(r'Distance $d$ [km]', fontsize=14)
ax[0,1].set_ylabel(r'$Q_d(\mathbf \Delta)$', fontsize=16)
ax[0,1].plot(np.arange(-200,3200), np.arange(-200,3200)*0, '--', linewidth=2, c='k')
ax[0,1].set_xlim([-200,3000])

ax[0,2].plot(deltas, cor_len1.loc['mis'].values[1:], '.-', label=r'$\alpha=1.0$')
ax[0,2].plot(deltas, cor_len2.loc['mis'].values[1:], '.-', label=r'$\alpha=0.8$')
ax[0,2].set_xlabel(r'$\delta$', fontsize=16)
ax[0,2].set_ylabel(r'Correlation length $\xi_{\Delta}$ [km]', fontsize=14)
ax[0,2].legend()

cor_p1=pd.read_pickle(path+ 'scf_inj_copula_M1494_alpha1.0_beta1.0')
cor_p2=pd.read_pickle(path+ 'scf_inj_copula_M1494_alpha0.8_beta1.0')

for i,delta in enumerate(deltas):
    ax[1,0].plot(cor_p1.dist, cor_p1.loc[:,str(delta)], c=c[i],
                                   label=r'$\delta={}$'.format(delta)) 
ax[1,0].plot(cor_p1.dist, cor_p1.loc[:,'original'], '--', c=c[i],label=r'original')
ax[1,0].legend(ncol=2)
ax[1,0].set_title(r'$\alpha=1.0$')
ax[1,0].set_xlabel(r'Distance $d$ [km]', fontsize=14)
ax[1,0].set_ylabel(r'$Q_d(\mathbf P)$', fontsize=16)
ax[1,0].plot(np.arange(-200,3200), np.arange(-200,3200)*0, '--', linewidth=2, c='k')
ax[1,0].set_xlim([-200,3000])

for i,delta in enumerate(deltas):
    ax[1,1].plot(cor_p2.dist, cor_p2.loc[:,str(delta)], c=c[i],
                                   label=r'$\delta={}$'.format(delta)) 
ax[1,1].plot(cor_p2.dist, cor_p2.loc[:,'original'], '--', c=c[i])
ax[1,1].set_title(r'$\alpha=0.8$')
ax[1,1].set_xlabel(r'Distance $d$ [km]', fontsize=14)
ax[1,1].set_ylabel(r'$Q_d(\mathbf P)$', fontsize=16)
ax[1,1].plot(np.arange(-200,3200), np.arange(-200,3200)*0, '--', linewidth=2, c='k')
ax[1,1].set_xlim([-200,3000])

ax[1,2].plot(deltas, cor_len1.loc['inj'].values[1:], '.-', label=r'$\alpha=1.0$')
ax[1,2].plot(deltas, cor_len2.loc['inj'].values[1:], '.-', label=r'$\alpha=0.8$')
ax[1,2].set_xlabel(r'$\delta$', fontsize=16)
ax[1,2].set_ylabel(r'Correlation length $\xi_P$ [km]', fontsize=14)
ax[1,2].legend()

plt.tight_layout()

plt.savefig(path+ 'figures/scf_mis_inj_M1494.pdf')

#%% observables vs delta for different betas, alpha=1

c=cm.get_cmap('viridis')(np.linspace(0,1,betas.shape[0]))
plt.figure(figsize=(10,8))

plt.subplot(4,2,1)
for i,beta in enumerate(betas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha1.0_beta{}'.format(beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].K_T*1e-3, '.-', c=c[i])
plt.ylabel(r'$\overline{\mathcal K}^T$ [GW]', fontsize=15)
plt.grid()

plt.subplot(4,2,2)
for i,beta in enumerate(betas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha1.0_beta{}'.format(beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].K_B*1e-3, '.-', c=c[i])
plt.ylabel(r'$\overline{\mathcal K}^B$ [GW]', fontsize=15)
plt.grid()

plt.subplot(4,2,3)
for i,beta in enumerate(betas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha1.0_beta{}'.format(beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].T_c*1e-6, '.-', c=c[i])
plt.ylabel(r'$\overline{\mathcal T}$ [GW$\cdot$1000km]', fontsize=15)
plt.grid()

plt.subplot(4,2,4)
for i,beta in enumerate(betas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha1.0_beta{}'.format(beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].E_B*1e-3, '.-',  c=c[i])
plt.ylabel(r'$\overline{E}^B$ [GWh]', fontsize=15)
plt.grid()

plt.subplot(4,2,5)
for i,beta in enumerate(betas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha1.0_beta{}'.format(beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].M_var,'.-',c=c[i])
plt.ylabel(r'Var$\left(\overline{\mathbf \Delta}\right) / \langle L_{EU} \rangle^2$', fontsize=15)
plt.grid()

plt.subplot(4,2,6)
for i,beta in enumerate(betas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha1.0_beta{}'.format(beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].P_K95,'.-',c=c[i])
plt.ylabel(r'$\overline{K}_{95}^P$', fontsize=15)
plt.xlabel(r'$\delta$', fontsize=19)
plt.grid()

plt.subplot(4,2,7)
for i,beta in enumerate(betas):
    result=pd.read_csv(path+ 'wind_copula_M1494_alpha1.0_beta{}'.format(beta))
    plt.plot(result.loc[1:].delta.values, result.loc[1:].F_K95,
             '.-', label=r'$\beta=${}'.format(beta), c=c[i])
plt.ylabel(r'$\overline{K}_{95}^F$', fontsize=15)
plt.xlabel(r'$\delta$', fontsize=19)
plt.legend(ncol=2)
plt.grid()

plt.tight_layout()
plt.savefig(path + 'figures/obs_vs_delta_M1494_alpha1.0.pdf')

#%% mismatch pca vs alpha for different beta and delta

fig, ax=plt.subplots(3,2, sharex='col', sharey='row', figsize=(10,10))

for i,beta in enumerate([1.,0., 2.]):
    for j,delta in enumerate([1.,0.]):
        
        res=np.zeros((alphas.shape[0], 1494))
        #Load and prepare results
        for n,alpha in enumerate(alphas):
            res[n]= np.load(path+ 'pca_mis_alpha{}_beta{}_delta{}_M1494.npz'.format(alpha,
                            beta,delta))['sm_eva']
            res[n]=res[n]/ np.sum(res[n]) 
        res=np.cumsum(res, axis=1)
        
        #plot results
        ax[i,j].fill_between(alphas, np.zeros(alphas.shape[0]),res.T[0], label='k=1')
        for n,curve in enumerate(res.T[1:9]):
            ax[i,j].fill_between(alphas, curve, res.T[n], label='k={}'.format(n+2))
        ax[i,j].fill_between(alphas, curve, np.ones(alphas.shape[0]), label=r'k$>$8')
        
        ax[i,j].grid()
        ax[i,j].autoscale(enable=True, tight=True)
        ax[i,j].set_title(r'$\delta=${}, $\beta=${}'.format(delta,beta))
        
ax[2,0].set_xlabel(r'$\alpha$', fontsize=19)
ax[2,1].set_xlabel(r'$\alpha$', fontsize=19)
ax[1,0].set_ylabel(r'$\tilde \lambda_k^{\downarrow}(\overline{\mathbf \Sigma}^\Delta)$', fontsize=19)
ax[1,1].legend(ncol=5, loc='lower center')
        
plt.tight_layout()
plt.savefig(path+'figures/mis_pca_vs_alpha.pdf')

#%% balancing and injection contribution to mismatch PCs

c=cm.get_cmap('tab10')(np.linspace(0,1,5))
fig, ax=plt.subplots(3,2, sharex='col', sharey='row', figsize=(10,10))

for i,beta in enumerate([1.,0., 2.]):
    for j,delta in enumerate([1.,0.]):
        
        bal_con=np.zeros((alphas.shape[0], 1494))
        inj_con=np.zeros((alphas.shape[0], 1494))
        
        #Load and prepare results
        for n,alpha in enumerate(alphas):
            result=np.load(path+ 'pca_mis_alpha{}_beta{}_delta{}_M1494.npz'.format(alpha,
                            beta,delta))
            bal_con[n]= result['bal_cont']/(result['sm_eva']).sum()
            inj_con[n]= result['inj_cont']/(result['sm_eva']).sum()
            
        bal_con=np.cumsum(bal_con, axis=1)
        inj_con=np.cumsum(inj_con, axis=1)
        
        #plot balancing contribution
        ax[i,j].fill_between(alphas, np.zeros(alphas.shape[0]),bal_con.T[0], label='k=1', facecolor=c[0])
        for n,curve in enumerate(bal_con.T[1:4]):
            ax[i,j].fill_between(alphas, curve, bal_con.T[n], label='k={}'.format(n+2), facecolor=c[n+1])
        ax[i,j].fill_between(alphas, curve, bal_con.T[-1], label=r'k$>$4', facecolor=c[-1])
        
        ax[i,j].plot(alphas, bal_con.T[-1], '--', linewidth=3, c='k')
        
        #plot injection contribution
        ax[i,j].fill_between(alphas, bal_con.T[-1], bal_con.T[-1]+inj_con.T[0], facecolor=c[0] )
        for n,curve in enumerate(bal_con.T[-1]+inj_con.T[1:4]):
            ax[i,j].fill_between(alphas, bal_con.T[-1]+inj_con.T[n], curve, facecolor=c[n+1])
        ax[i,j].fill_between(alphas, bal_con.T[-1]+inj_con.T[-1],curve, facecolor=c[-1])
        
        ax[i,j].grid()
        ax[i,j].autoscale(enable=True, tight=True)
        ax[i,j].set_title(r'$\delta=${}, $\beta=${}'.format(delta,beta))
        
ax[2,0].set_xlabel(r'$\alpha$', fontsize=19)
ax[2,1].set_xlabel(r'$\alpha$', fontsize=19)
ax[1,0].set_ylabel(r'$\tilde \lambda_k^{P,B}$', fontsize=19)
ax[1,1].legend(ncol=5, loc='lower center')
        
plt.tight_layout()
plt.savefig(path+'figures/mis_pca_cont_vs_alpha.pdf')

#%% Plot mismatch sum for different deltas and fixed alpha

deltas=np.arange(0,1.1,0.1)
beta=1.
fig, ax=plt.subplots(2,1)
c=cm.get_cmap('viridis')(np.linspace(0,1,deltas.shape[0]))

m_sum=pd.read_pickle(path + 'mis_sum_M1494_alpha{}_beta{}'.format(0.8,beta))
for i,delta in enumerate(deltas):
    ax[0].hist(m_sum.loc[:,str(delta)]/load.mean().sum(),
      bins=100, histtype='step', color=c[i], label=r'$\delta=${}'.format(delta),
      density=True)
ax[0].set_title(r'$\alpha=0.8$')
ax[0].set_ylabel('Probability density')
ax[0].set_xlim([-1.5,2])
ax[0].set_ylim([0,2.5])
ax[0].legend(ncol=3, fontsize=8)

m_sum=pd.read_pickle(path + 'mis_sum_M1494_alpha{}_beta{}'.format(1.,beta))
for i,delta in enumerate(deltas):
    ax[1].hist( (m_sum.loc[:,str(delta)]/load.mean().sum()).values,
      bins=100, histtype='step', color=c[i], density=True)
ax[1].set_title(r'$\alpha=1.0$')
ax[1].set_xlabel(r'$\Delta_{EU} / \langle L_{EU} \rangle$', fontsize=14)
ax[1].set_ylabel('Probability density')
ax[1].set_xlim([-1.5,2])
ax[1].set_ylim([0,2.5])

m_var=pd.read_csv(path+ 'wind_copula_M1494_alpha1.0_beta{}'.format(beta),
                    index_col='delta').loc[0, 'M_var']
bins=np.linspace(-0.5,0.5,100)
ax[1].plot(bins, norm.pdf(bins,0, np.sqrt(m_var)), '--', c='tab:orange', 
           label=r'$\mathcal N(0, \rm{Var}(\mathbf \Delta))$')
ax[1].plot(bins, norm.pdf(bins,0, np.sqrt(m_sum.var().iloc[1])/load.mean().sum()),
           '-', c='tab:orange', label=r'$\mathcal N(0, \rm{Var}(\sum_n \Delta^{\delta=0}_n))$')
ax[1].legend()

plt.tight_layout()
plt.savefig(path+ 'figures/mis_sum_M1494_beta{}.pdf'.format(beta))

#%% Difference in observables between original and copula model

beta=1.0

orig_res=pd.DataFrame(index=alphas, columns=result.columns)
cop_res=pd.DataFrame(index=alphas, columns=result.columns)

for j,alpha in enumerate(alphas):
    res=pd.read_csv(path+ 'wind_copula_M1494_alpha{}_beta{}'.format(alpha,1.),
                                 index_col=1)
    orig_res.iloc[j]=res.iloc[0]
    cop_res.iloc[j]=res.iloc[-1]

c=cm.get_cmap('viridis')(np.linspace(0,1,deltas.shape[0]))
plt.figure(figsize=(7.5,3))


#plt.plot(alphas, (orig_res.K_T-cop_res.K_T)/orig_res.K_T, '.-',
#         label=r'$(\mathcal K^T - \overline{\mathcal K}^T)/ \mathcal K^T$' )
plt.plot(alphas, (orig_res.T_c-cop_res.T_c)/orig_res.T_c, '.-',
         label=r'$(\mathcal T - \overline{\mathcal T})/\mathcal T$' )
plt.plot(alphas, (orig_res.K_B-cop_res.K_B)/orig_res.K_B, '.-',
         label=r'$(\mathcal K^B- \overline{\mathcal K}^B)/\mathcal K^B$' )
plt.plot(alphas, (orig_res.E_B-cop_res.E_B)/orig_res.E_B, '.-',
         label=r'$(E_B - \overline{E}_B)/E_B$' )
plt.plot(alphas, (orig_res.M_var-cop_res.M_var)/orig_res.M_var, '.-',
         label=r'$(\rm{Var}(\mathbf \Delta) - \rm{Var}(\overline{\mathbf \Delta}))/\rm{Var}{\mathbf \Delta}$' )

plt.legend(ncol=2, fontsize=13, loc='upper left')
plt.xlabel(r'$\alpha$', fontsize=20)
plt.ylim([-0.17,0.30])
plt.xlim([0,1.05])
plt.ylabel('Relative difference', fontsize=16)

plt.grid()
plt.savefig(path + 'figures/obs_diff_M1494.pdf', bbox_inches='tight' )


    
#%% Plot Kolmogorov-Smirnov distance

alphas=np.linspace(0,1.,11)

ks_w=pd.read_csv(path+'ksd_g_wind', index_col=0)
ks_m=pd.read_csv(path+'ksd_mismatch', index_col=0)
ks_p=pd.read_csv(path+'ksd_injection', index_col=0)
ks_f=pd.read_csv(path+'ksd_flow', index_col=0)

fig,ax= plt.subplots(2,2, figsize=(8,6), sharex=True, sharey=True)


ax[0,0].boxplot(ks_w.values, positions=alphas, widths=0.05, sym='', whis=[5,95])
ax[0,0].set_title(r'Wind generation $\mathbf G^W$')
ax[0,0].set_ylabel( r'$d_{KS}(G^W_n,\overline G^W_n)$', fontsize=17)

ax[0,1].boxplot(ks_m.values, positions=alphas, widths=0.05, sym='', whis=[5,95])
ax[0,1].set_title(r'Mismatch $\mathbf \Delta$')
ax[0,1].set_ylabel( r'$d_{KS}(\Delta_n,\overline \Delta_n)$', fontsize=17)

ax[1,0].set_xlabel(r'$\alpha$', fontsize=17)
ax[1,0].boxplot(ks_p.values, positions=alphas, widths=0.05, sym='', whis=[5,95])
ax[1,0].set_title(r'Injection $\mathbf P$')
ax[1,0].set_xlabel(r'$\alpha$', fontsize=17)
ax[1,0].set_ylabel( r'$d_{KS}(P_n,\overline P_n)$', fontsize=17)
    
ax[1,1].set_xlabel(r'$\alpha$', fontsize=17)
ax[1,1].set_ylabel( r'$d_{KS}(F_l,\overline F_l)$', fontsize=17)
ax[1,1].boxplot(ks_f.values, positions=alphas, widths=0.05, sym='', whis=[5,95])
ax[1,1].set_xlim([-0.1,1.1])
ax[1,1].set_ylim([-0.01,0.1])
ax[1,1].set_title(r'Flow $\mathbf F$')
    
plt.tight_layout()

plt.savefig(path+ 'figures/ksd_M1494.pdf', bbox_inches='tight' )


#%% Plot percentile differences between copula and original

alpha=1.
p_vals=np.arange(1,100,2)

fig,ax= plt.subplots(2,2, figsize=(8,6), sharex=True, sharey=True)

per=np.load(path + 'percentiles_M1494_alpha{}_original.npz'.format(alpha))['gwind']
per_c=np.load(path + 'percentiles_M1494_alpha{}_beta1.0_delta1.0.npz'.format(alpha))['gwind']
for i,per_diff in enumerate((per-per_c)/per): 
    ax[0,0].plot(p_vals[i]*np.ones(1494), per_diff, '.', c='b', markersize=0.1)
ax[0,0].boxplot(((per-per_c)/per).T, positions=p_vals, widths=4, sym='', whis=[5,95])
ax[0,0].set_title(r'Wind generation $\mathbf G^W$')

per=np.load(path + 'percentiles_M1494_alpha{}_original.npz'.format(alpha))['mis']
per_c=np.load(path + 'percentiles_M1494_alpha{}_beta1.0_delta1.0.npz'.format(alpha))['mis']
for i,per_diff in enumerate((per-per_c)/per): 
    ax[0,1].plot(p_vals[i]*np.ones(1494), per_diff, '.', c='b', markersize=0.1)
ax[0,1].boxplot(((per-per_c)/per).T, positions=p_vals, widths=4, sym='', whis=[5,95])
ax[0,1].set_title(r'Mismatch $\mathbf \Delta$')

per=np.load(path + 'percentiles_M1494_alpha{}_original.npz'.format(alpha))['inj']
per_c=np.load(path + 'percentiles_M1494_alpha{}_beta1.0_delta1.0.npz'.format(alpha))['inj']
for i,per_diff in enumerate((per-per_c)/per): 
    ax[1,0].plot(p_vals[i]*np.ones(1494), per_diff,
      '.', c='b', markersize=0.1)
ax[1,0].set_xlabel('q', fontsize=17)
ax[1,0].boxplot(((per-per_c)/per).T, positions=p_vals, widths=4, sym='', whis=[5,95])
ax[1,0].set_title(r'Injection $\mathbf P$')

per=np.load(path + 'percentiles_M1494_alpha{}_original.npz'.format(alpha))['flow']
per_c=np.load(path + 'percentiles_M1494_alpha{}_beta1.0_delta1.0.npz'.format(alpha))['flow']
for i,per_diff in enumerate((per-per_c)/per): 
    ax[1,1].plot(p_vals[i]*np.ones(2156), per_diff,
      '.', c='b', markersize=0.1)
ax[1,1].set_xlabel('q', fontsize=17)
ax[1,1].boxplot(((per-per_c)/per).T, positions=p_vals, widths=4, sym='', whis=[5,95])
ax[1,1].set_xlim([-5,95])
ax[1,1].set_ylim([-1,1])
ax[1,1].set_title(r'Flow $\mathbf F$')

fig.text(-0.02, 0.5, r'$(X_{q,n} - \overline{X}_{q,n})/X_{q,n}$', ha='center', 
         rotation='vertical', va='center', fontsize=19)

plt.tight_layout()

plt.savefig(path+ 'figures/percentile_diff_M1494_alpha{}.pdf'.format(alpha),
            bbox_inches='tight' )

#%% Make q-q plot from original data and copula model

alpha=1.0
p_vals=np.arange(1,100,2)
vnorm=load.mean().sum()
fig,ax= plt.subplots(2,2, figsize=(8,6))

per=np.load(path + 'percentiles_M1494_alpha{}_original.npz'.format(alpha))['gwind']/vnorm
per_c=np.load(path + 'percentiles_M1494_alpha{}_beta1.0_delta1.0.npz'.format(alpha))['gwind']/vnorm
ax[0,0].plot(per, per_c,'.', c='b',markersize=3)
ax[0,0].set_xlabel(r'$X_{q,n}/ \langle L_{EU} \rangle$', fontsize=17)
ax[0,0].set_ylabel( r'$\overline{X}_{q,n}/ \langle L_{EU} \rangle$', fontsize=17)
ax[0,0].set_title(r'Wind generation $\mathbf G^W$')
ax[0,0].plot(np.linspace(np.amin(per),np.amax(per)),
             np.linspace(np.amin(per),np.amax(per)), '-', c='r', linewidth=1)

per=np.load(path + 'percentiles_M1494_alpha{}_original.npz'.format(alpha))['mis']/vnorm
per_c=np.load(path + 'percentiles_M1494_alpha{}_beta1.0_delta1.0.npz'.format(alpha))['mis']/vnorm
ax[0,1].plot(per, per_c,'.', c='b',markersize=3)
ax[0,1].set_xlabel(r'$X_{q,n}/ \langle L_{EU} \rangle$', fontsize=17)
ax[0,1].set_ylabel( r'$\overline{X}_{q,n}/ \langle L_{EU} \rangle$', fontsize=17)
ax[0,1].set_title(r'Mismatch $\mathbf \Delta$')
ax[0,1].plot(np.linspace(np.amin(per),np.amax(per)),
             np.linspace(np.amin(per),np.amax(per)), '-', c='r', linewidth=1)

per=np.load(path + 'percentiles_M1494_alpha{}_original.npz'.format(alpha))['inj']/vnorm
per_c=np.load(path + 'percentiles_M1494_alpha{}_beta1.0_delta1.0.npz'.format(alpha))['inj']/vnorm
ax[1,0].plot(per, per_c,'.', c='b',markersize=3)
ax[1,0].set_xlabel(r'$X_{q,n}/ \langle L_{EU} \rangle$', fontsize=17)
ax[1,0].set_ylabel( r'$\overline{X}_{q,n}/ \langle L_{EU} \rangle$', fontsize=17)
ax[1,0].set_title(r'Injection $\mathbf P$')
ax[1,0].plot(np.linspace(np.amin(per),np.amax(per)),
             np.linspace(np.amin(per),np.amax(per)), '-', c='r', linewidth=1)

per=np.load(path + 'percentiles_M1494_alpha{}_original.npz'.format(alpha))['flow']/vnorm
per_c=np.load(path + 'percentiles_M1494_alpha{}_beta1.0_delta1.0.npz'.format(alpha))['flow']/vnorm
ax[1,1].plot(per, per_c,'.', c='b',markersize=3)
ax[1,1].set_xlabel(r'$X_{q,l}/ \langle L_{EU} \rangle$', fontsize=17)
ax[1,1].set_ylabel( r'$\overline{X}_{q,l}/ \langle L_{EU} \rangle$', fontsize=17)
ax[1,1].set_title(r'Flow $\mathbf F$')
ax[1,1].plot(np.linspace(np.amin(per),np.amax(per)),
             np.linspace(np.amin(per),np.amax(per)), '-', c='r', linewidth=1)


plt.tight_layout()

plt.savefig(path+ 'figures/q-q_plot_M1494_alpha{}.pdf'.format(alpha),
            bbox_inches='tight',dpi=300)

#%% Plot correlation differences between original data and copula model

alphas=np.linspace(0.,1.,11)

cd_w=pd.read_pickle(path+'corr_diff_gwind_M1494')
cd_m=pd.read_pickle(path+'corr_diff_mis_M1494')
cd_p=pd.read_pickle(path+'corr_diff_inj_M1494')
cd_f=pd.read_pickle(path+'corr_diff_flow_M1494')

fig,ax= plt.subplots(2,2, figsize=(8,6), sharex=True, sharey=True)


ax[0,0].boxplot(cd_w.values, positions=alphas, widths=0.05, sym='', whis=[5,95])
ax[0,0].set_title(r'Wind generation $\mathbf G^W$')

ax[0,1].boxplot(cd_m.values, positions=alphas, widths=0.05, sym='', whis=[5,95])
ax[0,1].set_title(r'Mismatch $\mathbf \Delta$')

ax[1,0].set_xlabel(r'$\alpha$', fontsize=17)
ax[1,0].boxplot(cd_p.values, positions=alphas, widths=0.05, sym='', whis=[5,95])
ax[1,0].set_title(r'Injection $\mathbf P$')

    
ax[1,1].set_xlabel(r'$\alpha$', fontsize=17)
ax[1,1].boxplot(cd_f.values, positions=alphas, widths=0.05, sym='', whis=[5,95])
ax[1,1].set_xlim([-0.1,1.1])
ax[1,1].set_ylim([-0.2,0.2])
ax[1,1].set_title(r'Flow $\mathbf F$')
    
fig.text(-0.02, 0.5, r'$\rm{Corr}(X_i, X_j) - \rm{Corr}(\overline{X}_i, \overline{X}_j)$',
         ha='center', 
         rotation='vertical', va='center', fontsize=19)

plt.tight_layout()

plt.savefig(path+ 'figures/corr_diff_M1494.pdf', bbox_inches='tight' )

#%% SCF with scatter plot for g_wind

g_solar, g_wind= ht.scale_layout(gwind,gsolar, load, net.buses, alpha=1.,
                                return_mis=False)

ncopula=tm.NormalCopula().estimate_cov(g_wind)
orig_cov=ncopula.cov.copy()

#ncopula.scale_cov(2)
#g_wind_c=ncopula.sample_joint_dist(g_wind)

cor_w=ht.spatial_corr(g_wind, net.buses.loc[:,'x':'y'].values)[0]
cor_vals=ht.spatial_corr(g_wind, net.buses.loc[:,'x':'y'].values,
                         no_avrg=True)

plt.figure(figsize=(4,3))
plt.plot(cor_vals.dij, cor_vals.correl, '.', markersize=1,rasterized=True,
         label=r'Corr$(G^W_n, G^W_m)$')
plt.plot(cor_w[:,0], cor_w[:,1], linewidth=4,  label=r'$Q_d(\mathbf G^W)$')
plt.legend(fontsize=13, markerscale=10 )
plt.xlabel('Distance d [km]', fontsize=12)
plt.ylabel('Correlation', fontsize=12)
plt.xlim([-100,3300])
plt.plot(np.arange(-100,3300), np.arange(-100,3300)*0, '--', linewidth=1, c='k')

plt.savefig(path+'figures/gwind_scf_scatter.pdf',dpi=300, bbox_inches='tight')

#%% Annual variability of infrastructure measures

net.load_dir(alpha=0.).balance().lpf()

annual_T=net.lines_t.p0.groupby(net.snapshots.year).apply(lambda x: x.abs().quantile(0.99))
annual_T=(net.lines.length*annual_T).sum(axis=1)
print (annual_T-annual_T.mean()).abs().max()/annual_T.mean()

back= -(net.mis + net.loads_t.p_set).clip_upper(0.)

annual_K_B=back.groupby(net.snapshots.year).quantile(0.99).sum(axis=1)
annual_E_B=back.groupby(net.snapshots.year).mean().sum(axis=1)

print (annual_K_B-annual_K_B.mean()).abs().max()/annual_K_B.mean()
print (annual_E_B-annual_E_B.mean()).abs().max()/annual_E_B.mean()

#0.0306145085874
#0.0131444373608
#0.00103268378951



