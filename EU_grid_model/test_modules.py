# -*- coding: utf-8 -*-

import lib.bal_flow_net as bfn
import numpy as np
import scipy.linalg as sl
import pandas as pd
import matplotlib.pyplot as plt
import os
import pypsa as ps
import networkx as nx
import lib.ts_manipulation as tm
import lib.help_tools as ht
import scipy.stats as stats


def spherical_distance(longlat1, longlat2):
    long1 = np.radians(longlat1[0])
    long2 = np.radians(longlat2[0])
    lat1 = np.radians(longlat1[1])
    lat2 = np.radians(longlat2[1])
    R = 6371. # the radius of the earth in km
    def hav(theta):
        return (1. - np.cos(theta))/2.
    return 2.*R*np.arcsin(np.sqrt(hav(lat2 - lat1)
            + np.cos(lat1)*np.cos(lat2)*hav(long2 - long1)))

path='./EU_grid_model/Results/test/'

#%% Define 3-node test directory

directory='/home/adminuser/Dokumente/Master_Arbeit/Code/EU_grid_model/' + \
'Results/data/3_node_test_network/'

if not os.path.exists(directory):
    os.makedirs(directory)


#%% Create 3-node test network with gaussian mismatch and load time series

small_net=ps.Network()

small_net.add('Bus','1', x=0.5, y=1.)
small_net.add('Bus','2', x=1., y=0.)
small_net.add('Bus','3', x=0., y=0.)
small_net.add('Line','1', bus0='1', bus1='2', x=1.)
small_net.add('Line','2', bus0='2', bus1='3', x=1.)
small_net.add('Line','3', bus0='3', bus1='1', x=1.)
small_net.madd('Load', small_net.buses.index, bus=small_net.buses.index)
small_net.set_snapshots(pd.to_datetime(np.arange(9999)))
small_net.export_to_csv_folder(directory)

pd.DataFrame(data=np.array([np.random.normal(0.,0.8,9999),
                            np.random.normal(0.,0.5,9999),
                            np.random.normal(0.,0.2,9999)]).reshape((3,9999)).T,
             columns=small_net.buses.index,
             index=small_net.snapshots).to_pickle(directory + 'mismatch.gzip')

pd.DataFrame(data=np.array([np.random.normal(0.25,0.8,9999),
                            np.random.normal(0.25,0.2,9999),
                            np.random.normal(0.5,0.1,9999)]).reshape((3,9999)).T,
             columns=small_net.buses.index,
             index=small_net.snapshots).to_pickle(directory + 'load.gzip')


#%% Calculate 3-node network

small_net=bfn.SyBaNet().load_top(folder=directory).load_dir()
small_net.balance().lpf()

#%% Test synchronized balancing and flow calculation on 3-node network

#Injection: Calculated by SyBaNet and directly
print '\n ----------Injection at time step 100:------- \n'
print -small_net.loads_t.p_set.iloc[100]
print small_net.mis.iloc[100] - np.array([0.25,0.25,0.5])*small_net.mis.iloc[100].sum()
print small_net.mis.iloc[100] -\
      small_net.load.mean()*small_net.mis.iloc[100].sum()/small_net.load.mean().sum()

#Ptdf matrix (as calculated by SyBaNet)
elist=small_net.lines.loc[:,['bus1', 'bus0']].values
nlist=small_net.buses.index.values
graph=small_net.graph()

K=nx.incidence_matrix(graph,
                      oriented=True,
                      edgelist=elist,
                      nodelist=nlist).toarray()

Li=sl.pinvh(nx.laplacian_matrix(graph,nodelist=nlist).toarray())
ptdf=pd.DataFrame(data=np.dot(K.T, Li),
                    index=small_net.lines.index.values,
                    columns=nlist)

#Incidence matrix and direction of links 
print '\n ------Edge incidence matrix:---------- \n'
print K
print small_net.lines.loc[:, 'bus0':'bus1']

#Flows: From SyBaNet and calculated directly with ptdf-matrix
print '\n -----------Flows at time step 100:-------------- \n'
print small_net.lines_t.p0.iloc[100]    
print np.dot(ptdf, -small_net.loads_t.p_set.iloc[100])

#%% Define coarse-graining test directory

directory='/home/adminuser/Dokumente/Master_Arbeit/Code/EU_grid_model/' + \
'Results/data/coarse_grain_test_network/'

if not os.path.exists(directory):
    os.makedirs(directory)

#%% Create coarse-grain test network

scale_net=ps.Network()

scale_net.add('Bus','1', x=0.4, y=0.5)
scale_net.add('Bus','2', x=0.5, y=1.6)
scale_net.add('Bus','3', x=0.2, y=1.5)
scale_net.add('Bus','4', x=1.4, y=1.5)
scale_net.add('Bus','5', x=1.5, y=1.8)
scale_net.add('Bus','6', x=1.5, y=0.5)
scale_net.add('Bus','7', x=1.2, y=0.6)
scale_net.add('Bus','8', x=1.2, y=0.4)

scale_net.add('Line','1', bus0='1', bus1='2', x=1.)
scale_net.add('Line','2', bus0='2', bus1='3', x=1.)
scale_net.add('Line','3', bus0='2', bus1='4', x=1.)
scale_net.add('Line','4', bus0='2', bus1='5', x=1.)
scale_net.add('Line','5', bus0='4', bus1='5', x=1.)
scale_net.add('Line','6', bus0='4', bus1='6', x=1.)
scale_net.add('Line','7', bus0='6', bus1='7', x=1.)
scale_net.add('Line','8', bus0='6', bus1='8', x=1.)

scale_net.madd('Load', small_net.buses.index, bus=small_net.buses.index)
scale_net.set_snapshots(pd.to_datetime(np.arange(9999)))
scale_net.export_to_csv_folder(directory)

pd.DataFrame(data=np.array([np.random.normal(0.,0.2,9999),
                            np.random.normal(0.,0.8,9999),
                            np.random.normal(0.,0.5,9999)]).reshape((3,9999)).T,
             columns=['1','2','3'],
             index=small_net.snapshots).to_pickle(directory + 'mismatch.gzip')

pd.DataFrame(data=np.array([np.random.normal(0.1,0.8,9999),
                            np.random.normal(0.3,0.8,9999),
                            np.random.normal(0.6,0.2,9999)]).reshape((3,9999)).T,
             columns=['1','2','3'],
             index=small_net.snapshots).to_pickle(directory + 'load.gzip')

#%% Load coarse-grain test network

scale_net=bfn.SyBaNet().load_top(folder=directory).load_dir()

#%% Test coarse-graining 

new_net,(x_ed, y_ed)=bfn.coarse_grain(scale_net, nbin=2)
new_net.balance().lpf()

#Plot New and old network
print '\n\n--------Check new nodes and lines--------------\n'
X,Y=np.meshgrid(x_ed,y_ed)
scale_net.plot(bus_colors='g', line_colors='g')
new_net.plot(bus_colors='r', line_colors='r')
plt.xticks(x_ed)
plt.yticks(y_ed)
plt.grid()
plt.savefig(path+'coarse_grain_test_network.pdf')
plt.show()
plt.clf()
#Sum original time serie and compare to new_net time series

print '\n--------Check aggregation of mismatch---------------\n'
print '\nNode 2 and 3 should be aggregated to node 1:'
plt.plot(np.arange(100), scale_net.mis.loc[:,'2':'3'].sum(axis=1)[:100],linewidth=5)
plt.plot(np.arange(100), new_net.mis.loc[:,'1'][:100])
plt.show()
print '\nIs node 1 also at the correct position?'
print new_net.buses.loc[:,'x':'y']

#Check linear power flow
print '\n------Check new power flows--------\n'
print new_net.lines_t.p0.mean()
print '\nAre power flows at correct lines? Lines:'
print new_net.lines.loc[:,'bus0':'bus1']

#Check length calculation
print '\n---------Check length calculation---------\n'
print '\n new_net lengths:'
print new_net.lines.length
print '\n lengths calculated with old spherical_distance function:'
for i,line in new_net.lines.iterrows():
    longlat1=new_net.buses.loc[line.loc['bus0'],'x':'y'].astype(np.float).values
    longlat2=new_net.buses.loc[line.loc['bus1'],'x':'y'].astype(np.float).values
    print line.name, ': ', spherical_distance(longlat1, longlat2) 
print '\n Length of new line 0 calculated directly:'
print spherical_distance(( 0.40 , 0.50),( 0.35 , 1.55))

#Check Transmission capacity
print '--------------Check Transmission capacity (here 99.7% quantile)---------------'
Kl=new_net.lines_t.p0.abs().quantile(0.997)
print Kl
print '\nSince F_l should be gaussian, the 99.7% quantile should be approx. 3*sigma:'
print new_net.lines_t.p0.std()*3


#%% Check coarse-graining on Eu grid: Only Topology

nbins= np.unique(np.round(2**np.arange(1,6.2,0.2)))[::-1]
result=pd.DataFrame(index=range(nbins.shape[0]+1),
                    columns=['dx','nbin','M','dM','K', 'Tk'],
                    dtype=np.float)

orig_net=bfn.SyBaNet().load_top().load_dir()

result.iloc[0].nbin=None
result.iloc[0].M=orig_net.buses.shape[0]
result.iloc[0].dM=orig_net.lines.length.mean()

net=orig_net
for i,nbin in enumerate(nbins, start=1):
    net, x_ed, y_ed=bfn.coarse_grain(net, nbin=nbin)

    result.iloc[i].nbin=nbin
    result.iloc[i].M=net.buses.shape[0]
    result.iloc[i].dM=net.lines.length.mean()
    
    print i,' out of ', nbins.shape[0], ' done! \n'
    
    
#%% Check copula method with uncorrelated uniform variabels
    
data=pd.DataFrame(np.random.uniform([0,0,0], [10,20,30], (20000,3)) )
nc=tm.NormalCopula().estimate_cov(data)   
res=nc.sample_joint_dist(data)
    
cres=np.corrcoef(res,rowvar=False)
cor=np.corrcoef(data,rowvar=False)

plt.imshow( np.abs((cres-cor)/cor) )
cb=plt.colorbar()
cb.set_label(label=r'$|(\rho_{copula}-\rho_{original})/\rho_{original}|$',
             size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.show()
plt.clf()

plt.imshow( cres )
cb=plt.colorbar()
cb.set_label(label=r'$\rho_{copula}$',
             size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.show()
plt.clf()

plt.imshow( cor )
cb=plt.colorbar()
cb.set_label(label=r'$\rho_{original}$',
             size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.show()
plt.clf()
   
#%% Check copula method with aggregated re-eu mismatch patterns
 
orig_net=bfn.BaFloNet().load_top().load_dir()
net=bfn.coarse_grain(orig_net, nbin=5)[0]

#%%   
nc=tm.NormalCopula().estimate_cov(net.mis)   
res=nc.sample_joint_dist(net.mis)
    

cres=np.corrcoef(res,rowvar=False)
cor=np.corrcoef(net.mis.values,rowvar=False)

plt.imshow( np.abs((cres-cor)/cor) )
cb=plt.colorbar()
cb.set_label(label=r'$|(\rho_{copula}-\rho_{original})/\rho_{original}|$',
             size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+'re-eu_mismatch_N24_copula_corr_rel_error.pdf')
plt.show()
plt.clf()

plt.imshow( cres )
cb=plt.colorbar()
cb.set_label(label=r'$\rho_{copula}$',
             size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+'re-eu_mismatch_N24_copula_corr.pdf')
plt.show()
plt.clf()

plt.imshow( cor )
cb=plt.colorbar()
cb.set_label(label=r'$\rho_{original}$',
             size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+'re-eu_mismatch_N24_original_corr.pdf')
plt.show()
plt.clf()

plt.hist(net.mis.values[:,0], bins=30, histtype='step', linewidth=4,
         normed=True, label='Original node 1')
plt.hist(res.values[:,0], bins=30, histtype='step', linewidth=4, normed=True,
         label='Copula node 1')
plt.xlabel('Mismatch')
plt.ylabel('Propability density')
plt.legend()
plt.savefig(path+'re-eu_mismatch_N24_marginal_distribution_node_1.pdf')

#%% Plot bivariate gaussian copula

x, y = np.mgrid[0.01:1:.01, 0.01:1:.01]
u = stats.norm.ppf(np.dstack((x, y)))

rv = stats.multivariate_normal([0, 0], [[1.0, 0.3], [0.3, 1]])
rv_ind = stats.multivariate_normal([0, 0], [[1.0, 0.], [0., 1]])

#Copula
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
im1=ax1.contourf(x, y, rv.cdf(u))
fig1.colorbar(im1)
fig1.savefig(path+'normal_copula.pdf')

#Copula density

fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
im2=ax2.contourf(x, y, rv.pdf(u)*(rv_ind.pdf(u))**(-1))
fig2.colorbar(im2)
fig2.savefig(path+'normal_copula_density.pdf')


#%% Test principal component manipulation


ts=pd.DataFrame(np.random.randn(10000,4))
cov=ts.cov()

pvar, pcom= sl.eigh(cov)
new_cov=tm.MaPriCo(ts).import_pc(pvar, pcom).new_cov(2)
pvar_new, pcom_new= sl.eigh(new_cov)    

print pvar
print
print pvar_new 
print
print pcom
print
print pcom_new 

new_ts=tm.MaPriCo(ts).import_pc(pvar, pcom).new_ts(2)
pvar_new, pcom_new= sl.eigh(new_ts.cov())    

print
print pvar
print
print pvar_new 
print
print pcom
print
print pcom_new 
    
    
#%% Test analytical approximation to T=H^TH

epsilon=1e-1

nodelist=['1','2','3','4']
edgelist=[('1','2'),('2','3'),('3','4'),('4','1')]

w=np.ones(4)+ np.random.randn(4)*epsilon
edges=[('1','2', {'weight':w[0]}),
       ('2','3', {'weight':w[1]}),
       ('3','4', {'weight':w[2]}),
       ('4','1', {'weight':w[3]})]

g=nx.Graph()
g.add_nodes_from(nodelist)
g.add_edges_from(edges)

B=nx.laplacian_matrix(g, nodelist=nodelist).toarray()
L=nx.laplacian_matrix(g, nodelist=nodelist, weight=None).toarray()
K=nx.incidence_matrix(g, oriented=True, nodelist=nodelist,
                      edgelist=edgelist).toarray()

omega=np.diag(w)
Bi=sl.pinv(B)
Li=sl.pinv(L)
H=np.dot(omega,np.dot(K.T,Bi))
T=np.dot(H.T,H)
Ti=np.around(sl.pinv(T), decimals=10)

dB=B-L
domega=np.diag(w-np.ones(4))
Ti_a=np.around(L+ np.dot(dB,np.dot(Li,dB)) - np.dot(K,np.dot(domega**2,K.T)),decimals=10) 

Ti1=np.dot(dB,np.dot(Li,dB))
Ti2=np.dot(K,np.dot(domega**2,K.T))


gc=nx.complete_graph(4)
Lic=sl.pinv(nx.laplacian_matrix(gc).toarray())
Kc=nx.incidence_matrix(gc).toarray()



    
    
    
    
    
    
    
    
    
    
    
    
    