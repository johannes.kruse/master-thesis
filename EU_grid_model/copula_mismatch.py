# -*- coding: utf-8 -*-

#Apply the normal-copula model to nodal mismatch. Parameter-sweep and analysis
#of results

import lib.bal_flow_net as bfn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import lib.observables as ob
import lib.ts_manipulation as tm
import matplotlib.cm as cm

path= './EU_grid_model/Results/copula_method/copula_mismatch/'

#%% Define parameter ranges

nbins= np.unique(np.round(2**np.arange(1,6.3,0.2)))[::-1]
result=pd.DataFrame(index=range(nbins.shape[0]+1),
                    columns=['nbin','M','dM', 'F_K95', 'P_K95', 'K', 'Tk'],
                    dtype=np.float)
alphas=np.linspace(0.,1., 21)
deltas=np.linspace(0.,1.,11)



#%% Scale mismatch correlations of original network 


for n,alpha in enumerate(alphas):    
    for m,delta in enumerate(deltas):
        
        #Load re-eu network and rescale solar-wind layout
        orig_net=bfn.BaFloNet().load_top().load_dir(alpha=alpha)
        
        #Apply copula to rescale correlations
        ncopula=tm.NormalCopula().estimate_cov(orig_net.mis).scale_cov(delta)
        orig_net.mis=ncopula.sample_joint_dist(orig_net.mis)
        
        #Calculate eigenvalues 
        orig_net.balance().calc_sp().calc_sf().calc_hth()
        orig_net.calc_peig().calc_feig().lpf()
        
        #Calculate transmission capacity
        Kl=orig_net.lines_t.p0.abs().quantile(0.99)
        
        #Save results
        result.iloc[0].nbin=None
        result.iloc[0].M=orig_net.buses.shape[0]
        result.iloc[0].dM=orig_net.lines.length.mean()
        result.iloc[0].P_K95=ob.qx(orig_net.sp_eva, 0.95)[0]
        result.iloc[0].F_K95=ob.qx(orig_net.sf_eva, 0.95)[0]
        result.iloc[0].K=Kl.sum()
        result.iloc[0].Tk=(Kl*orig_net.lines.length).sum()

        #Coarse-graining
        net=orig_net
        for i,nbin in enumerate(nbins, start=1):
            
            net=bfn.coarse_grain(net, nbin=nbin)[0]
            
            #Calculate eigenvalues 
            net.balance().calc_sp().calc_sf().calc_hth()
            net.calc_peig().calc_feig().lpf()
        
            #Calculate transmission capacity
            Kl=net.lines_t.p0.abs().quantile(0.99)
            
            #Save results
            result.iloc[i].nbin=nbin
            result.iloc[i].M=net.buses.shape[0]
            result.iloc[i].dM=net.lines.length.mean()
            result.iloc[i].P_K95=ob.qx(net.sp_eva, 0.95)[0]
            result.iloc[i].F_K95=ob.qx(net.sf_eva, 0.95)[0]
            result.iloc[i].K=Kl.sum()
            result.iloc[i].Tk=(Kl*net.lines.length).sum()
                    
            print 'Coarse-graining: ', i,' out of ', nbins.shape[0], ' done! \n'

        #Save alpha-delta result to file
        result.to_csv(path+'alpha{}_delta{}.csv'.format(alpha,delta))
        #Clear results
        result*=np.nan
    
        print '\n-----------------------------------------------\n'     
        print 'Parameters: ', n*deltas.shape[0]+m,' out of ',\
        deltas.shape[0]*alphas.shape[0], ' done! \n'
        print '\n-----------------------------------------------\n'    



#%% observables vs network size
        
alpha=0.
c=cm.get_cmap('viridis')(np.linspace(0,1,deltas.shape[0]+1))

f1=plt.figure(1)
f2=plt.figure(2)
f3=plt.figure(3)
f4=plt.figure(4)
f5=plt.figure(5)

for i,delta in enumerate(deltas):
    result=pd.read_csv(path+'alpha{}_delta{}.csv'.format(alpha,delta))
    
    if delta!=0:
        plt.figure(1)
        l1,=plt.plot(result.M, result.loc[:,'F_K95'], '-', color=c[i],
                 label=r'$\delta={}$'.format(delta))
        l2,=plt.plot(result.M, result.loc[:,'P_K95'].values, '--', color=c[i])

    plt.figure(2)
    plt.plot(result.M, result.loc[:,'K'], '.-', color=c[i],
             label=r'$\delta={}$'.format(delta))
    plt.figure(3)
    plt.plot(result.M, result.loc[:,'Tk'], '.-', color=c[i],
             label=r'$\delta={}$'.format(delta))
    plt.figure(4)
    plt.plot(result.M, result.loc[:,'F_K95'], '.-', color=c[i],
                 label=r'$\delta={}$'.format(delta))
    plt.figure(5)
    plt.plot(result.M, result.loc[:,'P_K95'].values, '.-', color=c[i],
             label=r'$\delta={}$'.format(delta))

res_orig=pd.read_csv('./EU_grid_model/Results/coarse_graining/'+\
                         'original_re-eu_alpha{}.csv'.format(alpha))

plt.figure(1)
plt.plot(res_orig.M, res_orig.loc[:,'F_K95'], '-', color=c[-1],label='Original')
plt.plot(res_orig.M, res_orig.loc[:,'P_K95'].values, '--', color=c[-1],)
plt.subplots_adjust(bottom=0.4)
leg1=plt.legend(bbox_to_anchor=(1.05, -0.3), ncol=5) 
plt.legend([l2,l1], [r'Injection',r'Flow'])
plt.gca().add_artist(leg1)
plt.xlabel('Network size M', fontsize=12)
plt.yscale('log')
plt.ylabel(r'95-Percentile of PCs', fontsize=12)
plt.savefig(path+'figures/K_q_vs_M_alpha{}.pdf'.format(alpha))

plt.figure(2)
plt.plot(res_orig.M, res_orig.loc[:,'K'], '-', color=c[-1],label='Original')
plt.subplots_adjust(bottom=0.4)
plt.legend(bbox_to_anchor=(1.05, -0.3), ncol=5) 
plt.xlabel('Network size M', fontsize=12)
plt.ylabel(r'Transmission capacity [MWh]', fontsize=12)
plt.savefig(path+'figures/trans_cap_vs_M_alpha{}.pdf'.format(alpha),bbox_inches="tight")

plt.figure(3)
plt.plot(res_orig.M, res_orig.loc[:,'Tk'], '-', color=c[-1],label='Original')
plt.subplots_adjust(bottom=0.4)
plt.legend(bbox_to_anchor=(1.05, -0.3), ncol=5) 
plt.xlabel('Network size M', fontsize=12)
plt.ylabel(r'Transmission cost [MWh $\cdot$ km]', fontsize=12)
plt.savefig(path+'figures/trans_cost_vs_M_alpha{}.pdf'.format(alpha),bbox_inches="tight")

plt.figure(4)
plt.plot(res_orig.M, res_orig.loc[:,'F_K95'], '-', color=c[-1],label='Original')
plt.subplots_adjust(bottom=0.4)
plt.legend(bbox_to_anchor=(1.05, -0.3), ncol=5) 
plt.xlabel('Network size M', fontsize=12)
plt.ylabel(r'95-Percentile of Flow-PCs', fontsize=12)
plt.savefig(path+'figures/K_q_vs_M_flow_alpha{}.pdf'.format(alpha))

plt.figure(5)
plt.plot(res_orig.M, res_orig.loc[:,'P_K95'].values, '-', color=c[-1],label='Original')
plt.subplots_adjust(bottom=0.4)
leg1=plt.legend(bbox_to_anchor=(1.05, -0.3), ncol=5) 
plt.xlabel('Network size M', fontsize=12)
plt.ylabel(r'95-Percentile of Injection-PCs', fontsize=12)
plt.savefig(path+'figures/K_q_vs_M_injection_alpha{}.pdf'.format(alpha))


#%% observables vs alpha

nbin=32 #for original resolution concatenate frame.loc[frame.loc[:,'nbin'].isnull()] 
#nbin=7,32,original resolution
c=cm.get_cmap('viridis')(np.linspace(0,1,deltas.shape[0]+1))

f1=plt.figure(1)
f2=plt.figure(2)
f3=plt.figure(3)
f4=plt.figure(4)


for i,delta in enumerate(deltas):
    result=pd.DataFrame()
    
    for j,alpha in enumerate(alphas):
        frame=pd.read_csv(path+'alpha{}_delta{}.csv'.format(alpha,delta))
        frame.loc[:,'alpha']=alpha
        result=pd.concat([result,frame.loc[frame.loc[:,'nbin'].isnull()]])
        
    result.sort_values(by='alpha', inplace=True)
    
    plt.figure(1)
    plt.plot(result.alpha, result.loc[:,'F_K95'], '.-', color=c[i],
             label=r'$\delta={}$'.format(delta))
    
    plt.figure(2)
    plt.plot(result.alpha, result.loc[:,'P_K95'], '.-', color=c[i],
             label=r'$\delta={}$'.format(delta))
    
    plt.figure(3)
    plt.plot(result.alpha, result.loc[:,'K'], '.-', color=c[i],
             label=r'$\delta={}$'.format(delta))
    
    plt.figure(4)
    plt.plot(result.alpha, result.loc[:,'Tk'], '.-', color=c[i],
             label=r'$\delta={}$'.format(delta))

res_orig=pd.DataFrame()    
for j,alpha in enumerate(alphas):
    frame=pd.read_csv('./EU_grid_model/Results/coarse_graining/'+\
                     'original_re-eu_alpha{}.csv'.format(alpha))
    frame.loc[:,'alpha']=alpha
    res_orig=pd.concat([res_orig,frame.loc[frame.loc[:,'nbin'].isnull()]])
    
res_orig.sort_values(by='alpha', inplace=True)    
    
plt.figure(1)
plt.plot(res_orig.alpha, res_orig.loc[:,'F_K95'], '-', color=c[-1],label='Original')
plt.subplots_adjust(bottom=0.4)
plt.legend(bbox_to_anchor=(1.05, -0.3), ncol=5) 
plt.xlabel(r'$\alpha$', fontsize=15)
plt.ylabel(r'95-Percentile of Flow-PCs', fontsize=15)
plt.savefig(path+'figures/Kq_flow_vs_alpha_M{}.pdf'.format(result.M.iloc[0]))

plt.figure(2)
plt.plot(res_orig.alpha, res_orig.loc[:,'P_K95'], '-', color=c[-1],label='Original')
plt.subplots_adjust(bottom=0.4)
plt.legend(bbox_to_anchor=(1.05, -0.3), ncol=5) 
plt.xlabel(r'$\alpha$', fontsize=15)
plt.ylabel(r'95-Percentile of Injection-PCs', fontsize=15)
plt.savefig(path+'figures/Kq_injection_vs_alpha_M{}.pdf'.format(result.M.iloc[0]))

plt.figure(3)
plt.plot(res_orig.alpha, res_orig.loc[:,'K'], '-', color=c[-1],label='Original')
plt.subplots_adjust(bottom=0.4)
plt.legend(bbox_to_anchor=(1.05, -0.3), ncol=5) 
plt.xlabel(r'$\alpha$', fontsize=15)
plt.ylabel(r'Transmission capacity [MWh]', fontsize=15)
plt.savefig(path+'figures/trans_cap_vs_alpha_M{}.pdf'.format(result.M.iloc[0]),
            bbox_inches="tight")

plt.figure(4)
plt.plot(res_orig.alpha, res_orig.loc[:,'Tk'], '-', color=c[-1],label='Original')
plt.subplots_adjust(bottom=0.4)
plt.legend(bbox_to_anchor=(1.05, -0.3), ncol=5) 
plt.xlabel(r'$\alpha$', fontsize=15)
plt.ylabel(r'Transmission cost [MwH $\cdot$ km]', fontsize=15)
plt.savefig(path+'figures/trans_cost_vs_alpha_M{}.pdf'.format(result.M.iloc[0]),
            bbox_inches="tight")

#%% Difference between copula(delta=1) and original network

delta=1.
c=cm.get_cmap('viridis')(np.linspace(0,1,alphas[::2].shape[0]))

f2=plt.figure(1)
f3=plt.figure(2)
f4=plt.figure(3)
f5=plt.figure(4)

for i,alpha in enumerate(alphas[::2]):
    result=pd.read_csv(path+'alpha{}_delta{}.csv'.format(alpha,delta))
    res_orig=pd.read_csv('./EU_grid_model/Results/coarse_graining/'+\
                         'original_re-eu_alpha{}.csv'.format(alpha))
     
    plt.figure(1)
    plt.plot(result.M, (result.loc[:,'K']-res_orig.loc[:,'K'])/res_orig.loc[:,'K'],
             '.-', color=c[i], label=r'$\alpha={}$'.format(alpha))
    plt.figure(2)
    plt.plot(result.M, (result.loc[:,'Tk']-res_orig.loc[:,'Tk'])/res_orig.loc[:,'Tk'],
             '.-', color=c[i], label=r'$\alpha={}$'.format(alpha))
    plt.figure(3)
    plt.plot(result.M,
             (result.loc[:,'F_K95']-res_orig.loc[:,'F_K95'])/res_orig.loc[:,'F_K95'],
             '.-', color=c[i], label=r'$\alpha={}$'.format(alpha))
    plt.figure(4)
    plt.plot(result.M,
             (result.loc[:,'P_K95']-res_orig.loc[:,'P_K95'])/res_orig.loc[:,'P_K95'],
             '.-', color=c[i], label=r'$\alpha={}$'.format(alpha))



plt.figure(1)
plt.subplots_adjust(bottom=0.45)
plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.xlabel('Network size M', fontsize=12)
plt.ylabel(r'$\left(\mathcal K_{\delta=1}-\mathcal K_{orig} \right)/ \mathcal K_{orig}$',
          fontsize=12)
plt.savefig(path+'figures/trans_cap_diff_vs_M.pdf',bbox_inches="tight")

plt.figure(2)
plt.subplots_adjust(bottom=0.45)
plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.xlabel('Network size M', fontsize=12)
plt.ylabel(r'$\left( \mathcal T_{\delta=1}-\mathcal T_{orig}\right)/ \mathcal T_{orig}$',
           fontsize=12)
plt.savefig(path+'figures/trans_cost_diff_vs_M.pdf',bbox_inches="tight")

plt.figure(3)
plt.subplots_adjust(bottom=0.45)
plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.xlabel('Network size M', fontsize=12)
plt.ylabel(r'$\left(K_{95}^{F,orig}-K_{95}^{F,\delta=1} \right)/ K_{95}^{F,orig}$',
           fontsize=12)
plt.savefig(path+'figures/K_q_diff_vs_M_flow.pdf')

plt.figure(4)
plt.subplots_adjust(bottom=0.45)
leg1=plt.legend(bbox_to_anchor=(1.1, -0.3), ncol=5) 
plt.xlabel('Network size M', fontsize=12)
plt.ylabel(r'$\left(K_{95}^{P,orig}-K_{95}^{P,\delta=1} \right)/ K_{95}^{P,orig}$',
           fontsize=12)
plt.savefig(path+'figures/K_q_diff_vs_M_injection.pdf')

#%% Analytical approximations


alpha=0.
c=cm.get_cmap('viridis')(np.linspace(0,2,deltas[::4].shape[0]+1))
p_k95=lambda x: x*1.3
f_k95=lambda x: x**(0.8)*1.2

    
for i,delta in enumerate([1.,0.]):
    result=pd.read_csv(path+'alpha{}_delta{}.csv'.format(alpha,delta))
    
    l1,=plt.plot(result.M/1494., result.loc[:,'F_K95']/result.loc[0,'F_K95'],
                 'o', color=c[i], label=r'$\delta={}$'.format(delta))
    l2,=plt.plot(result.M/1494., result.loc[:,'P_K95'].values/result.loc[0,'P_K95'],
                 '<', color=c[i])


res_orig=pd.read_csv('./EU_grid_model/Results/coarse_graining/'+\
                         'original_re-eu_alpha{}.csv'.format(alpha))

Ms=np.arange(3,1494.)/1494.
plt.plot(Ms, p_k95(Ms), '--', color='r',label=r'$\sim M$')
plt.plot(Ms, f_k95(Ms), '-', color='r',label=r'$\sim M^{0.8}$')

#plt.plot(res_orig.M, res_orig.loc[:,'F_K95'], '-', color=c[-1],label='Original')
#plt.plot(res_orig.M, res_orig.loc[:,'P_K95'].values, '--', color=c[-1],)

plt.subplots_adjust(bottom=0.4)
leg1=plt.legend(bbox_to_anchor=(1.05, -0.3), ncol=5) 
plt.legend([l2,l1], [r'Injection',r'Flow'])
plt.gca().add_artist(leg1)
plt.xlabel('M/N', fontsize=12)
plt.ylabel(r'$K_{95}^M$/$K_{95}^N$', fontsize=12)
plt.yscale('log')
plt.xscale('log')
#plt.ylim([-10,600])
plt.grid()
plt.savefig(path+'figures/K_q_vs_M_analytics_normed_alpha{}.pdf'.format(alpha))








