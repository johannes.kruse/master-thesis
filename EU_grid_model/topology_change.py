# -*- coding: utf-8 -*-

import lib.bal_flow_net as bfn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import lib.observables as ob
import lib.help_tools as ht

path= './EU_grid_model/Results/topology_change/'

#%% Prepare network and calculation injection

alpha=0.0
net=bfn.BaFloNet().load_top().load_dir(alpha=alpha)
L=net.lines.shape[0]
M=net.buses.shape[0]

net.balance().calc_sp().calc_peig()

#%% Parameters for different methods

#Polygons to chose nodes from
pmid=[( 0.52, 45.251), ( 7.313,49.302), ( 18.139,49.068), (17.368,47.768),
      (7.566, 47.797), (2.516, 44.618)]
pnorth1=[(-5.998,48.57), (10.217,58.163), ( 22.71,55.55), ( 25.16,53.346),
        ( 8.,52.651), ( -3.221,46.617)]
pnorth2=[(-2.3, 45.2), (25.4,52.2), (16.3,59.3), (-7.6,48.4)]
psouth=[(-9.5,42.2),(-6.6,36.),(27.3,39.),(27,47.)]
peast=[(26.,44.), (22.,39.), (14., 53.), (16.5, 53.)]
pwest=[(-7.,38.), (3., 49.), (5.5, 49.),(-3., 38.)]

#Lines to cut edges
l1=[(7.89,53.95),(8.84, 44.19)] #32
l2=[(-1.8,44),(3.3, 42.8)] #8
l3=[(22.74,48.62),(13.,45.5)] #12
l4=[(-1.8,44),(22.74,48.62)] #44
l5=[(8.8,43.),(13.,45.)] #8

#choose nlines, so that always one connection between clusters is left!

#%% Chose lines using one method

method='line'
number=1
params=l1
nlines=31

if method=='line':
    chosen_lines= ht.choose_lines(net, params, nlines, method=method)    
else:
    buses0,buses1= ht.choose_lines(net, params, nlines, method=method)

#%% Chose lines using multiple cut-lines
    
method='line'
number=8
nlines=56

params=l1
chosen_lines1= ht.choose_lines(net, params, 31, method=method)    
params=l2
chosen_lines2= ht.choose_lines(net, params, 7, method=method)    
params=l3
chosen_lines3= ht.choose_lines(net, params, 11, method=method)    
params=l5
chosen_lines4= ht.choose_lines(net, params, 7, method=method)   

chosen_lines=np.concatenate([chosen_lines1,chosen_lines2, chosen_lines3, chosen_lines4])
#chosen_lines=np.setdiff1d(chosen_lines,['907','499','173','709'])

np.random.shuffle(chosen_lines)

#%% Chose lines using multiple polygons

method='polygon'
number=4
nlines=31

params=[pnorth2, psouth, 'x']
buses0_1, buses1_1= ht.choose_lines(net, params, 15, method=method)    
params=[peast, pwest, 'y']
buses0_2, buses1_2= ht.choose_lines(net, params, 16, method=method)    

buses0=np.concatenate([buses0_1, buses0_2])
buses1=np.concatenate([buses1_1, buses1_2])

shuffle_ind=np.arange(nlines)
np.random.shuffle(shuffle_ind)
buses0=buses0[shuffle_ind]
buses1=buses1[shuffle_ind]

#%% Chose buses0,buses1 from netplot.pdf

method='supergrid'
number=1

buses0=map(str,[250, 1250, 1509, 1039,1509, 770, 1074, 1102, 1056, 1056, 1011,1190,1398,1398,1270,749,175,215,20,175,307,307,250,749,307,250,1250,1509,1509 ])
buses1=map(str,[1250, 1509, 1039, 770, 770, 1074,1102, 1056, 933, 1011,1190,1398,1420, 1270,749,175,215,20,31,49,49,31,307,670,466,296,966,890,819 ])
nlines=len(buses0)

#%% Plot the chosen buses/ chosen lines

if method=='line':
    lc=np.ones(net.lines.shape[0])
    lc[net.lines.index.isin(chosen_lines)]=0.2
    lw=np.ones(net.lines.shape[0])*0.2
    lw[net.lines.index.isin(chosen_lines)]=2
    
    net.plot(line_colors=lc, line_widths=lw, bus_sizes=0.1)
    plt.savefig(path + 'figures/lines_from_method_{}{}.pdf'.format(method, number))
    
else:
    net.plot(line_widths=0., bus_sizes=1)
    for bus in buses0[:nlines]:
        plt.plot(net.buses.loc[bus].x, net.buses.loc[bus].y, 'o', c='tab:green', ms=3)
    for bus in buses1[:nlines]:
        plt.plot(net.buses.loc[bus].x, net.buses.loc[bus].y, 'o', c='tab:orange', ms=3)
    
    plt.savefig(path + 'figures/buses_method_{}{}.pdf'.format(method, number))

    
#%% Add/Remove lines and simulate the system

res=pd.DataFrame(columns=['L','F_K95'], index=np.arange(nlines))
#res.name='Method:{}, Params:{}'.format(method, params)

net.calc_hth().calc_feig()

res.iloc[0].F_K95=ob.qx(net.sf_eva, 0.95)[0]
res.iloc[0].L= net.lines.shape[0]

for i in np.arange(nlines):
    print i
    if method=='line':
        net.remove('Line', chosen_lines[i])
        net.update_ptdf()
    else:
        net.add_line(buses0[i], buses1[i], '{}'.format(L+i), up_ptdf=False)

    net.update_ptdf()
    net.calc_hth().calc_feig()
    
    res.iloc[i+1].F_K95=ob.qx(net.sf_eva, 0.95)[0]
    res.iloc[i+1].L= net.lines.shape[0]
    
res.to_csv(path + 'result_alpha{}_method_{}{}.csv'.format(alpha, method, number))

#%% Plot all new lines

if not method=='line':
    lc=np.ones(net.lines.shape[0])
    lc[L:]=0.2
    
    net.plot(line_colors=lc, bus_sizes=1)
    plt.savefig(path + 'figures/lines_from_method_{}{}.pdf'.format(method, number))

#%% Plot result

plt.figure(figsize=(5,3))
plt.plot(res.loc[:,'F_K95'], '.-')
plt.ylabel(r'$K_{95}^F$', fontsize=15)
if method=='line':
    plt.xlabel('Number of removed links', fontsize=15)
else:
    plt.xlabel('Number of new links', fontsize=15)

plt.savefig(path+'figures/results_F_K95_alpha{}_method_{}{}.pdf'.format(alpha,
                                                                        method,
                                                                        number),
            bbox_inches='tight')
            


#%% Plot topological components using all new lines

net.calc_htheig()
hth_eva=net.hth_eva/net.hth_eva.sum()
hth_eve=net.hth_eve

vmax=0.04
fig,ax=plt.subplots(3,2, figsize=(9,7))
for j in range(6):
    net.rplot(hth_eve[:,j], ax=ax[j/2][j%2], 
             title=r'$\tilde\lambda_{}={:.3f}$'.format(j+1,hth_eva[j]),
             vmin=-vmax, vmax=vmax)

ht.plot_cbar(vmax, fig, ax)
plt.savefig(path+'figures/hth_eve_method_{}{}.pdf'.format(method,number),
            bbox_inches='tight')


#%% Plot flow-PCs using all new lines

feva=net.sf_eva/net.sf_eva.sum()
feve=net.sf_eve
meve=net.m_eve

vmax=0.04
fig,ax=plt.subplots(1,3,figsize=(10,5))

for i in range(3):
    net.rplot(meve[:,i], ax=ax[i], vmin=-vmax, vmax=vmax, alpha=0.4)
    net.qplot(feve[:,i], ax=ax[i], title=r'$\tilde\lambda_{{{}}}={:.3f}$'.format(i,feva[i]),
              quant=0.9, basemap=False, line_widths=3000)
plt.tight_layout()

ht.plot_cbar(vmax, fig, ax, orientation='horizontal')
plt.savefig(path+'figures/sf_eve_alpha{}_method_{}{}.pdf'.format(alpha, method,number),
            bbox_inches='tight')

#%% Eigenvalue&vector overlap

ht.plot_eve_overlap(net.sp_eva,net.sp_eve, net.hth_eva,net.hth_eve)
plt.savefig(path+ 'figures/overlap_coeff_flow_pca_alpha{}_method_{}{}.pdf'.format(alpha,
            method,number))


#%% Add nlines from northern to southern area at distance h

pup=[(-5.998,48.57), (10.217,58.163), ( 22.71,55.55), ( 25.16,53.346),
        ( 8.,52.651), ( -3.221,46.617)]
pdown=lambda h: [( -9., 50.-h), ( 30.,50-h), (30.,49-h), (-9., 49-h)]

h_range=np.linspace(0,10,20)

method='ns_lines'
number=1
nlines=15
res=pd.DataFrame(columns=['h','F_K95'], index=np.arange(h_range.shape[0]))
buses0,dummy= ht.choose_lines(net, [pup, pdown(0), 'x'], nlines, method='polygon',
                                    randomize=False)

for j,h in enumerate(h_range):
    
    print j
    params=[pup, pdown(h), 'x']
    dummy,buses1= ht.choose_lines(net, params, nlines, method='polygon', randomize=False)
    
    for i in np.arange(nlines):
            net.add_line(buses0[i], buses1[i], '{}'.format(L+i), up_ptdf=False)
            
    net.update_ptdf()
    net.calc_hth().calc_feig()    
    res.iloc[j].h=h
    res.iloc[j].F_K95=ob.qx(net.sf_eva, 0.95)[0]  
    
    for i in np.arange(nlines):
            net.remove('Line', '{}'.format(L+i))


res.to_csv(path + 'result_alpha{}_method_{}{}_nlines{}.csv'.format(alpha, method, number,nlines))

#%% Plot result

plt.figure(figsize=(5,3))
plt.plot(49.5-res.h.values,  res.loc[:,'F_K95'].values, '.-')
plt.ylabel(r'$K_{95}^F$', fontsize=15)

plt.xlabel(r'Lattitude h [$^\circ$]', fontsize=15)

plt.savefig(path+'figures/results_F_K95_alpha{}_method_{}{}.pdf'.format(alpha,
                                                                        method,
                                                                        number),
            bbox_inches='tight')

#%% Plot new lines at maximal h

import geopandas as gpd
from shapely.geometry import Polygon
    
#for i in np.arange(nlines):
#    net.add_line(buses0[i], buses1[i], '{}'.format(L+i), up_ptdf=False)
    
lc=np.repeat('y', net.lines.shape[0])
lc[L:]='b'

net.plot(line_colors=lc, bus_sizes=1)
pm=gpd.GeoSeries(Polygon(pdown(h)))
pm.plot(alpha=0.5,ax=plt.gca(), color='b')
plt.axis('on')
plt.gca().set_yticks(np.linspace(40,50,6))
plt.gca().set_yticklabels(['{}$^\circ$'.format(np.int(i)) for i in np.linspace(40,50,6)])
plt.grid()
hx,hy=np.array(pdown(h)).mean(axis=0)
plt.plot(hx,hy,'x', color='red', ms=10)
plt.text(hx-4.2, hy-1.8, 'Lattitude h',{'color':'r'} )

plt.savefig(path + 'figures/lines_from_method_{}{}.pdf'.format(method, number))

