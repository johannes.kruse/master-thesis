# -*- coding: utf-8 -*-

#(Try to) reproduce results from scaling paper [Schäfer et al, 2017]

import lib.bal_flow_net as bfn
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit as cf
import matplotlib.pyplot as plt


path= './EU_grid_model/Results/scaling_paper/'

#%% Define coarse graining scales
dxs=10.**np.arange(-1,1.2,0.08)
nbins= np.unique(np.round(2**np.arange(1,6.2,0.2)))[::-1][::2]
result=pd.DataFrame(index=range(nbins.shape[0]+1),
                    columns=['dx','nbin','M','dM','K', 'Tk'],
                    dtype=np.float)



#%% Calculate original network

orig_net=bfn.BaFloNet().load_top().load_dir()

orig_net.balance().lpf()
Kl=orig_net.lines_t.p0.abs().quantile(0.99)

result.iloc[0].nbin=None
result.iloc[0].M=orig_net.buses.shape[0]
result.iloc[0].dM=orig_net.lines.length.mean()
result.iloc[0].K=Kl.sum()
result.iloc[0].Tk=(Kl*orig_net.lines.length).sum()

#%% Coarse-graining

net=orig_net
for i,nbin in enumerate(nbins, start=1):
    net=bfn.coarse_grain(net, nbin=nbin)[0]
    
    net.balance().lpf()
    Kl=net.lines_t.p0.abs().quantile(0.99)
    
    result.iloc[i].nbin=nbin
    result.iloc[i].M=net.buses.shape[0]
    result.iloc[i].dM=net.lines.length.mean()
    result.iloc[i].K=Kl.sum()
    result.iloc[i].Tk=(Kl*net.lines.length).sum()
    
    print i,' out of ', nbins.shape[0], ' done! \n'

#%% Save/Load results

#result.to_csv(path+'iterative_rectangular_coarse_graining_result.csv')
#result=pd.read_csv(path+'iterative_rectangular_coarse_graining_result.csv')

#%% Plot #nodes vs. mean link length

f=lambda x,a,b: a*x**b
popt, pcov= cf(f, result.dM.values[:-7], result.M.values[:-7])
plt.loglog(result.dM.values, result.M.values, '.') 
plt.loglog(np.arange(50,1000), f(np.arange(50,1000),popt[0],popt[1]),
           label=r'$\propto \langle d_M \rangle^{\eta}$, $\eta=$'+ \
           '{:.2f}'.format(popt[1]) )
plt.xlabel(r'$\langle d_M \rangle$',fontsize=16)
plt.ylabel('M',fontsize=16)
plt.legend(fontsize=16)
#plt.savefig(path + 'dM_M_iterative_rectangular.pdf')

#%% Plot transmission capacity vs. mean link length

f_KM=lambda x,N: np.sqrt(1-(2*np.log(x))/np.log(N))*x**(-1)

popt, pcov= cf(f, result.dM.values/result.dM[0], result.K.values/result.K[0])
plt.loglog(result.dM.values/result.dM[0], result.K.values/result.K[0], '.') 
plt.loglog(np.arange(1,20,0.5), f(np.arange(1,20,0.5),popt[0],popt[1]),
           label=r'$\propto \langle d_M \rangle^{\eta}$, $\eta=$'+ \
           '{:.2f}'.format(popt[1]))
plt.loglog(result.dM.values/result.dM[0],
           f_KM(result.dM.values/result.dM[0],  result.M[0]),
           '-', label='Analytical')
plt.xlabel(r'$\langle d_M \rangle/\langle d_N \rangle$',fontsize=16)
plt.ylabel(r'$K_M/K_N$',fontsize=16)
plt.legend(fontsize=16)
#plt.savefig(path + 'dM_K_iterative_rectangular.pdf')

#%% Plot transmission cost vs. mean link length

f_TM=lambda x, N: np.sqrt(1-(2*np.log(x))/np.log(N))

popt, pcov= cf(f, result.dM.values[:-7]/result.dM[0], result.Tk.values[:-7]/result.Tk[0])
plt.loglog(result.dM.values/result.dM[0], result.Tk.values/result.Tk[0], '.') 
plt.loglog(np.arange(1,20,0.5), f(np.arange(1,20,0.5),popt[0],popt[1]),
           label=r'$\propto \langle d_M \rangle^{\eta}$, $\eta=$'+ \
           '{:.2f}'.format(popt[1])) 
plt.loglog(result.dM.values/result.dM[0],
           f_TM(result.dM.values/result.dM[0],  result.M[0]),
           '-', label='Analytical')
plt.xlabel(r'$\langle d_M \rangle/\langle d_N \rangle$',fontsize=16)
plt.ylabel(r'$T_M/T_N$',fontsize=16)
plt.legend(fontsize=16)
#plt.savefig(path + 'dM_T_iterative_rectangular.pdf')



