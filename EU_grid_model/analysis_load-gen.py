# -*- coding: utf-8 -*-

#Data analysis of original model: Load, Generation

import lib.bal_flow_net as bfn
import numpy as np
import scipy.linalg as sl
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import lib.help_tools as ht
from matplotlib.colorbar import ColorbarBase

path= './EU_grid_model/Results/original_network/'


#%% Import load and generation 

data_dir='/home/adminuser/Dokumente/Master_Arbeit/' + \
            'Code/EU_grid_model/Results/data/re-eu_original' 
            
g_wind=pd.read_pickle(data_dir + '/g_wind.gzip')
g_solar=pd.read_pickle(data_dir + '/g_solar.gzip')
load=pd.read_pickle(data_dir + '/load.gzip')

net=bfn.BaFloNet().load_top()

g_solar, g_wind= ht.scale_layout(g_wind,g_solar, load, net.buses, alpha=0.5,
                                return_mis=False)

#%% Correlation matrix of Load and generation

z=pd.concat([g_wind,g_solar, load], axis=1)
corr=np.corrcoef(z.values.T)

fig,ax=plt.subplots()

im=ax.imshow(corr, vmin=-1, vmax=1, cmap='RdBu_r')
ax.set_xticks([500,2000,3500])
ax.set_yticks([500,2000,3500])
ax.set_xticklabels(['$\mathbf G^W$','$\mathbf G^S$','$\mathbf L$'])
ax.set_yticklabels(['$\mathbf G^W$','$\mathbf G^S$','$\mathbf L$'])
cbar=plt.colorbar(im)

plt.savefig(path+'figures/load_gwind_gsolar_corr.pdf')
plt.clf()

cov=np.cov(z.values.T)
fig,ax=plt.subplots()

im=ax.imshow(cov, vmax=1, vmin=0.)
ax.set_xticks([500,2000,3500])
ax.set_yticks([500,2000,3500])
ax.set_xticklabels(['$\mathbf G^W$','$\mathbf G^S$','$\mathbf L$'])
ax.set_yticklabels(['$\mathbf G^W$','$\mathbf G^S$','$\mathbf L$'])
cbar=plt.colorbar(im)

#plt.savefig(path+'figures/load_gwind_gsolar_cov.pdf')

#%% Spatial correlation function

cor_l=ht.spatial_corr(load, net.buses.loc[:,'x':'y'].values)[0]
cor_w=ht.spatial_corr(g_wind, net.buses.loc[:,'x':'y'].values)[0]
cor_s=ht.spatial_corr(g_solar, net.buses.loc[:,'x':'y'].values)[0]

plt.figure(figsize=(5,2.5))
plt.plot(cor_w[:,0], cor_w[:,1], label=r'$\mathbf G^W$')
plt.plot(cor_s[:,0], cor_s[:,1], label=r'$\mathbf G^S$')
plt.plot(cor_l[:,0], cor_l[:,1], label=r'$\mathbf L$')
plt.legend(ncol=3)
plt.ylim([0,1.1])
plt.xlabel('Distance d [km]', fontsize=12)
plt.ylabel('$Q_d$', fontsize=16)

plt.savefig(path + 'figures/spat_corr_load-gen.pdf',bbox_inches='tight' )

#%% Wind generation SCF with scatter plot

cor_w=ht.spatial_corr(g_wind, net.buses.loc[:,'x':'y'].values)[0]
cor_vals=ht.spatial_corr(g_wind, net.buses.loc[:,'x':'y'].values,
                         no_avrg=True)

plt.plot(cor_vals.dij, cor_vals.correl, '.', markersize=1,
         label=r'Corr$(G^W_n, G^W_m)$')
plt.plot(cor_w[:,0], cor_w[:,1], linewidth=4,  label=r'$Q_d(\mathbf G^W)$')

plt.legend(fontsize=14)
plt.xlabel('Distance d [km]', fontsize=14)
plt.ylabel('Correlation', fontsize=14)

plt.savefig(path + 'figures/spat_corr_gwind_with_scatter.jpeg', dpi=300)


#%% PCA
      
names=['g_wind_','g_solar_','load_']
M=net.buses.shape[0]

for i,data in enumerate([g_wind,g_solar, load]):
        
    eva, eve= sl.eigh(data.cov().values)     
    eva, eve= eva[::-1]/eva.sum(), eve[:,::-1]

    fig,ax=plt.subplots(2,3,figsize=(10,5))
    vmax=0.04#np.abs(res[:,:2]).max()

    for j in range(6): 
        a=net.plot(line_widths=0., ax=ax[j/3][j%3],line_colors='k',
                   bus_cmap='coolwarm',
                   title=r'$\tilde\lambda_{}={:.3f}$'.format(j+1,eva[j]),
                   bus_colors=eve[:,j],   bus_sizes=2., basemap=False)
        a[0].set_clim(-vmax,vmax)
        a[0].set_linewidth(0)
        ht.eu_map(net,ax[j/3][j%3])

    
    cbar=plt.colorbar(a[0], ax=ax[1], orientation='horizontal', aspect=100,
                 pad=0.05)
    ticks=cbar.ax.get_xticklabels()
    ticks[0].set_text(r'$< 0.04$')
    ticks[-1].set_text(r'$> 0.04$')
    cbar.ax.set_xticklabels(ticks) 
    
    
    plt.savefig(path+\
                'figures/{}eve_M{}.pdf'.format(names[i],M), bbox_inches='tight')
                
#%% Principal Topological Components
    
net.calc_hth().calc_htheig()
hth_eva=net.hth_eva/net.hth_eva.sum()
hth_eve=net.hth_eve

vmax=0.04
M=net.buses.shape[0]
fig,ax=plt.subplots(3,2,figsize=(10,8))

for j in range(6):
    net.rplot(hth_eve[:,j], ax=ax[j/2][j%2], 
             title=r'$\tilde\lambda_{}={:.3f}$'.format(j+1,hth_eva[j]),
             vmin=-vmax, vmax=vmax)

cbar_ax = fig.add_axes([0.9, 0.05, 0.01, .9])
ColorbarBase(cmap='RdBu_r', norm=plt.Normalize(vmin=-vmax, vmax=vmax),ax=cbar_ax)
ticks=cbar_ax.get_yticklabels()
ticks[0].set_text(r'$< 0.04$')
ticks[-1].set_text(r'$> 0.04$')
cbar_ax.set_yticklabels(ticks) 
                
plt.savefig(path+\
            'figures/hth_eve_M{}.pdf'.format(M), bbox_inches='tight')

    
    
    
    
    
    
    