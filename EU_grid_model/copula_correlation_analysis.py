# -*- coding: utf-8 -*-

#Apply the normal-copula model to control correlations between nodes. Apply mainly
#to network of one size and examine correlation matrix/ spatial correlogram

import lib.bal_flow_net as bfn
import numpy as np
import matplotlib.pyplot as plt
import lib.ts_manipulation as tm
import copy
import sympy as sp
import lib.help_tools as ht
import matplotlib.cm as cm
import pandas as pd

path= './EU_grid_model/Results/copula_method/correlations/'



#%% Create network with original and rescaled mismatch

delta=1.

#create network
orig_net=bfn.BaFloNet().load_top().load_dir()
orig_net.balance().lpf()
N=orig_net.mis.shape[1]

#rescale mismatch
resca_net=copy.deepcopy(orig_net)
ncopula=tm.NormalCopula().estimate_cov(resca_net.mis).scale_cov(delta)
resca_net.mis=ncopula.sample_joint_dist(resca_net.mis)
resca_net.balance().lpf()

#%%Compare correlations on network with original and rescaled mismatch

#mismatch correlation 
plt.imshow(np.corrcoef(orig_net.mis.values.T))
plt.colorbar().set_label(label=r'$\rho_{original}$', size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+'figures/original_mismatch_correlation_N{}.pdf'.format(N))
plt.clf()

plt.imshow(np.corrcoef(resca_net.mis.values.T))
plt.colorbar().set_label(label=r'$\rho_{{\delta={} }}$'.format(delta), size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+\
            'figures/scale_original_mismatch_{}_mismatch_correlation_N{}.pdf'.format(delta,N))
plt.clf()

plt.imshow(np.abs(np.corrcoef(resca_net.mis.values.T)-np.corrcoef(orig_net.mis.values.T)))
plt.colorbar().set_label(label=r'$|\rho_{{original}}-\rho_{{\delta={} }}|$'.format(delta),
             size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+\
            'figures/scale_original_mismatch_{}_mismatch_correlation_error_N{}.pdf'.format(delta,N))
plt.clf()

#injection correlation
plt.imshow(np.corrcoef(-orig_net.loads_t.p_set.values.T))
plt.colorbar().set_label(label=r'$\rho_{original}$', size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+'figures/original_injection_correlation_N{}.pdf'.format(N))
plt.clf()

plt.imshow(np.corrcoef(-resca_net.loads_t.p_set.values.T))
plt.colorbar().set_label(label=r'$\rho_{{\delta={} }}$'.format(delta), size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+\
            'figures/scale_original_mismatch_{}_injection_correlation_N{}.pdf'.format(delta,N))
plt.clf()

plt.imshow(np.abs(np.corrcoef(orig_net.loads_t.p_set.values.T)-\
                  np.corrcoef(resca_net.loads_t.p_set.values.T)))
plt.colorbar().set_label(label=r'$|\rho_{{original}}-\rho_{{\delta={} }}|$'.format(delta),
             size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+\
            'figures/scale_original_mismatch_{}_injection_correlation_error_N{}.pdf'.format(delta,N))
plt.clf()

#Flow correlation
plt.imshow(np.corrcoef(orig_net.lines_t.p0.values.T))
plt.colorbar().set_label(label=r'$\rho_{original}$', size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+'figures/original_flow_correlation_N{}.pdf'.format(N))
plt.clf()

plt.imshow(np.corrcoef(resca_net.lines_t.p0.values.T))
plt.colorbar().set_label(label=r'$\rho_{{\delta={} }}$'.format(delta), size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+\
            'figures/scale_original_mismatch_{}_flow_correlation_N{}.pdf'.format(delta,N))
plt.clf()

plt.imshow(np.abs(np.corrcoef(resca_net.lines_t.p0.values.T)-\
                  np.corrcoef(orig_net.lines_t.p0.values.T)))
plt.colorbar().set_label(label=r'$|\rho_{{original}}-\rho_{{\delta={} }}|$'.format(delta),
             size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+\
            'figures/scale_original_mismatch_{}_flow_correlation_error_N{}.pdf'.format(delta,N))
plt.clf()

#Load correlation
plt.imshow(np.corrcoef(orig_net.load.values.T))
plt.colorbar().set_label(label=r'$\rho_{original}$', size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+'figures/original_load_correlation_N{}.pdf'.format(N))
plt.clf()

#Flow, mismatch and injection marginal pdfs

plt.hist(orig_net.mis.values[:,0], bins=100, histtype='step', linewidth=2,
         normed=True, label='Original node 1',log=True)
plt.hist(resca_net.mis.values[:,0], bins=100, histtype='step', linewidth=2, normed=True,
         label='Copula node 1',log=True)
plt.xlabel('Mismatch')
plt.ylabel('Propability density')
plt.legend()
plt.savefig(path+'figures/scale_original_mismatch_{}_N{}_mismatch_marginal.pdf'.format(delta,N))
plt.clf()

plt.hist(-orig_net.loads_t.p_set.values[:,0], bins=100, histtype='step', linewidth=2,
         normed=True, label='Original node 1',log=True)
plt.hist(-resca_net.loads_t.p_set.values[:,0], bins=100, histtype='step', linewidth=2,
         normed=True,label='Copula node 1',log=True)
plt.xlabel('Injection')
plt.ylabel('Propability density')
plt.legend()
plt.savefig(path+'figures/scale_original_mismatch_{}_N{}_injection_marginal.pdf'.format(delta,N))
plt.clf()

plt.hist(orig_net.lines_t.p0.values[:,0], bins=100, histtype='step', linewidth=2,
         normed=True, label='Original link 1', log=True)
plt.hist(resca_net.lines_t.p0.values[:,0], bins=100, histtype='step', linewidth=2,
         normed=True, label='Copula link 1',log=True)
plt.xlabel('Flow')
plt.ylabel('Propability density')
plt.legend()
plt.savefig(path+'figures/scale_original_mismatch_{}_N{}_flow_marginal.pdf'.format(delta,N))
plt.clf()


#%% Check spatial correlation function on original and copula-based network

fig1,ax1=plt.subplots()
fig2,ax2=plt.subplots()

for alpha in [0,0.8]:
    #create network
    orig_net=bfn.BaFloNet().load_top().load_dir(alpha=alpha)
    orig_net.balance()
    
    #rescale mismatch
    resca_net=copy.deepcopy(orig_net)
    ncopula=tm.NormalCopula().estimate_cov(resca_net.mis).scale_cov(1.)
    resca_net.mis=ncopula.sample_joint_dist(resca_net.mis)
    resca_net.balance()
    
    #Calculate spatial correlation
    cor1=ht.spatial_corr(orig_net.mis.values,orig_net.buses.loc[:,['y','x']].values)[0]
    cor2=ht.spatial_corr(resca_net.mis.values,resca_net.buses.loc[:,['y','x']].values)[0]
    cor3=ht.spatial_corr(-orig_net.loads_t.p_set.values,
                         orig_net.buses.loc[:,['y','x']].values)[0]
    cor4=ht.spatial_corr(-resca_net.loads_t.p_set.values,
                         resca_net.buses.loc[:,['y','x']].values)[0]
    #Plot
    ax1.plot(cor1.dist, cor1.correl, label=r'$\alpha={}$, original'.format(alpha))
    ax1.plot(cor2.dist, cor2.correl, label=r'$\alpha={}, \delta={}$'.format(alpha,1))
    ax2.plot(cor3.dist, cor3.correl, label=r'$\alpha={}$, original'.format(alpha))
    ax2.plot(cor4.dist, cor4.correl, label=r'$\alpha={}, \delta={}$'.format(alpha,1))

ax1.set_xlabel('Distance [km]')
ax1.set_ylabel(r'Corr($\Delta$)')
ax1.legend()

ax2.set_xlabel('Distance [km]')
ax2.set_ylabel('Corr(P)')
ax2.legend()

fig1.savefig(path + 'spatial_correlation_mismatch.pdf')
fig2.savefig(path + 'spatial_correlation_injection.pdf')

#%% Calculate spatial correlation for certain delta and different alphas

schemes=['syn', 'uni', 'one']    
alphas=np.linspace(0.,1., 21)[::2]
delta=0.

for n,alpha in enumerate(alphas):    
        
    #Load re-eu network and rescale solar-wind layout
    net=bfn.BaFloNet().load_top().load_dir(alpha=alpha)
    
    for scheme in schemes:
        
        #Manipulate mismatch with copula method and calculate injection
        ncopula=tm.NormalCopula().estimate_cov(net.mis).scale_cov(delta)
        net.mis=ncopula.sample_joint_dist(net.mis)
        net.balance(scheme=scheme)
        
        #Calculate spatial correlation
        cor_m=ht.spatial_corr(net.mis.values,
                              net.buses.loc[:,'x':'y'].values)[0]
        cor_p=ht.spatial_corr(-net.loads_t.p_set.values,
                              net.buses.loc[:,'x':'y'].values)[0]

        #Save results
        np.savez(path+'spatial_corr_alpha{}_M{}_{}_bal_delta{}'.format(alpha,
                 net.buses.shape[0], scheme, delta),
                 cor_m=cor_m, cor_p=cor_p)
        

            
    print '\n-----------------------------------------------\n'     
    print 'Parameters: ',n,' out of ', alphas.shape[0],' done! \n'
    print '\n-----------------------------------------------\n'  

#%% Plot spatial correlation for delta=0
    
c=cm.get_cmap('viridis')(np.linspace(0,1,alphas.shape[0]))

for M in [1494]:
    for scheme in ['syn', 'uni', 'one']:
        for i,alpha in enumerate(alphas):
            res=np.load(path + 'spatial_corr_alpha{}_M{}_{}_bal_delta0.0.npz'.format(alpha,
                        M, scheme))['cor_p']
            plt.plot(res[:,0],res[:,1], color=c[i], label=r'$\alpha={}$'.format(alpha))
        
        plt.xlabel('Distance [km]', fontsize=13)
        plt.ylabel('Corr(P)', fontsize=13)
        plt.ylim([-0.4,1.])
        plt.xlim([0,3200])
        plt.plot(np.arange(0,3200), np.zeros(np.arange(0,3200).shape[0]),
                 '--', color='k')
        plt.legend(ncol=4)
        
        plt.savefig(path + 'figures/spatial_corr_injection_M{}_{}_bal_delta0.0.pdf'.format(M, scheme))
        plt.clf()

for M in [1494]:
    for i,alpha in enumerate(alphas):
        res=np.load(path + 'spatial_corr_alpha{}_M{}_{}_bal_delta0.0.npz'.format(alpha,
                    M, scheme))['cor_m']        
        plt.plot(res[:,0],res[:,1], color=c[i], label=r'$\alpha={}$'.format(alpha))
        
    plt.xlabel('Distance [km]')
    plt.ylabel('Corr($\Delta$)')
    plt.ylim([-0.4,1.])
    plt.xlim([0,3000])
    plt.plot(np.arange(0,3000), np.zeros(np.arange(0,3000).shape[0]),
             '--', color='k')
    plt.legend(ncol=4)

    plt.savefig(path + 'figures/spatial_corr_mismatch_M{}_{}_bal_delta0.0.pdf'.format(M, scheme))
    plt.clf()

#%% Check correlations on coarse-grained network

#Use rescaled mismatch for coarse graining
net=bfn.coarse_grain(resca_net, nbin=14)[0]
net.balance()
N2=net.mis.shape[1]

#mismtach correlation of coarse-grain network
plt.imshow(np.corrcoef(net.mis.values.T))
plt.colorbar().set_label(label=r'$\rho_{{\delta={} }}$'.format(delta), size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+\
            'figures/scale_original_mismatch_{}_mismatch_correlation_N{}.pdf'.format(delta,N2))
plt.show()
plt.clf()

#injection correlation of coarse-grain network
plt.imshow(np.corrcoef(-net.loads_t.p_set.values.T))
plt.colorbar().set_label(label=r'$\rho_{{\delta={} }}$'.format(delta), size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+\
            'figures/scale_original_mismatch_{}_injection_correlation_N{}.pdf'.format(delta,N2))
plt.show()
plt.clf()

#load correlation of coarse-grain network
plt.imshow(np.corrcoef(net.load.values.T))
plt.colorbar().set_label(label=r'$\rho_{{\delta={} }}$'.format(delta), size=18)
plt.xlabel('Nodes')
plt.ylabel('Nodes')
plt.savefig(path+\
            'figures/scale_original_mismatch_{}_load_correlation_N{}.pdf'.format(delta,N2))
plt.show()
plt.clf()

#%% Analyse statistics of balancing term

plt.scatter(orig_net.lines_t.p0.abs().sum(axis=1),orig_net.mis.sum(axis=1))
plt.xlabel(r'$\sum_l |F_l(t)|$', fontsize=15)
plt.ylabel(r'$\sum_n \Delta_n(t)$', fontsize=15)
plt.savefig(path + 'figures/mis_sum_vs_flow_sum_N{}.pdf'.format(N))

plt.hist(orig_net.mis.sum(axis=1), histtype='step', linewidth=2, density=True,
         log=True, bins=100, label='original')
plt.hist(resca_net.mis.sum(axis=1), histtype='step', linewidth=2, density=True,
         log=True, bins=100, label=r'$\delta=1$')
plt.xlabel(r'$\sum_n \Delta_n$', fontsize=15)
plt.ylabel('Probability density', fontsize=15)
plt.legend(fontsize=13)
plt.savefig(path + 'figures/mis_sum_distribution_N{}.pdf'.format(N))


#%% Analyse balancing contribution to injection covariance matrix

Na=20
x=np.array(sp.symbols('a0:%d'%Na)).reshape((Na,1))

A= Na*np.dot(x,x.T)-(np.dot(x,np.ones((Na,1)).T)+np.dot(x,np.ones((Na,1)).T).T)
A=sp.Matrix(A)

vals=dict(zip(x[:,0],np.ones(Na)/Na))
#vals=dict(zip(x[:,0],np.append(1,np.zeros(Na-1))))

A2=A.evalf(subs=vals, chop=True)
print vals
#print
print A2.eigenvals()













