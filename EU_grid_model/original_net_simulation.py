# -*- coding: utf-8 -*-

#Simulations using the original re-eu data 

import lib.bal_flow_net as bfn
import numpy as np
import pandas as pd
import lib.observables as ob
import lib.help_tools as ht

path= './EU_grid_model/Results/original_network/'


#%% Define parameter ranges

nbins= np.unique(np.round(2**np.arange(1,6.3,0.2)))[::-1]
result=pd.DataFrame(index=range(nbins.shape[0]+1),
                    columns=['nbin','M','dM', 'F_K95', 'P_K95','M_K95', 'K', 'Tk'],
                    dtype=np.float)
alphas=np.linspace(0.,1., 21)



#%% Coarse grain and alpha sweep on original network

for n,alpha in enumerate(alphas):    
        
    #Load re-eu network and rescale solar-wind layout
    orig_net=bfn.BaFloNet().load_top().load_dir(alpha=alpha)
    
    #Calculate eigenvalues 
    orig_net.balance().calc_sp().calc_hth().calc_sm()
    orig_net.calc_meig().calc_peig().calc_feig().lpf()
    
    #Calculate transmission capacity
    Kl=orig_net.lines_t.p0.abs().quantile(0.99)
    
    #Save results
    result.iloc[0].nbin=None
    result.iloc[0].M=orig_net.buses.shape[0]
    result.iloc[0].dM=orig_net.lines.length.mean()
    result.iloc[0].P_K95=ob.qx(orig_net.sp_eva, 0.95)[0]
    result.iloc[0].F_K95=ob.qx(orig_net.sf_eva, 0.95)[0]
    result.iloc[0].M_K95=ob.qx(orig_net.sm_eva, 0.95)[0]
    result.iloc[0].K=Kl.sum()
    result.iloc[0].Tk=(Kl*orig_net.lines.length).sum()

    #Coarse-graining
    net=orig_net
    for i,nbin in enumerate(nbins, start=1):
        
        net=bfn.coarse_grain(net, nbin=nbin)[0]
        
        #Calculate eigenvalues 
        net.balance().calc_sp().calc_hth().calc_sm()
        net.calc_meig().calc_peig().calc_feig().lpf()
        
        
        #Calculate transmission capacity
        Kl=net.lines_t.p0.abs().quantile(0.99)
        
        #Save results
        result.iloc[i].nbin=nbin
        result.iloc[i].M=net.buses.shape[0]
        result.iloc[i].dM=net.lines.length.mean()
        result.iloc[i].P_K95=ob.qx(net.sp_eva, 0.95)[0]
        result.iloc[i].F_K95=ob.qx(net.sf_eva, 0.95)[0]
        result.iloc[i].M_K95=ob.qx(net.sm_eva, 0.95)[0]
        result.iloc[i].K=Kl.sum()
        result.iloc[i].Tk=(Kl*net.lines.length).sum()
                
        print 'Coarse-graining: ', i,' out of ', nbins.shape[0], ' done! \n'

    #Save alpha-delta result to file
    result.to_csv(path+'original_re-eu_alpha{}.csv'.format(alpha))
    
    #Clear results
    result*=np.nan

    print '\n-----------------------------------------------\n'     
    print 'Parameters: ',n,' out of ', alphas.shape[0],' done! \n'
    print '\n-----------------------------------------------\n'    



#%% pca on original network for different balancing

schemes=['syn', 'uni', 'one']    

for n,alpha in enumerate(alphas):    
        
    #Load re-eu network and rescale solar-wind layout
    orig_net=bfn.BaFloNet().load_top().load_dir(alpha=alpha)
    
    for scheme in schemes:
        
        #Balance and calculate eigenvalues 
        orig_net.balance(scheme=scheme)
        orig_net.calc_sm().calc_sp().calc_hth()
        orig_net.calc_meig().calc_peig().calc_feig().calc_htheig()
        
        #Save results
        np.savez(path+'pca_alpha{}_M{}_{}_bal'.format(alpha,
                 orig_net.buses.shape[0], scheme),
                 sm_eva=orig_net.sm_eva, sm_eve=orig_net.sm_eve,
                 sp_eva=orig_net.sp_eva, sp_eve=orig_net.sp_eve,
                 sf_eva=orig_net.sf_eva, sf_eve=orig_net.sf_eve,
                 hth_eva=orig_net.hth_eva, hth_eve=orig_net.hth_eve,
                 sm=orig_net.sm, sp=orig_net.sp)
        
        #Coarse-graining
        net=orig_net
        for i,nbin in enumerate(nbins, start=1):
            
            net=bfn.coarse_grain(net, nbin=nbin)[0]
            
            #Calculate eigenvalues 
            net.balance(scheme=scheme)
            net.calc_sm().calc_sp().calc_hth()
            net.calc_meig().calc_peig().calc_feig().calc_htheig()
        
    
            #Save results
            np.savez(path+'pca_alpha{}_M{}_{}_bal'.format(alpha, 
                     net.buses.shape[0], scheme),
                     sm_eva=net.sm_eva, sm_eve=net.sm_eve,
                     sp_eva=net.sp_eva, sp_eve=net.sp_eve,
                     sf_eva=net.sf_eva, sf_eve=net.sf_eve,
                     hth_eva=net.hth_eva, hth_eve=net.hth_eve,
                     sm=orig_net.sm, sp=orig_net.sp)
        
    print '\n-----------------------------------------------\n'     
    print 'Parameters: ',n,' out of ', alphas.shape[0],' done! \n'
    print '\n-----------------------------------------------\n'    


#%% Calculate spatial correlation of mismatch/injection in original network 

schemes=['syn', 'uni', 'one']    

for n,alpha in enumerate(alphas):    
        
    #Load re-eu network and rescale solar-wind layout
    orig_net=bfn.BaFloNet().load_top().load_dir(alpha=alpha)
    
    for scheme in schemes:
        
        #Balance and calculate spatial correlation
        orig_net.balance(scheme=scheme)
        cor_m=ht.spatial_corr(orig_net.mis.values,
                              orig_net.buses.loc[:,'x':'y'].values)[0]
        cor_p=ht.spatial_corr(-orig_net.loads_t.p_set.values,
                              orig_net.buses.loc[:,'x':'y'].values)[0]
        cor_dm_sq=ht.spatial_corr((orig_net.mis.values[1:]-orig_net.mis.values[:-1])**2,
                                orig_net.buses.loc[:,'x':'y'].values)[0]

        #Save results
        np.savez(path+'spatial_corr_alpha{}_M{}_{}_bal'.format(alpha,
                 orig_net.buses.shape[0], scheme),
                 cor_m=cor_m, cor_p=cor_p, cor_dm_sq=cor_dm_sq)
        
        #Coarse-graining
        net=orig_net
        for i,nbin in enumerate(nbins, start=1):
            
            net=bfn.coarse_grain(net, nbin=nbin)[0]
            
            #Balance and calculate spatial correlation
            net.balance(scheme=scheme)
            cor_m=ht.spatial_corr(net.mis.values,
                                  net.buses.loc[:,'x':'y'].values)[0]
            cor_p=ht.spatial_corr(-net.loads_t.p_set.values,
                                  net.buses.loc[:,'x':'y'].values)[0]
            cor_dm_sq=ht.spatial_corr((net.mis.values[1:]-net.mis.values[:-1])**2,
                                       net.buses.loc[:,'x':'y'].values)[0]
    
            #Save results
            np.savez(path+'spatial_corr_alpha{}_M{}_{}_bal'.format(alpha,
                     net.buses.shape[0], scheme),
                     cor_m=cor_m, cor_p=cor_p, cor_dm_sq=cor_dm_sq)
            
    print '\n-----------------------------------------------\n'     
    print 'Parameters: ',n,' out of ', alphas.shape[0],' done! \n'
    print '\n-----------------------------------------------\n'    

