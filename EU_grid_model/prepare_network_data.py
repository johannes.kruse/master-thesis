# -*- coding: utf-8 -*-

import pypsa as ps
import pandas as pd
import numpy as np
import os
import lib.help_tools as ht
import geopandas as gp
import networkx as nx
from shapely.geometry import Polygon

dat_path='/home/adminuser/Dokumente/Master_Arbeit/External_data/RE-Europe_dataset_package'

directory='/home/adminuser/Dokumente/Master_Arbeit/Code/EU_grid_model/' + \
'Results/data/re-eu_original/'

if not os.path.exists(directory):
    os.makedirs(directory)


#%% Create pypsa network from RE-EU data

network=ps.Network()

#Load buses and edges
buses=pd.read_csv(dat_path+'/Static_data/network_nodes.csv',
                  usecols=[0,2,4,5],
                  index_col=0)
buses=buses.rename(columns={'longitude':'x', 'latitude':'y'})
edges=pd.read_csv(dat_path+'/Static_data/network_edges.csv',usecols=[0,1,6])
edges=edges.rename(columns={'fromNode': 'bus0', 'toNode': 'bus1'})

#Initialize snapshot times and load names
times=pd.read_csv(dat_path+'/Nodal_TS/load_signal.csv', usecols=[0])
load_buses=pd.DataFrame(data=list(buses.index),
                        index=list(buses.index),
                        columns=['bus'])

#Set up pypsa network
network.import_components_from_dataframe(buses, 'Bus')
network.import_components_from_dataframe(edges,'Line')
network.import_components_from_dataframe(load_buses, 'Load')
network.set_snapshots(times.Time.iloc[:-1])
network.lines.x=np.ones(len(network.lines))

#Save network and ptdf matrix
ht.get_ptdf(network).to_pickle(directory+ 'ptdf.gzip')
network.export_to_csv_folder(directory)


#%% Import load, capacity factors and "proportional" capacity layouts
#(In last row solar data is missing, so we exclude it)

load=pd.read_csv(dat_path+'/Nodal_TS/load_signal.csv', index_col=0).iloc[:-1]

solar_cf=pd.read_csv(dat_path+'/Nodal_TS/solar_signal_COSMO.csv',
                     index_col=0).iloc[:-1]
wind_cf=pd.read_csv(dat_path+'/Nodal_TS/wind_signal_COSMO.csv',
                    index_col=0).iloc[:-1]

solar_lay=pd.read_csv(dat_path+'/Static_data/solar_layouts_COSMO.csv', 
                      index_col=0).Proportional
wind_lay=pd.read_csv(dat_path+'/Static_data/wind_layouts_COSMO.csv',
                     index_col=0).Proportional
                     
#%%Apply capacity layouts to obtain raw generation time series
         
g_wind=wind_cf.mul(wind_lay.values)
g_solar=solar_cf.mul(solar_lay.values)

#Save data
g_wind.to_pickle(directory + 'g_wind.gzip')
g_solar.to_pickle(directory + 'g_solar.gzip')
load.to_pickle(directory+ 'load.gzip')

#%% Construct nodal regions by using Voronoi cells 

#Calculate Voronoi cells
vor_cells=ht.finite_voronoi(network) 

#Create mask for area of the network
map_path='/home/adminuser/Dokumente/Master_Arbeit/External_data/maps/eu_map.geo.json'
mask=ht.make_region_mask(network,map_path)

#Clip voronoi cells to the area of the network 
regions=gp.overlay(mask,vor_cells, how='intersection')

#Reindex with node indice. Regions outside the mask are filled with "None"
regions=regions.set_index('node').reindex(list(network.buses.index),
                                          fill_value=None)
#Save 
with open(directory+ 'node_regions.geo.json', "w") as text_file:
    text_file.write(regions.to_json())

#%% Create 2d-lattice network for comparisson

n=2
directory='/home/adminuser/Dokumente/Master_Arbeit/Code/EU_grid_model/' + \
'Results/data/2d-latticeN{}/'.format(n)

if not os.path.exists(directory):
    os.makedirs(directory)


#Create networkx grid
g=nx.grid_2d_graph(n,n)

#Convert grid to pypsa network
network=ps.Network()
network.madd('Bus', names=[str(node) for node in g.nodes.keys()],
         x=np.array(g.nodes.keys())[:,0],
         y=np.array(g.nodes.keys())[:,1])
network.madd('Line', names=np.arange(len(g.edges.keys())),
         bus0=[b0 for b0,b1 in g.edges.keys()],
         bus1=[b1 for b0,b1 in g.edges.keys()], x=1)
network.set_snapshots(pd.DatetimeIndex(range(10))) #random!
network.buses.index.name='name'
network.lines.index.name='name'

#Create nodal regions for the grid
regions=ht.finite_voronoi(network, radius=3)
mask=Polygon([(-0.5,-0.5),(-0.5,n-0.5),(n-0.5,n-0.5), (n-0.5, -0.5)])
mask=gp.GeoDataFrame({'geometry':[mask]})
regions=gp.overlay(mask,regions, how='intersection')
regions=regions.set_index('node').reindex(list(network.buses.index))

#Save regions
with open(directory+ 'node_regions.geo.json', "w") as text_file:
    text_file.write(regions.to_json())

#Save network and ptdf matrix
ht.get_ptdf(network).to_pickle(directory+ 'ptdf.gzip')
network.export_to_csv_folder(directory)
    
    
    
    