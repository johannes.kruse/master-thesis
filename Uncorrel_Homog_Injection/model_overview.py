# -*- coding: utf-8 -*-

# Examine principal flow eigenvalues \lambda_k, i.e. eigenvalues of HH^T=L^{-1}, 
# for different network models. For uncorr, homog. injection \lambda_k=1/\mu_k
#and \mu_k are laplacian eigenvalues

import lib.net_model as nm
import pandas as pd
import lib.observables as ob
import lib.help_tools as ht

#%%

models=['ring', 'grid_2d', 'barabasi_albert', 'watts_strogatz',
        'g_nm', 'complete_graph']
obs=pd.DataFrame(columns=models, 
                 index=pd.MultiIndex.from_product([['q50_abs', 'q50_rel', 'q95_abs', 'q95_rel'],[64,256] ]))

#%%  1d periodic lattice


w1=nm.NetModel( 'grid_1d', 64).calc_spec().hth_eva[1:][::-1]
w2=nm.NetModel( 'grid_1d', 256).calc_spec().hth_eva[1:][::-1]

ht.g_plot([w1,w2], {'N':[64,256]}, 'ring')
obs.ring.loc[(['q50_abs', 'q50_rel'],64)]=ob.q50(w1)
obs.ring.loc[(['q50_abs', 'q50_rel'],256)]=ob.q50(w2)
obs.ring.loc[(['q95_abs', 'q95_rel'],64)]=ob.q95(w1)
obs.ring.loc[(['q95_abs', 'q95_rel'],256)]=ob.q95(w2)

#%%  2d lattice

w1=nm.NetModel( 'grid_2d', 64).calc_spec().hth_eva[1:][::-1]
w2=nm.NetModel( 'grid_2d', 256).calc_spec().hth_eva[1:][::-1]

#plt.plot(w,'o', label='Numerical')
#plt.plot(w_an, '.', label='Analytical')
#plt.xlabel('Number k')
#plt.ylabel(r'Eigenvalue $\mu_k$')
#plt.legend()
#plt.savefig('Results/2d_lattice_N{}_Eigenvals.pdf'.format(nx*nx))

ht.g_plot([w1,w2], {'N':[64,256]}, 'grid_2d')
obs.grid_2d.loc[(['q50_abs', 'q50_rel'],64)]=ob.q50(w1)
obs.grid_2d.loc[(['q50_abs', 'q50_rel'],256)]=ob.q50(w2)
obs.grid_2d.loc[(['q95_abs', 'q95_rel'],64)]=ob.q95(w1)
obs.grid_2d.loc[(['q95_abs', 'q95_rel'],256)]=ob.q95(w2)

#%% Barabasi-Albert

w1=nm.NetModel( 'barabasi_albert', [64,2]).average(50).hth_eva[1:][::-1]
w2=nm.NetModel( 'barabasi_albert', [256,3]).average(50).hth_eva[1:][::-1]

ht.g_plot([w1,w2],{'N':[64,256], 'c':[2,3]}, 'Barabasi-Albert')
obs.barabasi_albert.loc[(['q50_abs', 'q50_rel'],64)]=ob.q50(w1)
obs.barabasi_albert.loc[(['q50_abs', 'q50_rel'],256)]=ob.q50(w2)
obs.barabasi_albert.loc[(['q95_abs', 'q95_rel'],64)]=ob.q95(w1)
obs.barabasi_albert.loc[(['q95_abs', 'q95_rel'],256)]=ob.q95(w2)

#%% Watts-Strogatz

w1=nm.NetModel( 'watts_strogatz', [64, 4, 0.4]).average(50).hth_eva[1:][::-1]
w2=nm.NetModel( 'watts_strogatz', [256,4, 0.4]).average(50).hth_eva[1:][::-1]

ht.g_plot([w1,w2],{'N':[64,256], 'p':[0.4,0.4], 'k':[4,4]}, 'watts_strogatz')
obs.watts_strogatz.loc[(['q50_abs', 'q50_rel'],64)]=ob.q50(w1)
obs.watts_strogatz.loc[(['q50_abs', 'q50_rel'],256)]=ob.q50(w2)
obs.watts_strogatz.loc[(['q95_abs', 'q95_rel'],64)]=ob.q95(w1)
obs.watts_strogatz.loc[(['q95_abs', 'q95_rel'],256)]=ob.q95(w2)

#%% G_nm

w1=nm.NetModel( 'g_nm', [64,106]).average(50).hth_eva[1:][::-1]
w2=nm.NetModel( 'g_nm', [256,772]).average(50).hth_eva[1:][::-1]

ht.g_plot([w1,w2],{'N':[64,256], 'm':[106,772]},'g_nm')
obs.g_nm.loc[(['q50_abs', 'q50_rel'],64)]=ob.q50(w1)
obs.g_nm.loc[(['q50_abs', 'q50_rel'],256)]=ob.q50(w2)
obs.g_nm.loc[(['q95_abs', 'q95_rel'],64)]=ob.q95(w1)
obs.g_nm.loc[(['q95_abs', 'q95_rel'],256)]=ob.q95(w2)

#%% Complete Graph

w1=nm.NetModel('complete',64).calc_spec().hth_eva[1:][::-1]
w2=nm.NetModel('complete', 256).calc_spec().hth_eva[1:][::-1]

ht.g_plot([w1,w2], {'N':[64,256]}, 'complete')
obs.complete_graph.loc[(['q50_abs', 'q50_rel'],64)]=ob.q50(w1)
obs.complete_graph.loc[(['q50_abs', 'q50_rel'],256)]=ob.q50(w2)
obs.complete_graph.loc[(['q95_abs', 'q95_rel'],64)]=ob.q95(w1)
obs.complete_graph.loc[(['q95_abs', 'q95_rel'],256)]=ob.q95(w2)

#%%

obs.T['q50_abs'].plot(style='o',markersize=10,figsize=(8,5)).figure.savefig('./Results/q50_abs')
obs.T['q50_rel'].plot(style='o',markersize=10, figsize=(8,5)).figure.savefig('./Results/q50_rel')

obs.T['q95_abs'].plot(style='o',markersize=10,figsize=(8,5)).figure.savefig('./Results/q95_abs')
obs.T['q95_rel'].plot(style='o',markersize=10, figsize=(8,5)).figure.savefig('./Results/q95_rel')

